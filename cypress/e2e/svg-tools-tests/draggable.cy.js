describe('Test drag n drop', () => {
  const createCommentID = '#create-text';
  const removeItemID = '#remove-content';

  beforeEach(() => {
    // Go to welcome page of special cviceni for testing
    cy.visit('http://localhost:3000/svg-test/index.html');
    // Go to first page of cviceni
    cy.get('.content > .button').click();

    cy.get('.active-slide .nav-right').click('center');
    cy.get('.active-slide .nav-right').click('center');
    cy.get('.active-slide .nav-right').click('center');
  });

  it('Checking how the tag works in the menu during drop into the image, followed by move and then deletion by clicking.', () => {
    const svgID = 'svg-1-s';
    const itemID = 'pretahovani-1-drag-item-0';
    const dropItemID = `${svgID}_${itemID}_0_dropItem`;

    cy.get('#svg-test-4').scrollTo(0, 170);
    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID} g > *`).should('have.length', 1);
    cy.get(`#${svgID} foreignObject`).should('have.length', 0);

    // drag n drop to svg
    cy.dragAndCheck({
      svgID, dropItemID, itemID, elemCount: 2, foCount: 1, position: 'center',
    });

    // move to center
    cy.dragTo(`#${dropItemID}`, `#${svgID}`, 'center');

    // check
    cy.get(`#${svgID} foreignObject`)
      .should('have.length', 1)
      .and('attr', 'id', dropItemID);
    cy.get(`[data-id="${itemID}"]`).should('have.class', 'was-dropped');
    cy.get('.active-slide .draggable__item').eq(1).should('not.have.class', 'was-dropped');

    // remove
    cy.get(`#${dropItemID}`).click();

    // check
    cy.get(`#${svgID} foreignObject`).should('have.length', 0);
    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
  });

  // can visualy check if works drop in corners
  it('Check drop position of multiple tags in corners and update tags after deletion using radial menu.', () => {
    const svgID = 'svg-1-s';
    const itemID1 = 'pretahovani-1-drag-item-0';
    const itemID2 = 'pretahovani-1-drag-item-1';
    const dropItem2ID = `${svgID}_${itemID2}_4_dropItem`;

    cy.get('#svg-test-4').scrollTo(0, 170);
    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID} g > *`).should('have.length', 1);
    cy.get(`#${svgID} foreignObject`).should('have.length', 0);

    // drag n drop to svg
    cy.dragAndCheck({
      svgID, dropItemID: `${svgID}_${itemID1}_0_dropItem`, itemID: itemID1, elemCount: 2, foCount: 1, position: 'topRight',
    });
    cy.get('.active-slide .draggable__item').eq(1).should('not.have.class', 'was-dropped');

    cy.dragAndCheck({
      svgID, dropItemID: `${svgID}_${itemID1}_1_dropItem`, itemID: itemID1, elemCount: 3, foCount: 2, position: 'topLeft',
    });
    cy.get('.active-slide .draggable__item').eq(1).should('not.have.class', 'was-dropped');

    cy.dragAndCheck({
      svgID, dropItemID: `${svgID}_${itemID1}_2_dropItem`, itemID: itemID1, elemCount: 4, foCount: 3, position: 'bottomRight',
    });
    cy.get('.active-slide .draggable__item').eq(1).should('not.have.class', 'was-dropped');

    cy.dragAndCheck({
      svgID, dropItemID: `${svgID}_${itemID1}_3_dropItem`, itemID: itemID1, elemCount: 5, foCount: 4, position: 'bottomLeft',
    });
    cy.get('.active-slide .draggable__item').eq(1).should('not.have.class', 'was-dropped');

    cy.dragAndCheck({
      svgID, dropItemID: dropItem2ID, itemID: itemID2, elemCount: 6, foCount: 5, position: 'center',
    });
    cy.get('.active-slide .draggable__item').eq(1).should('have.class', 'was-dropped');

    // remove tag be menu
    cy.useRadialMenu({
      svgID,
      element: 'tag',
      elementID: dropItem2ID,
      items: [createCommentID, removeItemID],
      clickItem: removeItemID,
    });

    cy.get(`#${svgID} g > *`).should('have.length', 5);
    cy.get(`#${svgID} foreignObject`).should('have.length', 4);
    cy.get('.active-slide .draggable__item').eq(1).should('not.have.class', 'was-dropped');
  });

  it('Nothing happens when you drop out of the picture.', () => {
    const svgID = 'svg-1-s';
    const itemID = 'pretahovani-1-drag-item-0';
    const positions = [
      `[data-id="${itemID}"].is-dragged`, // drop on self
      '#svg-test-4 .svgs-container > :nth-child(1)', // drop next image
      '.title', // drop on logo
      '#svg-test-4 .zadani',
      '#svg-test-4 .draggable', // box with draggable items
      '#svg-test-4 .nav-button-prev', // back button
      '#svg-test-4 .help-activities > :nth-child(1)',
    ];

    positions.forEach((position) => {
      // move to position
      cy.dragTo(`[data-id="${itemID}"]`, position);

      cy.get('.active-slide .draggable__item')
        .should('have.length', 2)
        .each(($item) => {
          cy.get($item).should('not.have.class', 'was-dropped');
        });
      cy.get(`#${svgID} g > *`).should('have.length', 1);
      cy.get(`#${svgID} foreignObject`).should('have.length', 0);
    });
  });

  it('Duplication check for tag creation, movement and deletion.', () => {
    const svgID1 = 'svg-1-s';
    const svgID2 = 'svg-3-s';
    const itemID = 'pretahovani-1-drag-item-0';
    const dropItemID1 = `${svgID1}_${itemID}_0_dropItem`;
    const dropItemID2 = `${svgID1}_${itemID}_1_dropItem`;

    // check start state
    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID1} g > *`).should('have.length', 1);
    cy.get(`#${svgID1} foreignObject`).should('have.length', 0);

    // drag n drop to svg
    cy.dragAndCheck({
      svgID: svgID1, dropItemID: dropItemID1, itemID, elemCount: 2, foCount: 1, position: 'top',
    });

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID2} g > *`).should('have.length', 2);
    cy.get(`#${svgID2} foreignObject`)
      .should('have.length', 1)
      .and('attr', 'id', `${svgID2}_${itemID}_0_dropItem_clone-${svgID1}`);

    // go back, move
    cy.get('.active-slide .nav-left').click('center');

    cy.dragTo(`#${dropItemID1}`, `#${svgID1}`, 'center');

    // check
    cy.get(`#${svgID1} foreignObject`)
      .should('have.length', 1)
      .and('attr', 'id', dropItemID1);

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID2} g > *`).should('have.length', 2);
    cy.get(`#${svgID2} foreignObject`)
      .should('have.length', 1)
      .and('attr', 'id', `${svgID2}_${itemID}_0_dropItem_clone-${svgID1}`);

    // do back, remove by click
    cy.get('.active-slide .nav-left').click('center');

    cy.get(`#${dropItemID1}`).click();

    // check
    cy.get(`#${svgID1} foreignObject`).should('have.length', 0);
    cy.get(`#${svgID1} g > *`).should('have.length', 1);
    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });

    // go to next next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} foreignObject`).should('have.length', 0);
    cy.get(`#${svgID2} g > *`).should('have.length', 1);

    // go back, drop again
    cy.get('.active-slide .nav-left').click('center');

    // drag n drop to svg
    cy.dragAndCheck({
      svgID: svgID1, dropItemID: dropItemID2, itemID, elemCount: 2, foCount: 1, position: 'topLeft',
    });

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID2} g > *`).should('have.length', 2);
    cy.get(`#${svgID2} foreignObject`)
      .should('have.length', 1)
      .and('attr', 'id', `${svgID2}_${itemID}_1_dropItem_clone-${svgID1}`);

    // do back, remove by menu
    cy.get('.active-slide .nav-left').click('center');

    // remove tag be menu
    cy.useRadialMenu({
      svgID1,
      element: 'tag',
      elementID: dropItemID2,
      items: [createCommentID, removeItemID],
      clickItem: removeItemID,
    });

    // go to next next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} foreignObject`).should('have.length', 0);
    cy.get(`#${svgID2} g > *`).should('have.length', 1);
  });

  it('After moving the clone, the original is not moved and the id of clone is modified.', () => {
    const svgID1 = 'svg-1-s';
    const svgID2 = 'svg-3-s';
    const itemID = 'pretahovani-1-drag-item-0';
    const dropItemID = `${svgID1}_${itemID}_0_dropItem`;
    const dropDupItemID = `${svgID2}_${itemID}_0_dropItem_clone-${svgID1}`;

    // check start state
    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID1} g > *`).should('have.length', 1);
    cy.get(`#${svgID1} foreignObject`).should('have.length', 0);

    // drag n drop to svg
    cy.dragAndCheck({
      svgID: svgID1, dropItemID, itemID, elemCount: 2, foCount: 1, position: 'top',
    });

    // go to next page, check, move
    cy.get('.active-slide .nav-right').click('center');

    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID2} g > *`).should('have.length', 2);
    cy.get(`#${svgID2} foreignObject`)
      .should('have.length', 1)
      .and('attr', 'id', dropDupItemID);

    cy.dragTo(`#${dropDupItemID}`, `#${svgID1}`, 'topRight');

    cy.get(`#${svgID2} g > *`).should('have.length', 2);
    cy.get(`#${svgID2} foreignObject`)
      .should('have.length', 1)
      .and('attr', 'id', `${svgID2}_${itemID}_0_dropItem`);

    // go back and check
    cy.get('.active-slide .nav-left').click('center');

    cy.tagCheck({
      svgID: svgID1, elemCount: 2, foCount: 1, itemID, dropItemID,
    });
  });

  it('After removing by click the clone, the original is not removed.', () => {
    const svgID1 = 'svg-1-s';
    const svgID2 = 'svg-3-s';
    const itemID = 'pretahovani-1-drag-item-0';
    const dropItemID = `${svgID1}_${itemID}_0_dropItem`;
    const dropDupItemID = `${svgID2}_${itemID}_0_dropItem_clone-${svgID1}`;

    // check start state
    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID1} g > *`).should('have.length', 1);
    cy.get(`#${svgID1} foreignObject`).should('have.length', 0);

    // drag n drop to svg
    cy.dragAndCheck({
      svgID: svgID1, dropItemID, itemID, elemCount: 2, foCount: 1, position: 'top',
    });

    // go to next page, check, move
    cy.get('.active-slide .nav-right').click('center');

    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID2} g > *`).should('have.length', 2);
    cy.get(`#${svgID2} foreignObject`)
      .should('have.length', 1)
      .and('attr', 'id', dropDupItemID);

    cy.get(`#${dropDupItemID}`).click();

    cy.get(`#${svgID2} g > *`).should('have.length', 1);
    cy.get(`#${svgID2} foreignObject`).should('have.length', 0);

    // go back and check
    cy.get('.active-slide .nav-left').click('center');

    cy.tagCheck({
      svgID: svgID1, elemCount: 2, foCount: 1, itemID, dropItemID,
    });
  });

  it('After removing by radial menu the clone, the original is not removed.', () => {
    const svgID1 = 'svg-1-s';
    const svgID2 = 'svg-3-s';
    const itemID = 'pretahovani-1-drag-item-0';
    const dropItemID = `${svgID1}_${itemID}_0_dropItem`;
    const dropDupItemID = `${svgID2}_${itemID}_0_dropItem_clone-${svgID1}`;

    // check start state
    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID1} g > *`).should('have.length', 1);
    cy.get(`#${svgID1} foreignObject`).should('have.length', 0);

    // drag n drop to svg
    cy.dragAndCheck({
      svgID: svgID1, dropItemID, itemID, elemCount: 2, foCount: 1, position: 'top',
    });

    // go to next page, check, move
    cy.get('.active-slide .nav-right').click('center');

    cy.get('.active-slide .draggable__item')
      .should('have.length', 2)
      .each(($item) => {
        cy.get($item).should('not.have.class', 'was-dropped');
      });
    cy.get(`#${svgID2} g > *`).should('have.length', 2);
    cy.get(`#${svgID2} foreignObject`)
      .should('have.length', 1)
      .and('attr', 'id', dropDupItemID);

    cy.useRadialMenu({
      svgID2,
      element: 'tag',
      elementID: dropDupItemID,
      items: [createCommentID, removeItemID],
      clickItem: removeItemID,
    });

    cy.get(`#${svgID2} g > *`).should('have.length', 1);
    cy.get(`#${svgID2} foreignObject`).should('have.length', 0);

    // go back and check
    cy.get('.active-slide .nav-left').click('center');

    cy.tagCheck({
      svgID: svgID1, elemCount: 2, foCount: 1, itemID, dropItemID,
    });
  });
});
