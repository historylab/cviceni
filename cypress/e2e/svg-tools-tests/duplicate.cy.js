describe('Component duplication tests', () => {
  const createCommentID = '#create-text';
  const blueColorID = '#color-point-blue';
  const redColorID = '#color-point-red';
  const removeItemID = '#remove-content';

  const blueClassName = 'blue';
  const redClassName = 'red';

  const randomText = 'Testovací';

  beforeEach(() => {
    // Go to welcome page of special cviceni for testing
    cy.visit('/svg-test/index.html');
    // Go to first page of cviceni
    cy.get('.content > .button').click();
  });

  it('Checking for changing text of a comment clone and then change text the comment.', () => {
    const svgID1 = 'svg-1';
    const svgID2 = 'svg-3';
    let commentPositionX;
    let commentPositionY;
    let commentID;
    let circleCommentID;

    // create comment
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'image',
      position: { x: 50, y: 30 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID1} textarea`)
      .type(randomText)
      .then(($textare) => {
        commentID = $textare.attr('id');
      });

    cy.get(`#${svgID1} circle`)
      .then(($circle) => {
        commentPositionX = $circle.attr('cx');
        commentPositionY = $circle.attr('cy');
        circleCommentID = $circle.attr('id');

        cy.checkComment({
          svgID: svgID1,
          commentPositionX,
          commentPositionY,
          text: randomText,
          markID: circleCommentID,
          commentID,
          markColor: { active: false, colorClassName: [blueClassName, redClassName] },
        });
      });

    // go to next page, check, change text
    cy.get('.active-slide .nav-right').click('center');
    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle_clone-${svgID1}`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}_clone-${svgID1}`,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    cy.get(`#${svgID2} textarea`)
      .type(randomText)
      .blur(); // important for trigger event;

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: `${randomText}${randomText}`,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}`,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    // go back, check, change text
    cy.get('.active-slide .nav-left').click('center');

    cy.get(`#${svgID1}`).then(() => {
      cy.checkComment({
        svgID: svgID1,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: circleCommentID,
        commentID,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    cy.get(`#${svgID1} textarea`)
      .type(`${randomText}${randomText}`)
      .blur();

    cy.get(`#${svgID1}`).then(() => {
      cy.checkComment({
        svgID: svgID1,
        commentPositionX,
        commentPositionY,
        text: `${randomText}${randomText}${randomText}`,
        markID: circleCommentID,
        commentID,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: `${randomText}${randomText}`,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}`,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });
  });

  it('Checking for the move of a comment clone and then move the comment.', () => {
    const svgID1 = 'svg-1';
    const svgID2 = 'svg-3';
    let commentPositionX;
    let commentPositionY;
    let commentID;
    let circleCommentID;

    // create comment
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'image',
      position: { x: 50, y: 30 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID1} textarea`)
      .type(randomText)
      .then(($textare) => {
        commentID = $textare.attr('id');
      });

    cy.get(`#${svgID1} circle`)
      .then(($circle) => {
        commentPositionX = $circle.attr('cx');
        commentPositionY = $circle.attr('cy');
        circleCommentID = $circle.attr('id');

        cy.checkComment({
          svgID: svgID1,
          commentPositionX,
          commentPositionY,
          text: randomText,
          markID: circleCommentID,
          commentID,
          markColor: { active: false, colorClassName: [blueClassName, redClassName] },
        });
      });

    // go to next page, check, change text
    cy.get('.active-slide .nav-right').click('center');
    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle_clone-${svgID1}`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}_clone-${svgID1}`,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    cy.get(`#${svgID2}`)
      .trigger('mousedown', 60, 40, { eventConstructor: 'MouseEvent' })
      .trigger('mousemove', 'center', { eventConstructor: 'MouseEvent' })
      .trigger('mouseup', 'center', { eventConstructor: 'MouseEvent' });

    let newCommentPositionX;
    let newCommentPositionY;
    cy.get(`#${svgID2} circle`)
      .then(($circle) => {
        newCommentPositionX = $circle.attr('cx');
        newCommentPositionY = $circle.attr('cy');

        cy.checkComment({
          svgID: svgID2,
          commentPositionX: newCommentPositionX,
          commentPositionY: newCommentPositionY,
          text: randomText,
          markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle`,
          commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}`,
          markColor: { active: false, colorClassName: [blueClassName, redClassName] },
        });
      });

    // go back, check, change text
    cy.get('.active-slide .nav-left').click('center');

    cy.get(`#${svgID1}`).then(() => {
      cy.checkComment({
        svgID: svgID1,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: circleCommentID,
        commentID,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    cy.get(`#${svgID1}`)
      .trigger('mousedown', 60, 40, { eventConstructor: 'MouseEvent' })
      .trigger('mousemove', 'center', { eventConstructor: 'MouseEvent' })
      .trigger('mouseup', 'center', { eventConstructor: 'MouseEvent' });

    cy.get(`#${svgID1} circle`)
      .then(($circle) => {
        const newPositionX = $circle.attr('cx');
        const newPositionY = $circle.attr('cy');

        cy.checkComment({
          svgID: svgID1,
          commentPositionX: newPositionX,
          commentPositionY: newPositionY,
          text: randomText,
          markID: `${svgID1}_svg-text-${commentPositionX}-${commentPositionY}-circle`,
          commentID: `${svgID1}_svg-text-${commentPositionX}-${commentPositionY}`,
          markColor: { active: false, colorClassName: [blueClassName, redClassName] },
        });
      });

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX: newCommentPositionX,
        commentPositionY: newCommentPositionY,
        text: randomText,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}`,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });
  });

  it('On comment clone, No draggable comments mark', () => {
    const svgID1 = 'svg-1';
    const svgID2 = 'svg-3';
    let commentPositionX;
    let commentPositionY;
    let commentID;
    let circleCommentID;

    // create comment
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'image',
      position: { x: 50, y: 30 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID1} textarea`)
      .type(randomText)
      .then(($textare) => {
        commentID = $textare.attr('id');
      });

    cy.get(`#${svgID1} circle`)
      .then(($circle) => {
        commentPositionX = $circle.attr('cx');
        commentPositionY = $circle.attr('cy');
        circleCommentID = $circle.attr('id');

        cy.checkComment({
          svgID: svgID1,
          commentPositionX,
          commentPositionY,
          text: randomText,
          markID: circleCommentID,
          commentID,
          markColor: { active: false, colorClassName: [blueClassName, redClassName] },
        });
      });

    // go to next page, check, change text
    cy.get('.active-slide .nav-right').click('center');
    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle_clone-${svgID1}`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}_clone-${svgID1}`,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    cy.get(`#${svgID2}`)
      .trigger('mousedown', 51, 31, { eventConstructor: 'MouseEvent' })
      .trigger('mousemove', 'center', { eventConstructor: 'MouseEvent' })
      .trigger('mouseup', 'center', { eventConstructor: 'MouseEvent' });

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle_clone-${svgID1}`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}_clone-${svgID1}`,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    // go back, check
    cy.get('.active-slide .nav-left').click('center');

    cy.get(`#${svgID1}`).then(() => {
      cy.checkComment({
        svgID: svgID1,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: circleCommentID,
        commentID,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });
  });

  it('Checking for changing color of a comment mark clone and then change color the comment mark.', () => {
    const svgID1 = 'svg-1';
    const svgID2 = 'svg-3';
    let commentPositionX;
    let commentPositionY;
    let commentID;
    let circleCommentID;

    // create comment
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'image',
      position: { x: 50, y: 30 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID1} textarea`)
      .type(randomText)
      .then(($textare) => {
        commentID = $textare.attr('id');
      });

    cy.get(`#${svgID1} circle`)
      .then(($circle) => {
        commentPositionX = $circle.attr('cx');
        commentPositionY = $circle.attr('cy');
        circleCommentID = $circle.attr('id');

        cy.checkComment({
          svgID: svgID1,
          commentPositionX,
          commentPositionY,
          text: randomText,
          markID: circleCommentID,
          commentID,
          markColor: { active: false, colorClassName: [blueClassName, redClassName] },
        });
      });

    // go to next page, check, change color comments mark
    cy.get('.active-slide .nav-right').click('center');
    cy.wait(100);

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle_clone-${svgID1}`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}_clone-${svgID1}`,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    cy.wait(500);
    cy.useRadialMenu({
      svgID: svgID2,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID],
      clickItem: redColorID,
    });

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}`,
        markColor: { active: true, colorClassName: redClassName },
      });
    });

    // go back, check, change comments mark color
    cy.get('.active-slide .nav-left').click('center');
    cy.wait(100);

    cy.get(`#${svgID1}`).then(() => {
      cy.checkComment({
        svgID: svgID1,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: circleCommentID,
        commentID,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    cy.wait(500);
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID],
      clickItem: blueColorID,
    });

    cy.get(`#${svgID1}`).then(() => {
      cy.checkComment({
        svgID: svgID1,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: circleCommentID,
        commentID,
        markColor: { active: true, colorClassName: blueClassName },
      });
    });

    // go next page, check
    cy.get('.active-slide .nav-right').click('center');
    cy.wait(100);

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}`,
        markColor: { active: true, colorClassName: redClassName },
      });
    });
  });

  it('Checking for the deletion of a comment clone and then deleting the comment.', () => {
    const svgID1 = 'svg-1';
    const svgID2 = 'svg-3';
    let commentPositionX;
    let commentPositionY;
    let commentID;
    let circleCommentID;

    // create comment
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'image',
      position: { x: 50, y: 30 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID1} textarea`)
      .type(randomText)
      .then(($textare) => {
        commentID = $textare.attr('id');
      });

    cy.get(`#${svgID1} circle`)
      .then(($circle) => {
        commentPositionX = $circle.attr('cx');
        commentPositionY = $circle.attr('cy');
        circleCommentID = $circle.attr('id');

        cy.checkComment({
          svgID: svgID1,
          commentPositionX,
          commentPositionY,
          text: randomText,
          markID: circleCommentID,
          commentID,
          markColor: { active: false, colorClassName: [blueClassName, redClassName] },
        });
      });

    // go to next page, check, remove comment
    cy.get('.active-slide .nav-right').click('center');
    cy.wait(100);

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle_clone-${svgID1}`,
        commentID: `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}_clone-${svgID1}`,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    cy.wait(500);
    cy.useRadialMenu({
      svgID: svgID2,
      element: 'textarea',
      position: { x: 50, y: 30 },
      items: [createCommentID, removeItemID],
      clickItem: removeItemID,
    });

    cy.get(`#${svgID2}`).then(() => {
      cy.checkRemovedComment(svgID2);
    });

    // go back, check, remove comment
    cy.get('.active-slide .nav-left').click('center');
    cy.wait(100);

    cy.get(`#${svgID1}`).then(() => {
      cy.checkComment({
        svgID: svgID1,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: circleCommentID,
        commentID,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    cy.wait(500);
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'textarea',
      position: { x: 50, y: 30 },
      items: [createCommentID, removeItemID],
      clickItem: removeItemID,
    });

    cy.get(`#${svgID1}`).then(() => {
      cy.checkRemovedComment(svgID1);
    });

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2}`).then(() => {
      cy.checkRemovedComment(svgID2);
    });
  });

  it('Checking for changing color of a marker clone and then change color the marker.', () => {
    const svgID1 = 'svg-1';
    const svgID2 = 'svg-3';
    const defaultColor = 'blue';
    let markPositionX;
    let markPositionY;
    let markId;

    // create mark
    cy.get(`#${svgID1}`).click('center');

    cy.get(`#${svgID1} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        markPositionX = $circle.attr('cx');
        markPositionY = $circle.attr('cy');
        markId = $circle.attr('id');
      });

    // go to next page, check, change color
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        expect($circle.attr('cx')).equal(markPositionX);
        expect($circle.attr('cy')).equal(markPositionY);
        expect($circle.attr('id')).equal(`${svgID2}_circle-${markPositionX}-${markPositionY}_clone-${svgID1}`);
      });

    cy.useRadialMenu({
      svgID: svgID2,
      element: 'circle',
      position: { x: 50, y: 30 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: redColorID,
    });

    cy.get(`#${svgID2} circle`)
      .should('have.length', 1)
      .should('have.class', redClassName)
      .then(($circle) => {
        expect($circle.attr('cx')).equal(markPositionX);
        expect($circle.attr('cy')).equal(markPositionY);
        expect($circle.attr('id')).equal(`${svgID2}_circle-${markPositionX}-${markPositionY}`);
      });

    // go back, check, change color
    cy.get('.active-slide .nav-left').click('center');
    cy.wait(500);

    cy.get(`#${svgID1} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        expect($circle.attr('id')).equal(markId);
      });

    cy.useRadialMenu({
      svgID: svgID1,
      element: 'circle',
      position: { x: 50, y: 30 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: redColorID,
    });

    cy.get(`#${svgID1} circle`)
      .should('have.length', 1)
      .should('have.class', redClassName)
      .then(($circle) => {
        expect($circle.attr('cx')).equal(markPositionX);
        expect($circle.attr('cy')).equal(markPositionY);
        expect($circle.attr('id')).equal(markId);
      });

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} circle`)
      .should('have.length', 1)
      .should('have.class', redClassName)
      .then(($circle) => {
        expect($circle.attr('cx')).equal(markPositionX);
        expect($circle.attr('cy')).equal(markPositionY);
        expect($circle.attr('id')).equal(`${svgID2}_circle-${markPositionX}-${markPositionY}`);
      });
  });

  it('Checking for the move of a marker clone and then move the marker.', () => {
    const svgID1 = 'svg-1';
    const svgID2 = 'svg-3';
    const defaultColor = 'blue';
    let markPositionX;
    let markPositionY;
    let markId;

    // create mark
    cy.get(`#${svgID1}`).click('center');

    cy.get(`#${svgID1} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        markPositionX = $circle.attr('cx');
        markPositionY = $circle.attr('cy');
        markId = $circle.attr('id');
      });

    // go to next page, check, change color
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        expect($circle.attr('cx')).equal(markPositionX);
        expect($circle.attr('cy')).equal(markPositionY);
        expect($circle.attr('id')).equal(`${svgID2}_circle-${markPositionX}-${markPositionY}_clone-${svgID1}`);
      });

    let newMarkPositionX;
    let newMarkPositionY;

    cy.get(`#${svgID2}`)
      .trigger('mousedown', 'center', { eventConstructor: 'MouseEvent' })
      .trigger('mousemove', 10, 10, { eventConstructor: 'MouseEvent' })
      .trigger('mouseup', 10, 10, { eventConstructor: 'MouseEvent' });

    cy.get(`#${svgID2} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        newMarkPositionX = $circle.attr('cx');
        newMarkPositionY = $circle.attr('cy');
        expect($circle.attr('id')).equal(`${svgID2}_circle-${markPositionX}-${markPositionY}`);
      });

    // go back, check, change color
    cy.get('.active-slide .nav-left').click('center');
    cy.wait(500);

    cy.get(`#${svgID1} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        expect($circle.attr('id')).equal(markId);
      });

    cy.get(`#${svgID1}`)
      .trigger('mousedown', 'center', { eventConstructor: 'MouseEvent' })
      .trigger('mousemove', 50, 10, { eventConstructor: 'MouseEvent' })
      .trigger('mouseup', 50, 10, { eventConstructor: 'MouseEvent' });

    cy.get(`#${svgID1} circle`)
      .should('have.length', 1)
      .should('have.class', defaultColor)
      .then(($circle) => {
        expect($circle.attr('id')).equal(markId);
      });

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} circle`)
      .should('have.length', 1)
      .should('have.class', defaultColor)
      .then(($circle) => {
        expect($circle.attr('cx')).equal(newMarkPositionX);
        expect($circle.attr('cy')).equal(newMarkPositionY);
        expect($circle.attr('id')).equal(`${svgID2}_circle-${markPositionX}-${markPositionY}`);
      });
  });

  it('Checking for the deletion of a marker clone and then deleting the marker.', () => {
    const svgID1 = 'svg-1';
    const svgID2 = 'svg-3';
    const defaultColor = 'blue';
    let markPositionX;
    let markPositionY;
    let markId;

    // create mark
    cy.get(`#${svgID1}`).click('center');

    cy.get(`#${svgID1} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        markPositionX = $circle.attr('cx');
        markPositionY = $circle.attr('cy');
        markId = $circle.attr('id');
      });

    // go to next page, check, change color
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        expect($circle.attr('cx')).equal(markPositionX);
        expect($circle.attr('cy')).equal(markPositionY);
        expect($circle.attr('id')).equal(`${svgID2}_circle-${markPositionX}-${markPositionY}_clone-${svgID1}`);
      });

    cy.get(`#${svgID2} circle`)
      .click('center')
      .should('have.length', 0);

    // go back, check, change color
    cy.get('.active-slide .nav-left').click('center');
    cy.wait(500);

    cy.get(`#${svgID1} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        expect($circle.attr('id')).equal(markId);
      });

    cy.get(`#${svgID1} circle`)
      .click('center')
      .should('have.length', 0);

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} circle`).should('have.length', 0);
  });

  it('Checking the clone color change of the drawing and then changing the drawing color.', () => {
    const scrollableComponentID1 = 'svg-test-1';
    const scrollableComponentID2 = 'svg-test-2';
    const svgID1 = 'svg-2';
    const svgID2 = 'svg-4';
    const defaultColor = 'yellow';
    let xCoord;
    let yCoord;
    let pathID;

    cy.get(`#${scrollableComponentID1}`).scrollTo('bottom');
    cy.wait(100);

    // draw line
    cy.drawDemoLine(svgID1);

    cy.get(`#${svgID1} path`)
      .should('have.length', 1)
      .should('have.class', defaultColor)
      .then(($path) => {
        const pathD = $path.attr('d');
        const dParts = pathD.split(',');
        xCoord = dParts[0].replace('M', '');
        yCoord = dParts[1].replace(/L[0-9]+/g, '');
        pathID = $path.attr('id');
        expect(pathID).equal(`${svgID1}_path-${xCoord}-${yCoord}`);
      });

    cy.get(`#${scrollableComponentID1}`).scrollTo('top');

    // go to next page, check, change color
    cy.get('.active-slide .nav-right').click('center');
    cy.get(`#${scrollableComponentID2}`).scrollTo('bottom');
    cy.wait(100);

    cy.get(`#${svgID2} path`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($path) => {
        expect($path.attr('id')).equal(`${svgID2}_path-${xCoord}-${yCoord}_clone-${svgID1}`);
      });

    cy.useRadialMenu({
      svgID: svgID2,
      element: 'path',
      position: { x: 150, y: 150 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: blueColorID,
    });

    cy.get(`#${svgID2} path`)
      .should('have.length', 1)
      .and('have.class', blueClassName)
      .then(($path) => {
        expect($path.attr('id')).equal(`${svgID2}_path-${xCoord}-${yCoord}`);
      });

    // go back, check, change color
    cy.get(`#${scrollableComponentID2}`).scrollTo('top');
    cy.get('.active-slide .nav-left').click('center');

    cy.get(`#${scrollableComponentID1}`).scrollTo('bottom');
    cy.wait(500);

    cy.get(`#${svgID1} path`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($path) => {
        expect($path.attr('id')).equal(pathID);
      });

    cy.useRadialMenu({
      svgID: svgID1,
      element: 'path',
      position: { x: 150, y: 150 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: redColorID,
    });

    cy.get(`#${svgID1} path`)
      .should('have.length', 1)
      .and('have.class', redClassName)
      .then(($path) => {
        expect($path.attr('id')).equal(pathID);
      });

    cy.get(`#${scrollableComponentID1}`).scrollTo('top');

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');
    cy.get(`#${scrollableComponentID1}`).scrollTo('bottom');
    cy.wait(100);

    cy.get(`#${svgID2} path`)
      .should('have.length', 1)
      .and('have.class', blueClassName)
      .then(($path) => {
        expect($path.attr('id')).equal(`${svgID2}_path-${xCoord}-${yCoord}`);
      });
  });

  it('Checking the deletion of a drawing clone and then deleting the drawing.', () => {
    const scrollableComponentID1 = 'svg-test-1';
    const scrollableComponentID2 = 'svg-test-2';
    const svgID1 = 'svg-2';
    const svgID2 = 'svg-4';
    const defaultColor = 'yellow';
    let xCoord;
    let yCoord;
    let pathID;

    cy.get(`#${scrollableComponentID1}`).scrollTo('bottom');
    cy.wait(100);

    // draw line
    cy.drawDemoLine(svgID1);

    cy.get(`#${svgID1} path`)
      .should('have.length', 1)
      .should('have.class', defaultColor)
      .then(($path) => {
        const pathD = $path.attr('d');
        const dParts = pathD.split(',');
        xCoord = dParts[0].replace('M', '');
        yCoord = dParts[1].replace(/L[0-9]+/g, '');
        pathID = $path.attr('id');
        expect(pathID).equal(`${svgID1}_path-${xCoord}-${yCoord}`);
      });

    cy.get(`#${scrollableComponentID1}`).scrollTo('top');

    // go to next page, check, remove drawing
    cy.get('.active-slide .nav-right').click('center');
    cy.get(`#${scrollableComponentID2}`).scrollTo('bottom');
    cy.wait(100);

    cy.get(`#${svgID2} path`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($path) => {
        expect($path.attr('id')).equal(`${svgID2}_path-${xCoord}-${yCoord}_clone-${svgID1}`);
      });

    cy.useRadialMenu({
      svgID: svgID2,
      element: 'path',
      position: { x: 150, y: 150 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: removeItemID,
    });

    cy.get(`#${svgID2} path`).should('have.length', 0);

    // go back, check, remove drawing
    cy.get(`#${scrollableComponentID2}`).scrollTo('top');
    cy.get('.active-slide .nav-left').click('center');

    cy.get(`#${scrollableComponentID1}`).scrollTo('bottom');
    cy.wait(500);

    cy.get(`#${svgID1} path`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($path) => {
        expect($path.attr('id')).equal(pathID);
      });

    cy.useRadialMenu({
      svgID: svgID1,
      element: 'path',
      position: { x: 150, y: 150 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: removeItemID,
    });

    cy.get(`#${svgID1} path`).should('have.length', 0);

    cy.get(`#${scrollableComponentID1}`).scrollTo('top');

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');
    cy.get(`#${scrollableComponentID2}`).scrollTo('bottom');
    cy.wait(100);

    cy.get(`#${svgID2} path`).should('have.length', 0);
  });

  it('Duplication check for creating, changing color, moving and deleting a marker.', () => {
    const svgID1 = 'svg-1';
    const svgID2 = 'svg-3';
    const defaultColor = 'blue';
    let markPositionX;
    let markPositionY;
    let markId;

    // create mark
    cy.get(`#${svgID1}`).click('center');

    cy.get(`#${svgID1} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        markPositionX = $circle.attr('cx');
        markPositionY = $circle.attr('cy');
        markId = $circle.attr('id');
      });

    // go to next page, check, go back
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} circle`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($circle) => {
        expect($circle.attr('cx')).equal(markPositionX);
        expect($circle.attr('cy')).equal(markPositionY);
        const idParts = markId.split('_');
        expect($circle.attr('id')).equal(`${svgID2}_${idParts[1]}_clone-${svgID1}`);
      });

    cy.get('.active-slide .nav-left').click('center');

    // change color
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: redColorID,
    });

    cy.get(`#${svgID1} circle`).should('have.class', redClassName);

    // go to next page, check, go back
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} circle`)
      .should('have.length', 1)
      .and('have.class', redClassName)
      .then(($circle) => {
        expect($circle.attr('cx')).equal(markPositionX);
        expect($circle.attr('cy')).equal(markPositionY);
        const idParts = markId.split('_');
        expect($circle.attr('id')).equal(`${svgID2}_${idParts[1]}_clone-${svgID1}`);
      });

    cy.get('.active-slide .nav-left').click('center');

    // move mark
    let newMarkPositionX;
    let newMarkPositionY;

    cy.get(`#${svgID1}`)
      .trigger('mousedown', 'center', { eventConstructor: 'MouseEvent' })
      .trigger('mousemove', 10, 10, { eventConstructor: 'MouseEvent' })
      .trigger('mouseup', 10, 10, { eventConstructor: 'MouseEvent' });

    cy.get(`#${svgID1} circle`).then(($circle) => {
      newMarkPositionX = $circle.attr('cx');
      newMarkPositionY = $circle.attr('cy');
      expect($circle.attr('id')).equal(markId);
    });

    // go to next page, check, go back
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} circle`)
      .should('have.length', 1)
      .and('have.class', redClassName)
      .then(($circle) => {
        expect($circle.attr('cx')).equal(newMarkPositionX);
        expect($circle.attr('cy')).equal(newMarkPositionY);
        const idParts = markId.split('_');
        expect($circle.attr('id')).equal(`${svgID2}_${idParts[1]}_clone-${svgID1}`);
      });

    cy.get('.active-slide .nav-left').click('center');

    // remove mark
    cy.get(`#${svgID1} circle`)
      .click()
      .should('have.length', 0);

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2} circle`).should('have.length', 0);
  });

  it('Duplication check for creating, changing color and deleting a marker.', () => {
    const scrollableComponentID = 'svg-test-1';
    const svgID1 = 'svg-2';
    const svgID2 = 'svg-4';
    const defaultColor = 'yellow';
    let xCoord;
    let yCoord;
    let pathID;
    let dupPathID;

    cy.get(`#${scrollableComponentID}`).scrollTo('bottom');

    // draw line
    cy.drawDemoLine(svgID1);

    cy.get(`#${svgID1} path`)
      .should('have.length', 1)
      .should('have.class', defaultColor)
      .then(($path) => {
        const pathD = $path.attr('d');
        const dParts = pathD.split(',');
        xCoord = dParts[0].replace('M', '');
        yCoord = dParts[1].replace(/L[0-9]+/g, '');
        pathID = $path.attr('id');
        expect(pathID).equal(`${svgID1}_path-${xCoord}-${yCoord}`);
      });

    cy.get(`#${scrollableComponentID}`).scrollTo('top');

    // go to next page, check, go back
    cy.get('.active-slide .nav-right').click('center');
    cy.get(`#${scrollableComponentID}`).scrollTo('bottom');

    cy.get(`#${svgID2} path`)
      .should('have.length', 1)
      .and('have.class', defaultColor)
      .then(($path) => {
        dupPathID = $path.attr('id');
        expect(dupPathID).equal(`${svgID2}_path-${xCoord}-${yCoord}_clone-${svgID1}`);
      });

    cy.get(`#${scrollableComponentID}`).scrollTo('top');
    cy.get('.active-slide .nav-left').click('center');

    cy.get(`#${scrollableComponentID}`).scrollTo('bottom');

    // change color on blue
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'path',
      position: { x: 150, y: 150 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: blueColorID,
    });

    cy.get(`#${svgID1} path`)
      .should('have.length', 1)
      .and('have.class', blueClassName)
      .then(($path) => {
        expect($path.attr('id')).equal(pathID);
      });

    cy.get(`#${scrollableComponentID}`).scrollTo('top');

    // go to next page, check, go back
    cy.get('.active-slide .nav-right').click('center');
    cy.get(`#${scrollableComponentID}`).scrollTo('bottom');

    cy.get(`#${svgID2} path`)
      .should('have.length', 1)
      .and('have.class', blueClassName)
      .then(($path) => {
        expect($path.attr('id')).equal(dupPathID);
      });

    cy.get(`#${scrollableComponentID}`).scrollTo('top');
    cy.get('.active-slide .nav-left').click('center');

    cy.get(`#${scrollableComponentID}`).scrollTo('bottom');

    // remove path
    cy.get(`#${svgID1} path`)
      .click('topLeft') // must be on path
      .should('have.length', 0);

    cy.get(`#${scrollableComponentID}`).scrollTo('top');

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');
    cy.get(`#${scrollableComponentID}`).scrollTo('bottom');

    cy.get(`#${svgID2} path`).should('have.length', 0);
  });

  it('Duplication check for creating, changing comments marker color, changing text, moving and deleting a comment.', () => {
    const svgID1 = 'svg-1';
    const svgID2 = 'svg-3';
    let commentPositionX;
    let commentPositionY;
    let commentID;
    let circleCommentID;
    let dupCommentID;
    let dupCircleCommentID;

    // create comment
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'image',
      position: { x: 10, y: 20 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID1} textarea`)
      .type(randomText)
      .then(($textare) => {
        commentID = $textare.attr('id');
      });

    cy.get(`#${svgID1} circle`)
      .then(($circle) => {
        commentPositionX = $circle.attr('cx');
        commentPositionY = $circle.attr('cy');
        circleCommentID = $circle.attr('id');
        // calculate dup ids
        dupCircleCommentID = `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}-circle_clone-${svgID1}`;
        dupCommentID = `${svgID2}_svg-text-${commentPositionX}-${commentPositionY}_clone-${svgID1}`;

        cy.checkComment({
          svgID: svgID1,
          commentPositionX,
          commentPositionY,
          text: randomText,
          markID: circleCommentID,
          commentID,
          markColor: { active: false, colorClassName: [blueClassName, redClassName] },
        });
      });

    // go to next page, check, go back
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: dupCircleCommentID,
        commentID: dupCommentID,
        markColor: { active: false, colorClassName: [blueClassName, redClassName] },
      });
    });

    cy.get('.active-slide .nav-left').click('center');

    // change comments mark color
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID],
      clickItem: redColorID,
    });

    cy.get(`#${svgID1}`).then(() => {
      cy.checkComment({
        svgID: svgID1,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: circleCommentID,
        commentID,
        markColor: { active: true, colorClassName: redClassName },
      });
    });

    // go to next page, check, go back
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: randomText,
        markID: dupCircleCommentID,
        commentID: dupCommentID,
        markColor: { active: true, colorClassName: redClassName },
      });
    });

    cy.get('.active-slide .nav-left').click('center');

    // change text
    cy.get(`#${svgID1} textarea`)
      .type(randomText)
      .blur();

    cy.get(`#${svgID1}`).then(() => {
      cy.checkComment({
        svgID: svgID1,
        commentPositionX,
        commentPositionY,
        text: `${randomText}${randomText}`,
        markID: circleCommentID,
        commentID,
        markColor: { active: true, colorClassName: redClassName },
      });
    });

    // go to next page, check, go back
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX,
        commentPositionY,
        text: `${randomText}${randomText}`,
        markID: dupCircleCommentID,
        commentID: dupCommentID,
        markColor: { active: true, colorClassName: redClassName },
      });
    });

    cy.get('.active-slide .nav-left').click('center');

    // move comment
    let newCommentPositionX;
    let newCommentPositionY;

    cy.get(`#${svgID1}`)
      .trigger('mousedown', 15, 25, { eventConstructor: 'MouseEvent' })
      .trigger('mousemove', 'center', { eventConstructor: 'MouseEvent' })
      .trigger('mouseup', 'center', { eventConstructor: 'MouseEvent' });

    cy.get(`#${svgID1} circle`)
      .then(($circle) => {
        newCommentPositionX = $circle.attr('cx');
        newCommentPositionY = $circle.attr('cy');
      });

    cy.get(`#${svgID1}`).then(() => {
      cy.checkComment({
        svgID: svgID1,
        commentPositionX: newCommentPositionX,
        commentPositionY: newCommentPositionY,
        text: `${randomText}${randomText}`,
        markID: circleCommentID,
        commentID,
        markColor: { active: true, colorClassName: redClassName },
      });
    });

    // go to next page, check, go back
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2}`).then(() => {
      cy.checkComment({
        svgID: svgID2,
        commentPositionX: newCommentPositionX,
        commentPositionY: newCommentPositionY,
        text: `${randomText}${randomText}`,
        markID: dupCircleCommentID,
        commentID: dupCommentID,
        markColor: { active: true, colorClassName: redClassName },
      });
    });

    cy.get('.active-slide .nav-left').click('center');

    // remove comment
    cy.useRadialMenu({
      svgID: svgID1,
      element: 'textarea',
      position: { x: 15, y: 25 },
      items: [createCommentID, removeItemID],
      clickItem: removeItemID,
    });

    cy.get(`#${svgID1}`).then(() => {
      cy.checkRemovedComment(svgID1);
    });

    // go to next page, check
    cy.get('.active-slide .nav-right').click('center');

    cy.get(`#${svgID2}`).then(() => {
      cy.checkRemovedComment(svgID2);
    });
  });
});
