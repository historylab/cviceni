describe('Test marks, comments and drawing features', () => {
  const createCommentID = '#create-text';
  const removeItemID = '#remove-content';
  const activeItemClass = 'is-active';
  const radialItemClass = 'radial-menu-item';
  const blueClassName = 'blue';
  const redClassName = 'red';

  const invalidText = 'a';
  const randomText = 'Testovací';

  beforeEach(() => {
    // Go to welcome page of special cviceni for testing
    cy.visit('/svg-test/index.html');
    // Go to first page of cviceni
    cy.get('.content > .button').click();
  });

  it('Create a comment and move it.', () => {
    const svgID = 'svg-1';
    let commentPositionX;
    let commentPositionY;

    // create comment
    cy.useRadialMenu({
      svgID,
      element: 'image',
      position: { x: 15, y: 15 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID} textarea`).type(randomText);

    cy.get(`#${svgID} circle`)
      .then(($circle) => {
        commentPositionX = $circle.attr('cx');
        commentPositionY = $circle.attr('cy');

        cy.checkComment({
          svgID,
          commentPositionX,
          commentPositionY,
          text: randomText,
          markID: `${svgID}_svg-text-${commentPositionX}-${commentPositionY}-circle`,
          commentID: `${svgID}_svg-text-${commentPositionX}-${commentPositionY}`,
          markColor: { active: false, colorClassName: [blueClassName, redClassName] },
        });
      });

    // move comment
    cy.get(`#${svgID}`)
      .trigger('mousedown', 15, 15, { eventConstructor: 'MouseEvent' })
      .trigger('mousemove', 'center', { eventConstructor: 'MouseEvent' })
      .trigger('mouseup', 'center', { eventConstructor: 'MouseEvent' });

    cy.get(`#${svgID} circle`)
      .then(($circle) => {
        const newCommentPositionX = $circle.attr('cx');
        const newCommentPositionY = $circle.attr('cy');

        cy.checkComment({
          svgID,
          commentPositionX: newCommentPositionX,
          commentPositionY: newCommentPositionY,
          text: randomText,
          markID: `${svgID}_svg-text-${commentPositionX}-${commentPositionY}-circle`,
          commentID: `${svgID}_svg-text-${commentPositionX}-${commentPositionY}`,
          markColor: { active: false, colorClassName: [blueClassName, redClassName] },
        });
      });
  });

  it('A comment is automatically deleted when it is empty.', () => {
    const svgID = 'svg-1';

    // create comment
    cy.useRadialMenu({
      svgID,
      element: 'image',
      position: { x: 200, y: 200 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID} textarea`)
      .blur() // blur comment
      .should('have.length', 0);
    cy.get('#svg-1 circle').should('have.length', 0);
  });

  it('The comment is automatically marked as invalid after blur if it is a short string.', () => {
    const svgID = 'svg-1';

    // create comment
    cy.useRadialMenu({
      svgID,
      element: 'image',
      position: { x: 200, y: 200 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID} textarea`)
      .type(invalidText)
      .blur() // blur comment
      .should('have.class', 'is-wrong');

    cy.get(`#${svgID} circle`).should('have.length', 1);
  });

  it('Check the correct display and orientation of the comment boxes in the corners and center of the image.', () => {
    // important for whole picture on window - viewport specific
    cy.get('#svg-test-1').scrollTo(0, 170);

    // right top
    cy.get('#svg-1').rightclick('topLeft');
    cy.get(`.${radialItemClass}.${activeItemClass}`).should('have.length', 1);
    cy.get(createCommentID).should('have.class', activeItemClass);

    cy.get(createCommentID).click();
    cy.get('#svg-1 textarea').eq(0).type(randomText);
    cy.get('#svg-1 foreignObject').eq(0).should('have.attr', 'data-svg-text-orientation', 'right, bottom');

    // left top
    cy.get('#svg-1').rightclick('topRight');
    cy.get(`.${radialItemClass}.${activeItemClass}`).should('have.length', 1);
    cy.get(createCommentID).should('have.class', activeItemClass);

    cy.get(createCommentID).click();
    cy.get('#svg-1 textarea').eq(1).type(randomText);
    cy.get('#svg-1 foreignObject').eq(1).should('have.attr', 'data-svg-text-orientation', 'left, bottom');

    // left bottom
    cy.get('#svg-1').rightclick('bottomRight');
    cy.get(`.${radialItemClass}.${activeItemClass}`).should('have.length', 1);
    cy.get(createCommentID).should('have.class', activeItemClass);

    cy.get(createCommentID).click();
    cy.get('#svg-1 textarea').eq(2).type(randomText);
    cy.get('#svg-1 foreignObject').eq(2).should('have.attr', 'data-svg-text-orientation', 'left, top');

    // right bottom
    cy.get('#svg-1').rightclick('bottomLeft');
    cy.get(`.${radialItemClass}.${activeItemClass}`).should('have.length', 1);
    cy.get(createCommentID).should('have.class', activeItemClass);

    cy.get(createCommentID).click();
    cy.get('#svg-1 textarea').eq(3).type(randomText);
    cy.get('#svg-1 foreignObject').eq(3).should('have.attr', 'data-svg-text-orientation', 'right, top');

    // center
    cy.get('#svg-1').rightclick('center');
    cy.get(`.${radialItemClass}.${activeItemClass}`).should('have.length', 1);
    cy.get(createCommentID).should('have.class', activeItemClass);

    cy.get(createCommentID).click();
    cy.get('#svg-1 textarea').eq(4).type(randomText);
    cy.get('#svg-1 foreignObject').eq(4).should('have.attr', 'data-svg-text-orientation', 'right, bottom');
  });

  it('When you click on a comment, it automatically moves above the other comments with its mark.', () => {
    const svgID = 'svg-1';
    let textareaID1;
    let circleID1;
    let textareaID2;
    let circleID2;

    // create comment 1
    cy.useRadialMenu({
      svgID,
      element: 'image',
      position: { x: 15, y: 15 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID} textarea`)
      .type(randomText)
      .should('have.length', 1)
      .then(($textarea) => {
        textareaID1 = $textarea.attr('id');
      });

    cy.get(`#${svgID} circle`)
      .should('have.length', 1)
      .then(($circle) => {
        circleID1 = $circle.attr('id');
      });

    // check positions
    cy.get(`#${svgID} g > *`).eq(-1).then(($elem) => {
      expect($elem.attr('id')).equal(circleID1);
    });
    cy.get(`#${svgID} g > *`).eq(-2).then(($elem) => {
      expect($elem.children().eq(0).attr('id')).equal(textareaID1);
    });

    // create comment 2
    cy.useRadialMenu({
      svgID,
      element: 'image',
      position: { x: 50, y: 40 },
      items: [createCommentID, removeItemID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID} textarea`).eq(1)
      .type(randomText)
      .then(($textarea) => {
        textareaID2 = $textarea.attr('id');
      });

    cy.get(`#${svgID} circle`).eq(1)
      .then(($circle) => {
        circleID2 = $circle.attr('id');
      });

    // check positions
    cy.get(`#${svgID} g > *`).eq(-1).then(($elem) => {
      expect($elem.attr('id')).equal(circleID2);
    });
    cy.get(`#${svgID} g > *`).eq(-2).then(($elem) => {
      expect($elem.children().eq(0).attr('id')).equal(textareaID2);
    });

    // click first comment
    cy.get(`#${svgID} textarea`).eq(0).click();

    cy.get(`#${svgID} g > *`).eq(-1).then(($elem) => {
      expect($elem.attr('id')).equal(circleID1);
    });
    cy.get(`#${svgID} g > *`).eq(-2).then(($elem) => {
      expect($elem.children().eq(0).attr('id')).equal(textareaID1);
    });

  });

  it('Create, move and delete a mark.', () => {
    const svgID = 'svg-1';
    let markPositionX;
    let markPositionY;

    // create mark
    cy.get(`#${svgID}`).click('center');

    cy.get(`#${svgID} circle`)
      .should('have.length', 1)
      .should('have.class', 'blue')
      .then(($circle) => {
        markPositionX = $circle.attr('cx');
        markPositionY = $circle.attr('cy');

        expect($circle.attr('id')).equal(`${svgID}_circle-${markPositionX}-${markPositionY}`);
      });

    // move mark
    cy.get(`#${svgID}`)
      .trigger('mousedown', 'center', { eventConstructor: 'MouseEvent' })
      .trigger('mousemove', 10, 10, { eventConstructor: 'MouseEvent' })
      .trigger('mouseup', 10, 10, { eventConstructor: 'MouseEvent' });

    cy.get(`#${svgID} circle`).then(($circle) => {
      const newMarkPositionX = $circle.attr('cx');
      const newMarkPositionY = $circle.attr('cy');

      expect(newMarkPositionX).not.equal(markPositionX);
      expect(newMarkPositionY).not.equal(markPositionY);

      expect(newMarkPositionX).equal('19'); // specific for viewport
      expect(newMarkPositionY).equal('17'); // specific for viewport
    });

    // remove mark
    cy.get(`#${svgID} circle`).click();
    cy.get(`#${svgID} circle`).should('have.length', 0);
  });

  it('Create, move and delete a drawing.', () => {
    const svgID = 'svg-2';
    cy.get('#svg-test-1').scrollTo('bottom');

    // draw line
    cy.drawDemoLine(svgID);

    cy.get(`#${svgID} path`)
      .should('have.length', 1)
      .should('have.class', 'yellow')
      .then(($path) => {
        const pathD = $path.attr('d');
        const dParts = pathD.split(',');
        const xCoord = dParts[0].replace('M', '');
        const yCoord = dParts[1].replace(/L[0-9]+/g, '');

        expect($path.attr('id')).equal(`${svgID}_path-${xCoord}-${yCoord}`);
      });

    // remove drawing
    cy.get(`#${svgID} path`).click('topLeft'); // must be on path
    cy.get(`#${svgID} path`).should('have.length', 0);
  });
});
