describe('Radial menu tests', () => {
  const createCommentID = '#create-text';
  const blueColorID = '#color-point-blue';
  const redColorID = '#color-point-red';
  const removeItemID = '#remove-content';

  const blueClassName = 'blue';
  const redClassName = 'red';

  const randomText = 'Testovací';

  beforeEach(() => {
    // Go to welcome page of special cviceni for testing
    cy.visit('/svg-test/index.html');
    // Go to first page of cviceni
    cy.get('.content > .button').click();
  });

  it('Create a comment on the image and a comment on that comment.', () => {
    const svgID = 'svg-1';

    // create comment
    cy.useRadialMenu({
      svgID,
      element: 'image',
      position: { x: 100, y: 100 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID} textarea`).type(randomText);

    // create comment
    cy.useRadialMenu({
      svgID,
      element: 'textarea',
      items: [createCommentID, removeItemID],
      clickItem: createCommentID,
    });

    cy.get('#svg-1 textarea').should('have.length', 2);
    cy.get('#svg-1 circle').should('have.length', 2);
  });

  it('Create a comment on the tag and delete it.', () => {
    const svgID = 'svg-1';

    // create mark
    cy.get(`#${svgID}`).click('center');
    cy.get(`#${svgID} circle`).should('have.length', 1);

    // create comment
    cy.useRadialMenu({
      svgID,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID} textarea`)
      .type(randomText)
      .should('have.length', 1);
    cy.get(`#${svgID} circle`).should('have.length', 2);

    // remove comment
    cy.useRadialMenu({
      svgID,
      element: 'textarea',
      items: [createCommentID, removeItemID],
      clickItem: removeItemID,
    });

    cy.get(`#${svgID} textarea`).should('have.length', 0);
    cy.get(`#${svgID} circle`).should('have.length', 1);
  });

  it('Change the marker color and delete it.', () => {
    const svgID = 'svg-1';

    // create mark
    cy.get(`#${svgID}`).click('center');
    cy.get(`#${svgID} circle`)
      .should('have.length', 1)
      .should('have.class', blueClassName);
    // change color on same color
    cy.useRadialMenu({
      svgID,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: blueColorID,
    });
    cy.get(`#${svgID} circle`)
      .should('have.length', 1)
      .should('have.class', blueClassName);

    // change color on different color
    cy.useRadialMenu({
      svgID,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: redColorID,
    });
    cy.get(`#${svgID} circle`)
      .should('have.length', 1)
      .should('have.class', redClassName);

    // remove mark
    cy.useRadialMenu({
      svgID,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: removeItemID,
    });
    cy.get(`#${svgID} circle`).should('have.length', 0);
  });

  it('Create a comment on the image. Change the color of the comment marker and create a comment on the comment marker.', () => {
    const svgID = 'svg-1';

    // create comment
    cy.useRadialMenu({
      svgID,
      element: 'image',
      position: { x: 100, y: 100 },
      items: [createCommentID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID} textarea`)
      .should('have.length', 1)
      .type(randomText);
    cy.get(`#${svgID} circle`).should('have.length', 1);

    // change color of comment's mark on blue
    cy.useRadialMenu({
      svgID,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID],
      clickItem: blueColorID,
    });
    cy.get(`#${svgID} circle`).should('have.class', blueClassName);

    // change color of comment's mark on red
    cy.useRadialMenu({
      svgID,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID],
      clickItem: redColorID,
    });
    cy.get(`#${svgID} circle`).should('have.class', redClassName);

    // create text on comment's mark
    cy.useRadialMenu({
      svgID,
      element: 'circle',
      items: [createCommentID, blueColorID, redColorID],
      clickItem: createCommentID,
    });
    cy.get(`#${svgID} textarea`).should('have.length', 2);
    cy.get(`#${svgID} circle`).should('have.length', 2);
  });

  it('Change the colour of the drawing. Create a comment on the drawing and delete the drawing.', () => {
    const svgID = 'svg-2';
    cy.get('#svg-test-1').scrollTo('bottom');

    // draw line
    cy.drawDemoLine(svgID);

    cy.get('#svg-2 path')
      .should('have.length', 1)
      .and('have.class', 'yellow');

    // change color on blue
    cy.useRadialMenu({
      svgID,
      element: 'path',
      position: { x: 150, y: 150 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: blueColorID,
    });

    cy.get(`#${svgID} path`)
      .should('have.length', 1)
      .and('have.class', blueClassName);

    // change color on red
    cy.useRadialMenu({
      svgID,
      element: 'path',
      position: { x: 150, y: 150 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: redColorID,
    });

    cy.get(`#${svgID} path`)
      .should('have.length', 1)
      .and('have.class', redClassName);

    // create comment
    cy.useRadialMenu({
      svgID,
      element: 'path',
      position: { x: 150, y: 150 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: createCommentID,
    });

    cy.get(`#${svgID} textarea`).should('have.length', 1);
    cy.get(`#${svgID} circle`).should('have.length', 1);
    // remove comment by blur
    cy.get(`#${svgID} textarea`)
      .blur()
      .should('have.length', 0);
    cy.get(`#${svgID} circle`).should('have.length', 0);

    // remove path
    cy.useRadialMenu({
      svgID,
      element: 'path',
      position: { x: 150, y: 150 },
      items: [createCommentID, blueColorID, redColorID, removeItemID],
      clickItem: removeItemID,
    });
    cy.get(`#${svgID} path`).should('have.length', 0);
  });
});
