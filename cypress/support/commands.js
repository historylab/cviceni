// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

// markColor is object of defining
Cypress.Commands.add('checkComment', ({
  svgID, commentPositionX, commentPositionY, text, markID, commentID, markColor,
}) => {
  cy.get(`#${svgID} circle`)
    .should('have.length', 1)
    .then(($circle) => {
      expect($circle.attr('cx')).equal(commentPositionX);
      expect($circle.attr('cy')).equal(commentPositionY);
      expect($circle.attr('id')).equal(markID);
    });

  // check mark color
  if (markColor.active) {
    cy.get(`#${svgID} circle`).should('have.class', markColor.colorClassName);
  }
  else {
    cy.get(`#${svgID} circle`)
      .should('not.have.class', markColor.colorClassName[0])
      .and('not.have.class', markColor.colorClassName[1]);
  }

  cy.get(`#${svgID} foreignObject`)
    .should('have.length', 1)
    .then(($fo) => {
      expect($fo.attr('x')).equal(commentPositionX);
      expect($fo.attr('y')).equal(commentPositionY);
    });

  cy.get(`#${svgID} textarea`)
    .should('have.length', 1)
    .and('have.value', text)
    .then(($textarea) => {
      expect($textarea.attr('id')).equal(commentID);
    });
});

Cypress.Commands.add('checkRemovedComment', (svgID) => {
  cy.get(`#${svgID} textarea`).should('have.length', 0);
  cy.get(`#${svgID} foreignObject`).should('have.length', 0);
  cy.get(`#${svgID} circle`).should('have.length', 0);
});

Cypress.Commands.add('useRadialMenu', ({
  svgID, element, position, items, clickItem, elementID,
}) => {
  const activeItemClass = 'is-active';
  const radialItemClass = 'radial-menu-item';

  switch (element) {
    case 'textarea':
      cy.get(`#${svgID} textarea`).rightclick('center');
      break;
    case 'circle':
      cy.get(`#${svgID} circle`).rightclick('center');
      break;
    case 'image':
      cy.get(`#${svgID}`).rightclick(position.x, position.y);
      break;
    case 'path':
      cy.get(`#${svgID}`)
        .trigger('contextmenu', {
          clientX: position.x, clientY: position.y, eventConstructor: 'MouseEvent', scrollBehavior: false,
        });
      break;
    case 'tag':
      cy.get(`#${elementID}`).rightclick('center');
      break;
    default:
      Cypress.log({
        name: 'Wrong element',
        message: 'failed',
      });
  }

  cy.get(`.${radialItemClass}.${activeItemClass}`).should('have.length', items.length);

  items.forEach((item) => {
    cy.get(item).should('have.class', activeItemClass);
  });

  cy.get(clickItem).click();
  cy.wait(100);
});

Cypress.Commands.add('drawDemoLine', (svgID) => {
  cy.get(`#${svgID}`)
    .trigger('mousedown', { clientX: 100, clientY: 100, eventConstructor: 'MouseEvent' })
    .trigger('mousemove', { clientX: 200, clientY: 200, eventConstructor: 'MouseEvent' })
    .trigger('mousemove', { clientX: 400, clientY: 200, eventConstructor: 'MouseEvent' })
    .trigger('mousemove', { clientX: 300, clientY: 300, eventConstructor: 'MouseEvent' })
    .trigger('mousemove', { clientX: 200, clientY: 300, eventConstructor: 'MouseEvent' })
    .trigger('mouseup', { clientX: 400, clientY: 200, eventConstructor: 'MouseEvent' });
});

Cypress.Commands.add('dragTo', (itemSelect, destinationSelect, position) => {
  cy.get(itemSelect).trigger('mousedown', { eventConstructor: 'MouseEvent' });
  // stupid trick to profit from cypress position oriented click
  // mouse up event is also part of click event
  if (position) {
    cy.get(destinationSelect).click(position, { force: true });
  }
  else {
    cy.get(destinationSelect).click({ force: true });
  }
  cy.wait(100);
});

Cypress.Commands.add('tagCheck', ({
  svgID, elemCount, foCount, itemID, dropItemID,
}) => {
  cy.get(`#${svgID} g > *`).should('have.length', elemCount);
  cy.get(`#${svgID} foreignObject`)
    .should('have.length', foCount)
    .eq(foCount - 1).and('attr', 'id', dropItemID);
  cy.get(`[data-id="${itemID}"]`).should('have.class', 'was-dropped');
});

Cypress.Commands.add('dragAndCheck', ({
  svgID, dropItemID, itemID, elemCount, foCount, position,
}) => {
  // move to position
  cy.dragTo(`[data-id="${itemID}"]`, `#${svgID}`, position);

  // check
  cy.tagCheck({
    svgID, elemCount, foCount, itemID, dropItemID,
  });
});
