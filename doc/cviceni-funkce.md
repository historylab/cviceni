# Obecné info

## Metadata

```json5 
{
  "funkce": [ 
    "lupa", // lupa v pramenech - D3
    "cteni",
    "text",
    "svg", // svg - D3
    "audio", // audio - wavesurfer
    "text-editor", // textEditor - prosemirror
    "kviz",
    "znacky", // svg - D3
    "kresleni", // svg - D3
    "pretahovani", // svg - D3
    "razeni" // razeni - D3
  ]
}
```

## Frame pro nový slide

```json5
{
  "class": [],
  "zadani": {
    "hlavni": "",
    "rozsirujici": ""
  },
  "napoveda": {
    "aktivity": [
      ""
    ],
    "text": ""
  }
}
```

![Frame pro nový slide](./pics/frame.png)

## Slide napůl

```json5
{
  "class": [
    "half-slide-previous"
  ]
}
```

```json5
{
  "class": [
    "half-slide-active"
  ]
}
```

![Frame pro nový slide](./pics/halfSlide.png)

## Slide s max. počtem obrázků v řádku 2

```json5
{
  "class": [
    "col-2"
  ]
}
```

![Frame pro nový slide](./pics/2MaxPic.png)

# Prameny

### Pramen s popiskem

```json5
{
  "prameny": [
    "co-byla-wild-west-show/pic-00-1280w.jpg"
  ],
  "popisky": [
    "Buffalo Bill´s Wild West, kolem 1900. Zdroj: UPM"
  ]
}
```

![Pramen s popiskem](./pics/prameny1.png)

### Pramen s textovým polem podním

```json5
{
  "prameny": [
    "iwojima/pic-00-1280w.jpg"
  ],
  "popisky": [
    ""
  ],
  "uzivatelskyText": {
    "otazky": [
      {
        "id": "text-0",
        "instrukce": "Popis fotografie…",
        "minDelka": 0,
        "maxDelka": 400
      }
    ]
  }
}
```

![Pramen s textovým polem](./pics/prameny2.png)

### Pramen se zpětnou vazbou

```json5
{
  "prameny": [
    "co-se-stalo-na-letisti-croydon/pic-00-1280w.jpg"
  ],
  "zpetnaVazba": {
    "text": "Mám prohlédnuto!"
  },
  "popisky": [
    "Uprchlík na letišti Croydon u Londýna, 31. března 1939. Zdroj: Wiener Library for the Study of the Holocaust and Genocide v Londýně"
  ]
}
```

![Pramen se zp2tnou vazbou](./pics/prameny3.png)

# SVG

## SVG s popisky

```json5
{
  "svg": {
    "soubory": [
      {
        "id": "svg-2",
        // pojmenování od svg-1, svg-2, svg-3
        "soubor": "co-delaji-zeny-na-fotografii/pic-00-1280w.jpg",
        "funkce": [
          "text",
          "kresleni",
          "znacky",
          "komiks"
          // prazdne pro zadnou funkci
        ],
        "barvy": [
          "blue",
          "red"
          // nic pro defaultní barvy (modrá, červená)
        ]
      }
    ]
  }
}
```

| Text                                  | Kreslení                                      |
|---------------------------------------|-----------------------------------------------|
| ![svg_text](./pics/svg_text.png) | ![svg_kresleni](./pics/svg_kresleni.png) |

| Značky                                    | Prazdne                               |
|-------------------------------------------|---------------------------------------|
| ![svg_znacky](./pics/svg_znacky.png) | ![svg_komikś](./pics/svg_prazdne.png) |

## SVG duplikované

```json5
{
  "svg": {
    "soubory": [
      {
        "id": "svg-2",
        // id noveho svg (nepouzivat svg-1-dupl, apod.)
        "soubor": "co-delaji-zeny-na-fotografii/pic-00-1280w.jpg",
        "duplikovat": [
          "svg-1"
          // id duplikovaneno svg
        ],
        "funkce": [
          "text",
          "kresleni"
        ],
        "barvy": [
          "red"
        ]
      }
    ]
  }
}
```

## SVG s komiksem

```json5
{
  "svg": {
    "soubory": [
      {
        "id": "svg-3",
        "soubor": "proc-byli-vysidleni/pic-00-1024h.jpg",
        "funkce": [
          "komiks"
        ],
        "komiks": [
          {
            "kategorie": "text",
            "subkategorie": "promluva",
            "pozice": [
              140,
              360
            ],
            "subjekt": [
              "top",
              "right"
            ]
          },
          {
            "kategorie": "text",
            "subkategorie": "myslenka",
            "pozice": [
              480,
              180
            ],
            "subjekt": [
              "bottom",
              "left"
            ]
          }
        ]
      },
      {
        "id": "svg-4",
        "soubor": "proc-byli-vysidleni/pic-01-1024w.jpg",
        "funkce": [
          //nutné ruční napozicování bubliny
          "text",
          "znacky",
          "komiks"
        ],
        "komiks": [
          {
            "kategorie": "text",
            // textove pole
            "subkategorie": "promluva",
            "pozice": [
              //nutné ruční napozicování bubliny
              375,
              350
            ],
            "subjekt": [
              // pozice ukazatele bubliny
              "top",
              "right"
            ]
          },
          {
            "kategorie": "text",
            // textove pole
            "subkategorie": "myslenka",
            "pozice": [
              //nutné ruční napozicování bubliny
              705,
              175
            ],
            "subjekt": [
              // pozice ukazatele bubliny
              "bottom",
              "left"
            ]
          },
          {
            "kategorie": "znacka",
            // znacka
            "barva": "blue",
            "pozice": [
              61,
              460
            ]
          }
        ]
      }
    ]
  }
}
```

![svg_komikś](./pics/svg_komiks.png)

## SVG s přetahováním

```json5
{
  "svg": {
    "pretahovani": {
      "id": "pretahovani-1",
      "items": [
        {
          "objekt": "lékařské",
          "medium": "tag"
          // uzivatelsky text, obrazek, svg, audio, video, text
        },
        {
          "objekt": "dietní",
          "medium": "tag"
        },
        {
          "objekt": "hygienické",
          "medium": "tag"
        },
        {
          "objekt": "morální/mravní",
          "medium": "tag"
        },
        {
          "objekt": "psychologické",
          "medium": "tag"
        },
        {
          "objekt": "environmentální",
          "medium": "tag"
        },
        {
          "objekt": "kulturní a hodnotové",
          "medium": "tag"
        }
      ]
    },
    "soubory": [
      {
        "id": "svg-1",
        "soubor": "co-je-dobre-pro-zdravi/pic-00-1024h.jpg",
        "funkce": [],
        "drop": true
        // urcuje, jestli sem muze byt polozena polozka ze seznamu
      }
    ]
  }
}
```

![svg_pretahovani](./pics/svg_pretahovani.png)

# Klíčová slova

```json5
{
  "klicovaSlova": {
    "klicovaSlova": [
      {
        "id": "klicova-slova-1",
        "funkce": "wordcloud",
        // selekce-, selekce+, wordcloud
        "tagy": [
          {
            "text": "naši učitelé"
          },
          {
            "text": "nedostatky výchovné práce"
          },
          {
            "text": "destruktivní působení"
          }
        ]
      }
    ]
  }
}
```

| selekce-                              | selekce+                                                         |
|---------------------------------------|------------------------------------------------------------------|
| ![klicovaSlova_selekceMinus](./pics/klicovaSlova_selekceMinus.png) | ![klicovaSlova_selekcePlus](./pics/klicovaSlova_selekcePlus.png) |

| wordcloud                              |
|----------------------------------------|
| ![wordcloud](./pics/klicovaSlova_wordcloud.png) |

# Uživatelský text

```json5
{
  "uzivatelskyText": {
    "layout": "horizontalni",
    // vertikalni nebo nic TODO
    "otazky": [
      {
        "id": "otazka-2",
        "duplikovat": [
          "otazka-1"
          // lze duplikovat jako u svg
        ],
        "zadani": "Má zjištění:",
        "instrukce": "Myslím si, že…",
        "minDelka": 10,
        "maxDelka": 400,
        "vyska": 25
      },
      {
        "id": "otazka-3",
        "zadani": "Moje novinová zpráva:",
        "instrukce": "",
        "minDelka": 10,
        "maxDelka": 999,
        "vyska": 25
      }
    ]
  }
}
```

![uzivatelskyText](./pics/uzivatelskyText.png)

# Řazení

```json5
{
  "razeni": {
    "id": "razeni-1",
    "zpetnaVazba": [
      {
        "podminka": 2,
        // Dvě a více chyb
        "text": "Opravdu? My jsme fotografie uspořádali odlišně.",
        "barva": "color-red"
      },
      {
        "podminka": 1,
        // Přesně jedna chyba
        "text": "Jednu fotografii bychom uspořádali odlišně.",
        "barva": "color-orange"
      },
      {
        "podminka": 0,
        // 0 chyb
        "text": "Výtečně. Uspořádali jsme fotografie stejně.",
        "barva": "color-green"
      }
    ],
    "typ": "horizontalni",
    // TODO
    "objekty": [
      {
        "medium": "audio",
        "objekt": "proc-byli-uneseni/audio-00.mp3",
        "nazev": "Oznámení o zadržení československých občanů v Angole.",
        "spravnaOdpoved": "2"
      },
      {
        "medium": "obrazek",
        "objekt": "dejiny-na-namesti/pic-00-1024h.jpg",
        "popisek": "Zdroj: Archiv DP Praha",
        "spravnaOdpoved": "4"
      },
      {
        "medium": "svg",
        "objekt": {
          "soubor": "co-se-stalo-v-abertamech/pic-03-1024w.jpg",
          "duplikovat": [
            "svg-3"
          ]
        },
        "spravnaOdpoved": "3"
      },
      {
        "medium": "svg",
        "objekt": {
          "soubor": "co-se-stalo-v-abertamech/pic-02-1024w.jpg",
          "duplikovat": [
            "svg-4"
          ]
        },
        "spravnaOdpoved": "1"
      },
      {
        "medium": "text",
        "objekt": "Důkaz urozeného stavu",
        "spravnaOdpoved": "4"
      }
    ]
  }
}
```

![razeni_zpetna_vazba](./pics/razeni_zpetna_vazba.png)

# Výběr

```json5
{
  "vyber": {
    "id": "vyber-1",
    "nastaveni": {
      "layout": "horizontalni",
      "viceOdpovedi": false,
      // radio, checkbox
      "disableFeedback": true
    },
    "objekty": [
      {
        "objekt": "jak-si-pripominame-listopad-na-albertove/pic-02-1280w.jpg"
      },
      {
        "objekt": "jak-si-pripominame-listopad-na-albertove/pic-03-1024h.jpg"
      },
      {
        "objekt": "co-se-dozvime-z-propagandistickych-plakatu/pic-05-1280w.jpg",
        "data": "Proč autor válečného plakátu zvolil jako námět ženu s dítětem a ne zobrazení Hitlera?"
      },
      {
        "medium": "text",
        "objekt": "historické poznání je vždy nepřesné, upravení hypotézy není chyba, ale cesta k poznání"
      },
      {
        "vzhled": "jednoduchy",
        "objekt": "to-byla-slava/audio-01.mp3",
        "label": "1. část",
        "nazev": "Otevření pražského metra"
      }
    ]
  }
}

```

### Radio - "viceOdpovedi": false
![vyber](./pics/vyber.png)

### Checkbox - "viceOdpovedi": true
![vyber_multi](./pics/vyber_multi.png)

# Imitace mapy

```json5
{
  "imitaceMapy": {
    "vrstvy": [
      {
        "nazev": "hustota obyvatel",
        "mapa": "mnichov-1938-jaka-uzemi-byla-odtrzena/pic-00-1280w.jpg"
      },
      {
        "nazev": "podíl pracujících v průmyslu",
        "mapa": "mnichov-1938-jaka-uzemi-byla-odtrzena/pic-04-1280w.jpg"
      },
      {
        "nazev": "podíl věřících",
        "mapa": "mnichov-1938-jaka-uzemi-byla-odtrzena/pic-05-1280w.jpg"
      },
      {
        "nazev": "podíl německého obyvatelstva",
        "mapa": "mnichov-1938-jaka-uzemi-byla-odtrzena/pic-01-1280w.jpg"
      },
      {
        "nazev": "československé opevnění",
        "mapa": "mnichov-1938-jaka-uzemi-byla-odtrzena/pic-02-1280w.jpg"
      }
    ],
    "popisky": [
      "Mapy obyvatelstva českých zemí Československé republiky v roce 1938 (podle sčítání obyvatelstva v roce 1930) a mapa československého opevnění. Zdroj: Martin Váňa (dle dat URRlab). Podklad: Open Street Map"
    ]
  }
}
```

![imitaceMapy](./pics/imitaceMapy.png)

# Textový editor

## Označování pasáží

```json5
{
  "textovyEditor": {
    "texty": [
      {
        "id": 1,
        "text": "text-00",
        "funkce": "zvyraznovani",
        // cteni, predznaceny, zvyraznovani, psani
        "menu": [
          {
            "title": "za co děkovali",
            "commandName": "akter-autori",
            "color": "green"
          },
          {
            "title": "proč chtěli odstoupení",
            "commandName": "akter-duvody",
            "color": "red"
          }
        ]
      }
    ]
  }
}
```

![textEditor_oznacovani](./pics/textEditor_oznacovani.png)

## Čtení

```json5
{
  "textovyEditor": {
    "texty": [
      {
        "id": 1,
        "text": "text-01",
        "funkce": "cteni"
      }
    ]
  }
}
```

![textEditor_cteni](./pics/textEditor_cteni.png)

## Předznačený

```json5
{
  "textovyEditor": {
    "texty": [
      {
        "id": 2,
        "text": "text-00",
        "nazev": "Baudouin",
        "funkce": "predznaceny",
        "predznaceni": [
          {
            "vyraz": "nezávislost Konga byla formována výsledky práce vymyšlené géniem krále Ludvíka II."
          },
          {
            "vyraz": "nechápejme ho jako dobývání, ale jako civilizování"
          },
          {
            "vyraz": "přiblížily zemi světovým rozměrům"
          },
          {
            "vyraz": "Nezpronevěřte se své budoucnosti uspěchanými reformami a nenahrazujte struktury, které vám Belgie dala"
          }
        ]
      }
    ]
  }
}
```

![textEditor_predznaceny](./pics/textEditor_predznaceny.png)

# Testovací kvíz

(společně se zpětnou vazbou)

```json5
{
  "testKviz": {
    "id": "table-testKviz-1",
    "galerie": [
      {
        "soubor": "proc-je-velka-britanie-zemi-caje/pic-05-1024h.jpg"
      }
    ],
    "zpetnaVazba": [
      {
        "podminka": [
          0,
          1,
          2,
        ],
        "text": "V některých závěrech se lišíme.",
        "barva": "color-orange"
      },
      {
        "podminka": [
          3
        ],
        "text": "Výtečně. Došli jsme ke stejným závěrům.",
        "barva": "color-green"
      }
    ],
    "zadani": [
      {
        "otazka": "Sběr čaje byla pouze mužská práce.",
        "spravnaOdpoved": 2,
        "odpovedi": [
          {
            "text": "Správné"
          },
          {
            "text": "Chybné"
          }
        ]
      },
      {
        "otazka": "Rytina zobrazuje srílanské pracovníky v podřízené pozici vůči Britům.",
        "spravnaOdpoved": 1,
        "odpovedi": [
          {
            "text": "Správné"
          },
          {
            "text": "Chybné"
          }
        ]
      },
      {
        "otazka": "Produkce čaje byla založena zejména na ruční práci a využití jednoduchých strojů.",
        "spravnaOdpoved": 1,
        "odpovedi": [
          {
            "text": "Správné"
          },
          {
            "text": "Chybné"
          }
        ]
      }
    ]
  }
}

```

![testKviz](./pics/testKviz.png)

# Nová tabulka

## 1 řádek 2 sloupce

```json5
{
  "novaTabulka": {
    "tabulky": [
      {
        "id": "nova-tabulka-1",
        // ID
        "dataFrom": [
          "textovy-editor-1"
          // ODKUD se berou data do tabulky
        ],
        "columns": [
          {
            "id": "co",
            "name": "Co jsem řekl?",
            "type": {
              "name": "dup-text",
              "type": "tag"
            },
            "bg-color": "",
            "values": []
          },
          {
            "id": "proc",
            "name": "Proč jsem vypovídal?",
            "type": {
              "name": "dup-text",
              "type": "tag"
            },
            "bg-color": "",
            "values": []
          }
        ],
        "rows": [
          {
            "id": "textovy-editor-1",
            "name": "",
            "type": "",
            "bg-color": "",
            "values": []
          }
        ]
      }
    ]
  }
}
```

![novaTabulka_copy](./pics/novaTabulka_copy.png)

## více řádků 1 sloupec

```json5
{
  "novaTabulka": {
    "tabulky": [
      {
        "id": "nova-tabulka-1",
        "dataFrom": [
          "textovy-editor-0",
          "textovy-editor-1"
        ],
        "columns": [
          {
            "id": "uryvky",
            "name": "",
            "type": "",
            "bg-color": "",
            "values": []
          }
        ],
        "rows": [
          {
            "id": "nejedly-patri",
            "name": "Patří do vědy - Nejedlý",
            "type": {
              "name": "dup-text",
              "type": "tag"
            },
            "bg-color": "",
            "values": []
          },
          {
            "id": "pekar-patri",
            "name": "Patří do vědy - Pekař",
            "type": {
              "name": "dup-text",
              "type": "tag"
            },
            "bg-color": "",
            "values": []
          },
          {
            "id": "nejedly-nepatri",
            "name": "Nepatří do vědy - Nejedlý",
            "type": {
              "name": "dup-text",
              "type": "tag"
            },
            "bg-color": "",
            "values": []
          },
          {
            "id": "pekar-nepatri",
            "name": "Nepatří do vědy - Pekař",
            "type": {
              "name": "dup-text",
              "type": "tag"
            },
            "bg-color": "",
            "values": []
          }
        ]
      }
    ]
  }
}
```

![novaTabulka_copy_2](./pics/novaTabulka_copy_2.png)

## Přetahování

```json5
{
  "novaTabulka": {
    "pretahovani": {
      "id": "pretahovani-1",
      "items": [
        {
          "medium": "svg",
          "objekt": {
            "id": "svg-1",
            "soubor": "jak-zobrazovali-svet/pic-01-872h.jpg"
          }
        },
        {
          "medium": "svg",
          "objekt": {
            "id": "svg-2",
            "soubor": "jak-zobrazovali-svet/pic-00-864w.jpg"
          }
        },
        {
          "medium": "svg",
          "objekt": {
            "id": "svg-3",
            "soubor": "jak-zobrazovali-svet/pic-02-1280w.jpg"
          }
        }
      ]
    },
    "tabulky": [
      {
        "id": "nova-tabulka-1",
        "columns": [
          {
            "id": "text-1-col",
            "name": " ",
            "type": "",
            "bg-color": "",
            "values": []
          },
          {
            "id": "text-2-col",
            "name": " ",
            "type": "",
            "bg-color": "",
            "values": []
          },
          {
            "id": "text-3-col",
            "name": " ",
            "type": "",
            "bg-color": "",
            "values": []
          }
        ],
        "rows": [
          {
            "id": "obrazek",
            "name": "",
            "type": {
              "name": "drop",
              "number": "1",
              "type": "obrazek"
            },
            "bg-color": "",
            "values": []
          }
        ]
      }
    ]
  }
}
```

![novaTabulka_pretahovani](./pics/novaTabulka_pretahovani.png)

## Tabulka s copy a možností výběru

```json5
{
  "novaTabulka": {
    "tabulky": [
      {
        "id": "nova-tabulka-1",
        "dataFrom": [
          "textovy-editor-1",
          "textovy-editor-2"
        ],
        "columns": [
          {
            "id": "text",
            "name": "Z textu",
            "type": {
              "name": "nazev",
              "type": "text"
            },
            "bg-color": "",
            "values": []
          },
          {
            "id": "pasaz",
            "name": "Označené pasáže",
            "type": {
              "name": "dup-text",
              "type": "tag"
            },
            "bg-color": "",
            "values": []
          },
          {
            "id": "osoba",
            "name": "Slovesná osoba",
            "type": {
              "name": "select",
              "options": [
                "Vyberte osobu",
                "já",
                "ty",
                "on/ona/ono",
                "my",
                "vy",
                "oni/ony/ona",
                "infinitiv"
              ]
            },
            "bg-color": "",
            "values": []
          },
          {
            "id": "zpusob",
            "name": "Slovesný způsob",
            "type": {
              "name": "select",
              "options": [
                "Vyberte způsob",
                "oznamovací",
                "podmiňovací",
                "rozkazovací",
                "infinitiv"
              ]
            },
            "bg-color": "",
            "values": []
          }
        ]
      }
    ]
  }
}
```

![novaTabulka_vyber](./pics/novaTabulka_vyber.png)

# Audio a Video

Medium jako hlavní položka slidu

```json5
{
  "media": {
    "nastaveni": {
      "vzhled": "normalni",
      "layout": "vertikalni" // horizontalni
    },
    "soubory": [
      {
        "soubor": "vanocni-zmena/audio-00.mp3",
        "nazev": "Projev Antonína Zápotockého z 24. prosinec 1952",
        "popisek": "Zdroj: Archiv ČRo"
      },
      {
        "muted": true,
        "soubor": "videt-a-slyset-srpnovou-okupaci/video-00.mp4",
        "poster": "videt-a-slyset-srpnovou-okupaci/pic-00-800w.jpg",
        "nazev": "Invaze vojsk Varšavské smlouvy do ČSSR",
        "popisek": "Československý filmový týdeník 35/1968. Zdroj: Archiv ČT"
      }
    ]
  }
}
```

![media_audio](./pics/media_audio.png)

## audio nastavení

```json5

{
  "soubor": "vanocni-zmena/audio-00.mp3",
  "nazev": "Projev Antonína Zápotockého z 24. prosinec 1952",
  "popisek": "Zdroj: Archiv ČRo",
  "label": "1. část",
  "prepis": "text-00",
}

```

![audio_all](./pics/audio_all.png)
![audio_prepis](./pics/audio_prepis.png)

### audio nastavení Simple

```json5
{
  "vzhled": "jednoduchy",
  "soubor": "to-byla-slava/audio-01.mp3",
  "label": "1. část",
  "nazev": "Otevření pražského metra",
}
```

![audio_simple](./pics/audio_simple.png)

## video nastavení

(jsou zde i titulky: {typ, jazyk}, ale ještě se nikde nepoužily)

```json5
{
  "muted": true,
  "soubor": "vidiet-a-pocut-augustovu-okupaciu-sk/video-00.mp4",
  "poster": "vidiet-a-pocut-augustovu-okupaciu-sk/pic-00-800w.jpg",
  "nazev": "Invázia vojsk Varšavskej zmluvy do ČSSR",
  "popisek": "Československý filmový týždenník 35/1968. Zdroj: Archív ČT",
},
{
  "soubor": "what-happened-to-czech-fields/video-00.mp4",
  "titulky": {
    "soubor": "what-happened-to-czech-fields/video-00.vtt",
    "jazyk": "pl" //defaultně en
  },
}
```

![video_media](./pics/video_media.png)

# Galerie

Galerie je samostatná jednotka, která se dá přidat k řadě modulů (vkládá se "přímo" do nastavení modulů):

* novaTabulka
* modulEditor
* klicovaSlova
* media
* svg
* testKviz
* uzivatelskyText
* vyber

Př:

```json5
{
  "uzivatelskyText": {
    "layout": "velka-galerie", // "" - pro malou
    "galerie": [
      {
        "soubor": "co-kluci-provedli/pic-04-1280w.jpg",
        "popisek": "Kolínské listy, 31. října 1918. Zdroj: Národní knihovna Praha"
      },
      {
        "soubor": "co-kluci-provedli/pic-05-1024h.jpg",
        "popisek": "Kolínské listy, 31. října 1918. Zdroj: Národní knihovna Praha"
      }
    ],
    "otazky": [
      {
        "id": "otazka-2",
        "zadani": "Doplňte, nebo upravte svou původní odpověď.",
        "instrukce": "Myslím si, že…",
        "minDelka": 2,
        "maxDelka": 400,
        "duplikovat": [
          "otazka-1"
        ]
      }
    ]
  }
}
```

![gallery_basic](./pics/gallery_basic.png)

### Obrázek
```json5
{
  "galerie": [
    {
      "soubor": "co-kluci-provedli/pic-04-1280w.jpg",
      "popisek": "Kolínské listy, 31. října 1918. Zdroj: Národní knihovna Praha"
    }
  ]
}
```

![galerie_obrazek](./pics/galerie_obrazek.png)
### SVG
```json5
{
  "galerie": [
    {
      "typ": "svg",
      "objekt": {
        "id": "svg-5",
        "soubor": "co-pripomina-pomnik/pic-01-1024w.jpg",
        "duplikovat": [
          "svg-4"
        ],
        "funkce": [
          "text",
          "znacky"
        ]
      }
    }
  ]
}
```
![galerie_svg](./pics/galerie_svg.png)
### Basic text
```json5
{
  "galerie": [
    {
      "typ": "text",
      "objekt": "Dočasná Instalace německo-syrského umělce Manafa Halbouniho nazvaná &bdquo;Monument&rdquo; (v překladu pomník, památník) se věnuje válce v Sýrii. Monument byl odhalen 7. února 2017 v Drážďanech na náměstí před slavným kostelem Frauenkirche. Dílo bylo odhaleno několik dní před výročím bombardování Drážďan na konci druhé světové války, během kterého zahynulo přibližně 25 000 obyvatel města. Kostel byl během náletů zničen a později obnoven. Foto: Thomas Skowron. Zdroj: Wikimedia [CC BY (https://creativecommons.org/licenses/by/4.0)]"
    }
  ]
}
```
![galerie_basic_text](./pics/galerie_basic_text.png)
### Uživatelský text
```json5
{
  "galerie": [
    {
      "typ": "text",
      "popisek": "Poznámka ze slajdu č.&thinsp;2",
      "duplikovat": [
        "otazka-1"
      ]
    }
  ]
}
```
![galerie_uzivatelskyText](./pics/galerie_uzivatelskyText.png)
### Video
```json5
{
  "galerie": [
    {
      "soubor": "co-zdarzylo-sie-w-volarach-pl/video-00.mp4",
      "popisek": "Materiał wideo sporządzili żołnierze amerykańscy po zajęciu Volar w maju 1945 r. Źródło: The National Archives and Records Administration"
    }
  ]
}
```
![galerie_video](./pics/galerie_video.png)
### Audio
```json5
{
  "galerie": [
    {
      "soubor": "jak-informovali-o-havarii/audio-00.mp3",
      "nazev": "Zpravodajství Československého rozhlasu, 30. dubna 1986",
      "label": "Nahrávka 1: ČsRo",
      "popisek": "Zdroj: Archiv ČRo"
    }
  ]
}
```
![galerie_audio](./pics/galerie_audio.png)
### Výběr
```json5
{
  "galerie": [
    {
      "typ": "vyber",
      "vyberId": "vyber-1"
    }
  ]
}
```
![galerie_vyber](./pics/galerie_vyber.png)

## Video Stamps
```json5
{
      "zadani": {
        "hlavni": "Podívejte se na videozáznam.",
        "rozsirujici": ""
      },
      "napoveda": {
        "aktivity": [
          "video"
        ],
        "text": "Přehrávání videa se spustí po kliknutí do okna přehrávače. Klikáním do prostoru spuštěného videa dojde k pozastavení přehrávání a následně k jeho dalšímu pokračování. Pod přehrávaným videem se zobrazuje ukazatel průběhu přehrávání. Kliknutím do libovolné části této časové osy se můžete přesunout na požadované místo ve videu."
      },
      "videoStamps": {
        "nastaveni": {
          "layout": "vertikalni",
          "vzhled": "normalni"
        },
        "video": {
          "id": "video-stamps-1",
          "muted": true,
          "soubor": "videt-a-slyset-srpnovou-okupaci/video-00.mp4",
          "poster": "videt-a-slyset-srpnovou-okupaci/pic-00-800w.jpg",
          "nazev": "Invaze vojsk Varšavské smlouvy do ČSSR",
          "popisek": "Československý filmový týdeník 35/1968. Zdroj: Archiv ČT",
          "stamps": [
            {
              "id": 1,
              "emoji":"\uD83D\uDC4D"
            },
            {
              "id": 2,
              "emoji":"\uD83D\uDC4E"
            },
            {
              "id": 3,
              "emoji":"\uD83D\uDE21"
            },
            {
              "id": 4,
              "emoji":"\uD83D\uDC4E"
            }]
        }
      }
    }


```
