# Jak vytvořit nové cvičení

Přepnout se do `dev` branch (`git checkout dev`) a aktualizovat branch z remote (`git pull`). Poté vytvořit z `dev` novou branch ve tvaru `cviceni/{nazev-cviceni}` a checkout do ní (`git checkout -b cviceni/{nazev-cviceni}`).

## Tvorba souborů
`npm run new-cviceni -- --name "Název cvičení"` 
 

- slug (`nazev-cviceni`) je **unikátní** identifikační string používaný v URL. V zásadě jde o slugified ([kebab case](https://en.wikipedia.org/wiki/Kebab_case)) název cvičení (`"Název cvičení"`).

### Struktura

- data: `/src/data/cviceniNazevCviceni.json`
- view: `/src/views/cviceni/nazev-cviceni/index.pug`
- img: `/src/img/nazev-cviceni/pic-00-1280w.jpg`
- audio: `/src/audio/nazev-cviceni/audio-00.mp3`
- text: `/src/text/nazev-cviceni/text-00.html`

### Obrázky

- `72dpi`
- max `1280px` na šířku a `1024px` na výšku
- kvalita `.jpg` stačí na `60-80%`
- pojmenování souboru
    - ve formátu `pic-00-1280w.jpg` pokud je obrázek orientovaný na šířku a má rozměry 1280xNECO nebo změna pojemnování podle odpovídajícího "největšího" rozměru, př.: pic-00-800w pro obrázek 800xNECO
    - ve formátu `pic-00-1024h.jpg` pokud je obrázek orientovaný na výšku a má rozměry NECOx1024 nebo změna pojemnování podle odpovídajícího "největšího" rozměru, př.: pic-00-800h pro obrázek NECOx800
    - `pic-%02d-1280w.jpg`, kde `%02d` znamená: dvourozměrné číslo, které nabývá hodnot od 00 do 99, př.: `pic-00-1280w.jpg`, `pic-01-1280w.jpg`, `pic-02-1024w.jpg`,`pic-05-1024h.jpg`

 - soubory typu .gif se berou jako by to byly obrázky a platí pro ně stejná pravidla, př.: `pic-00-800w.gif`

#### Velikosti pro SVG

Pro potřeby SVG je třeba myslet na velikost uživatelem vložených prvků, která se odvijí od poměru velikosti samotného obrázku, velikosti obrázku ve stránce a rozlišení uživatele. V případě nevhodně velikého obrázku jsou prvky buď příliš malé nebo velké. V tuto chvíli je jediné řešení následující:

- 1× `1280px` (na šířku), `1024px` (na výšku)
- 2× `1024px` nebo 1× SVG s doplňkovou galerií či jiným postraním elementem,
- 3× `810px`,
- 4× `640px`

#### Automatizace

Nejrychlejší a nejefektivnější možnost je využití knihovny [ImageMagick](https://imagemagick.org): `magick '*.jpg[1280x1024]' -quality 80 -density 72 pic-%02d-1280w.jpg` je poté nutné zkontrolovat všechny obrázky, a ty které jsou orientovány na výšku, přepsat do odpovídajícího formátu (pic-00-`1024h`.jpg)

### Audio & video

- normalizovat hlasitost nahrávek ve cvičení

#### Audio

- max samplerate `44100khz`, max bitrate `128kbps`
- Každý audio soubor je nejlepší ještě prohnat tímto: `ffmpeg -i source.mp3 -c:a copy -b:a 128k -sample_rate 44100 audio-00.mp3` . Zjistil jsem například různé interpretace časů atp. Tohle to znormalizuje.
- pojmenování `audio-%02d.mp3`, kde `%02d` znamená: dvourozměrné číslo, které nabývá hodnot od 00 do 99, př.: `audio-00.mp3`,`audio-01.mp3`,`audio-02.mp3`

#### Video

Ke konvertování videa lze využít [ffmpeg](http://ffmpeg.org/) s následujícími parametry:

- `ffmpeg -i source.mp4 -movflags faststart -vf scale="720:-1" -c:v libx264 -b:v 500k -maxrate 1M -bufsize 1M -r 25 -c:a copy -b:a 128k output.mp4`

Nebo [handbrake GUI](https://handbrake.fr/) s tímto profilem: [historylab profil pro handbrake](https://drive.google.com/open?id=1RXVOO3HnDgICJtxtVMOKzmAxTy30qO0i)

- pojmenování `video-%02d.mp4`, kde `%02d` znamená: dvourozměrné číslo, které nabývá hodnot od 00 do 99, př.: `video-00.mp4`,`video-01.mp4`,`video-02.mp4`

Titulky ve fomátu .vtt pojmenovat stejně jako video, např.: `video-00.vtt`

### Datový soubor

- Vyplnit všechna následující metadata:
  - `cviceni.autor`
  - `cviceni.klicovaSlova`
  - `cviceni.anotace.ucitel` a `cviceni.anotace.verejna`
  - `cviceni.trvani`
  - `cviceni.funkce` (!důležité, importují se díky tomu některé závislosti)

- Vyplnit a zkontrolovat unikátnost `id` nástrojů
    - nepoužívejte `slug` jako `id` pro jednotlivé nástroje
- Vyplnit nápovědy
- Nástroje
    - jednotlivé nástroje jsou popsány v souboru `cviceni-funkce.md`
- `slug` je univerzální pro každé cvičení a lze ho použít jen v těchto případech:
    - `cviceni.slug`
    - `cviceni.pdf.doporucenyPostup`
    - `cviceni.uvodniObrazek`
    - jako cesta k souborům např.: `co-tvrdil-atlas/pic-05-1280w.jpg`
    - nepojmenovávejte soubory (image, audio, video, text) pomocí slugu cvičení. Při přejmenování cvičení pomocí scriptu `change-name` by mohlo dojít k přepsání těchto hodnot a cvičení by nenahrávalo správná data.

### Texty

- pojmenování `text-%02d.html`, kde `%02d` znamená: dvourozměrné číslo, které nabývá hodnot od 00 do 99, př.: `text-00.html`,`text-01.html`,`text-02.html`

### Doporučený postup

- ukládat ve formátu: `slug-cviceni.pdf` do složky `pdf`, př.: `co-tvrdil-attlas.pdf`

## Vývoj

`npm start` nebo pro jedno konkrétní cvičení `npm install && gulp --cviceni nazev-cviceni`.

## Zveřejnění

Merge request přes Bitbucket do `dev`.

## Přejmenování cvičení

Pro jednoduší přejmenování cvičení lze použít tento příkaz, přejmenují se složky, soubory a i data v souborech (Pozn.: u souborů je jednoduché nahrazování stringů, tudíž když obrázek bude mít stejný název jako slug cvičení, přepíše se to v datech, ale obrázek v souboru se již nepřejmenuje. Je to problém jen u starých cvičení, u nových se používají obecné popisy obrázků.)
Jména cvičení musejí být přesná, ale zbylé formáty (slug a camelcase) si již generuji z názvů.
Není zde kontrola nového slugu, že je unikání.

`npm run change-name -- --oldname "Husitství a historici" --newname "Nové jméno pro cvičení"`

# Import dat z velké tabulky

Nyní lze spustit script, který vezme data z velké tabulky a dopíše je do JSONů cvičení. Script má volitelný argument `id`, který spustí import jen pro cvičení s daným `id`. Pokud se jméno lokálního souboru neshoduje s jménem ve velké tabulce, je uživatel upozorněn v příkazové řádce se od něj očekává, jestli přejmenování potvrdí (`yes`) nebo zamítne (`no`). Pokud zvolí kladnou odpověď, spustí se script pro přejmenování cvičení `change-name`.

`npm run import-table-data`

`npm run import-table-data -- --id 1`

Více o nastavení importu ve [/scripts/table-import/README.md](/scripts/table-import/README.md)
