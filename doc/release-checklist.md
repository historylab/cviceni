# Release checklist
1. Create merge request via Bitbucket UI
2. Check modified exercies and increase `cviceni.version.content` if needed
3. Check if all changes are documented in `CHANGELOG.md`
4. Run test with `npm run prerelease`
5. Checkout `master` locally
6. Merge `dev` or `hotfix` to `master` with `git merge --squash` with `git commit` locally
7. `npm version major` (or `minor` or `patch`)
8. `git push --follow-tags` (push commits & tags)
9. After [Pipilenes](https://bitbucket.org/historylab/cviceni/addon/pipelines/home) are finished update LMS with [cviceni-verze](https://bitbucket.org/historylab/cviceni-verze/)
10. If exercise content for LMS has been changed as well prepare JSONs

# Versioning
## Major version
Major versioning reflects differentiation of the application from user point of view. Change major version if any of the following occurs:

- Exercise: change of slug
- Exercise: fundamental change of layout or content. Better to bundle as much changes (exercises) together as possible
- Design: Radical change in design (UX, UI)
- Code: Change of structure in saved (user) data

## Minor
Updates which do not meet any requirements above (major) or below (patch).

## Patch
- hotfixes
- typos

Cherry-picking might be needed in current release process. Make a fix in `dev` branch and then cherry-pick it from `release` or `hotifx` branch.

# Servers & release branches
## TL (public)
- Branch: https://bitbucket.org/historylab/cviceni/branch/master
- Soubory cvičení: https://bitbucket.org/historylab/cviceni-verze-master/
- Katalog: https://bitbucket.org/historylab/seznam-cviceni-public/

## TL4 (test)
- Branch: https://bitbucket.org/historylab/cviceni/branch/release/test
- Soubory cvičení: https://bitbucket.org/historylab/cviceni-verze-test/
- Katalog: https://bitbucket.org/historylab/seznam-cviceni-test/

## Azure (staging)
- Branch: https://bitbucket.org/historylab/cviceni/branch/release/staging
- Soubory cvičení: https://bitbucket.org/historylab/cviceni-verze-staging/
- Katalog: https://bitbucket.org/historylab/seznam-cviceni-staging/

# Support branches
Older versions used to load old saved activities might need hotfixes sometimes. Use [this flow](https://stackoverflow.com/questions/16386323/following-git-flow-how-should-you-handle-a-hotfix-of-an-earlier-release):

```
git checkout 6.0
git checkout -b support/6.x
git checkout -b hotfix/6.0.1
```

... make your fix, then:

```
git checkout support/6.x
git merge hotfix/6.0.1
git branch -d hotfix/6.0.1
npm version patch
```
