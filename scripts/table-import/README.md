﻿# Google clouds
Abychom mohli přistupovat k datům z Google dokumentů bylo zapotřebí vytvořit projekt v platformě Google Cloud (Google Cloud Platform - https://console.cloud.google.com/). Do této platformy se musí přihlásit pomocí google účtu (libovolný google uživatel, pomocí emailu a hesla).

# Sekce API & Services
Do tohoto projektu následně bylo potřeba přidat "Google Sheets API" a "Google Drive API" v sekci API & Services.
Dále v podsekci "Credentials" bylo potřeba vytvořit nový "Service Account" a přidat mu alespoň roli "viewer". Po vytvoření se v seznamu "Service Accounts" objeví nový creditovaný uživatel. Následně na něj stačí kliknout (či kliknout na šipku pro úpravu).
Dole v seznamu je sekce "Keys". Zde pomocí "ADD KEY" vytvoříme nový klíč. Po potvrzení se stáhne JSON soubor s autentizačním klíčem, který následně použijeme v aplikaci.

# Autentizační soubor
Uložený autentizační soubor ve formátu JSON přejmenujeme na "AccountAuth.json" a vložíme jej do této složky: `cviceni\scripts\table-import`.

# Přidání emailu do Google sheets
Poté co jsme vytvořili nový účet, vytvoří se pro něj emailová adresa. Tato adresa je potřeba vložit do sdílení pro soubor tabulky (v našem případě Velká tabulka).

# Závěr a spouštění 
Následně script naimportje data z autentizačního souboru a načte data do scriptu.
Pro spuštění scriptu je potřeba napsat do konzole:
`npm run import-table-data` - přepsání dat všech cvičení, či
`npm run import-table-data -- --id 71` - spustí se script jen pro konkrétní cvičení a přepíšou se data pouze v něm.

## Výběr sheetu pro import
Byl přidán parametr, který spustí script pro specifický spredsheed pomocí čísla gid. V případě nezanesení je vybrán "hlavní katalog" alá "moderní dějiny", tj. gid: 1659665612. PS: gid je bráno z DevelopersListů.
Příklad spuštění:
`npm run import-table-data -- --gid 1982556687`

### Seznam sheetů (gid)
- `1659665612` moderní dějiny (default)
- `1982556687` starší dějiny
- `1631156714` anglická cvičení
- `1672286782` moduly
- `1940222835` polský katalog
- `295735063` slovenský katalog
