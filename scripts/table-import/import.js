/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable function-paren-newline */
/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable comma-dangle */
/* eslint-disable brace-style */

const fs = require('fs');
const fsp = require('fs').promises;
const { GoogleSpreadsheet } = require('google-spreadsheet');
const readlineSync = require('readline-sync');

const creds = require('./AccountAuth.json');

const { renameCviceni } = require('../rename-cviceni-function');

const idSheetMainCatalogue = '1659665612'; // id of main catalogue sheet (moderni dejiny)

const templateCviceni = require('../../src/scaffolding-templates/cviceni/data/cviceniNewData.json');

// Command line arguments
const indexIdOfCviceni = process.argv.indexOf('--id');

let inputIdOfCviceni = process.argv[indexIdOfCviceni + 1];
// Setup names
if (indexIdOfCviceni === -1) {
  inputIdOfCviceni = '';
}

// Setup sheet
const indexIdOfSheet = process.argv.indexOf('--gid');

let inputIdOfSheet = process.argv[indexIdOfSheet + 1];
if (indexIdOfSheet === -1) {
  inputIdOfSheet = idSheetMainCatalogue;
}

const getMobileData = (data) => {
  // 0 - not usable, 1 - with limitations, 2 - appropriate
  const mobileData = {
    mobileFriendly: 0,
    touch: 2,
    smallScreen: 2,
    tabletScreen: 2,
  };

  const updateMobilData = (touchValue, smallScreenValue, tabletScreenValue) => {
    // console.log("Data> ", touchValue, smallScreenValue, tabletScreenValue)
    if (mobileData.touch > touchValue) mobileData.touch = touchValue;
    if (mobileData.smallScreen > smallScreenValue) {
      mobileData.smallScreen = smallScreenValue;
    }
    if (mobileData.tabletScreen > tabletScreenValue) {
      mobileData.tabletScreen = tabletScreenValue;
    }
  };

  for (const index in data.slajd) {
    const slide = data.slajd[index];

    // ================== PRAMENY ==================
    if (slide.prameny) {
      if (slide.prameny.length === 3) updateMobilData(2, 1, 2);
      if (slide.prameny.length === 4) updateMobilData(2, 0, 1);
      if (slide.prameny.length > 4) updateMobilData(2, 0, 0);
    }
    // ================== SVG ==================
    if (slide.svg) {
      if (slide.svg.galerie) {
        if (slide.svg.soubory.length === 1) updateMobilData(2, 2, 2);
        if (slide.svg.soubory.length === 2) updateMobilData(2, 1, 1);
        if (slide.svg.soubory.length > 2) updateMobilData(2, 0, 1);
      }
      if (slide.svg.pretahovani) {
        updateMobilData(2, 2, 2); // ? nejsou plná data
      }
      if (slide.svg.soubory.length === 1) updateMobilData(2, 2, 2);
      if (slide.svg.soubory.length === 2) updateMobilData(2, 1, 2);
      if (slide.svg.soubory.length === 3) updateMobilData(2, 1, 1);
      if (slide.svg.soubory.length > 3) updateMobilData(2, 0, 1);
    }
    // ================== RAZENI ==================
    if (slide.razeni) {
      updateMobilData(0, 2, 2); // ? nejsou plná data
      let numOfAudio = 0;
      let numOfVideo = 0;
      for (const i in slide.razeni.objekty) {
        const object = slide.razeni.objekty[i];
        const mediumSoubor = object.medium;
        if (mediumSoubor === 'audio') numOfAudio++;
        if (mediumSoubor === 'video') numOfVideo++;
        if (mediumSoubor === 'svg' || mediumSoubor === 'obrazek') {
          numOfAudio++;
          numOfVideo++;
        }
      }
      if (numOfAudio === 3) updateMobilData(2, 1, 2);
      if (numOfAudio === 4) updateMobilData(2, 0, 1);
      if (numOfAudio > 4) updateMobilData(2, 0, 0);

      if (numOfVideo === 2) updateMobilData(2, 1, 2);
      if (numOfVideo === 3) updateMobilData(2, 0, 1);
      if (numOfVideo > 3) updateMobilData(2, 0, 0);
    }
    // ================== TEXTOVY-EDITOR ==================
    if (slide.textovyEditor) {
      if (slide.textovyEditor.galerie) {
        if (slide.textovyEditor.texty.length === 1) updateMobilData(1, 1, 2);
        if (slide.textovyEditor.texty.length > 1) updateMobilData(1, 0, 0);
      }
      if (slide.textovyEditor.texty.length === 1) updateMobilData(1, 2, 2);
      if (slide.textovyEditor.texty.length === 2) updateMobilData(1, 1, 2);
      if (slide.textovyEditor.texty.length > 2) updateMobilData(1, 0, 0);
    }
    // ================== UZIVATELSKY-TEXT ==================
    if (slide.uzivatelskyText) {
      if (slide.uzivatelskyText.novaTabulka) {
        if (slide.uzivatelskyText.novaTabulka.pretahovani) {
          updateMobilData(0, 2, 2);
        }
      }
    }

    // ================== NOVA-TABULKA ==================
    if (slide.novaTabulka) {
      if (slide.novaTabulka.pretahovani) updateMobilData(0, 2, 2);
    }
    // ================== KLICOVA-SLOVA ==================
    // Nothing change => if(slide.klicovaSlova){}
    // ================== VYBER ==================
    if (slide.vyber) {
      let onlyText = true;
      for (const object in slide.vyber.objekty) {
        const { medium } = slide.vyber.objekty[object];
        if (!medium || medium !== 'text') onlyText = false;
      }
      if (!onlyText) updateMobilData(2, 1, 2);
    }
    // ================== MEDIA ==================
    if (slide.media) {
      let numOfAudio = 0;
      let numOfVideo = 0;
      for (const i in slide.media.soubory) {
        const object = slide.media.soubory[i];
        const splitSoubor = object.soubor.split('.');
        if (splitSoubor[1] === 'mp3') numOfAudio++;
        if (splitSoubor[1] === 'mp4') numOfVideo++;
        if (splitSoubor[1] === 'jpg') {
          numOfAudio++;
          numOfVideo++;
        }
      }
      if (numOfAudio === 3) updateMobilData(2, 1, 2);
      if (numOfAudio === 4) updateMobilData(2, 0, 1);
      if (numOfAudio > 4) updateMobilData(2, 0, 0);

      if (numOfVideo === 2) updateMobilData(2, 1, 2);
      if (numOfVideo === 3) updateMobilData(2, 0, 1);
      if (numOfVideo > 3) updateMobilData(2, 0, 0);
    }
  }

  if (
    mobileData.touch + mobileData.smallScreen + mobileData.tabletScreen ===
    6
  ) {
    mobileData.mobileFriendly = 2;
  } else if (
    !mobileData.touch ||
    !mobileData.smallScreen ||
    !mobileData.tabletScreen
  ) {
    mobileData.mobileFriendly = 0;
  } else {
    mobileData.mobileFriendly = 1;
  }

  return mobileData;
};

/** Function to read all jsons in src\data folder
 * @param {Path} dirname path to src\data
 * @param {Function} onFileContent given function
 * @param {Function} onError given error function
 */
function readFiles(dirname, onFileContent, onError) {
  fs.readdir(dirname, (err, filenames) => {
    if (err) {
      onError(err);
      return;
    }
    let cviceniUpdated = false;
    // Foreach file read it and call given function onFileContent
    filenames.forEach((filename) => {
      const file = fs.readFileSync(dirname + filename);
      cviceniUpdated = cviceniUpdated || onFileContent(dirname, filename, file);
    });
    if (inputIdOfCviceni) {
      if (!cviceniUpdated) {
        console.log(
          `Activity with id:${inputIdOfCviceni} WAS NOT updated! Can't find it.`,
        );
      }
    }
  });
}

/**
 * Main method to rewrite files in cviceni
 * @param {JSON} exportVelkaTabulka table of imported data from Velka Tabulka
 */
function rewriteCviceniWithExportOfVelkaTabulka(exportVelkaTabulka) {
  /**
   *
   * @param {Path} dirname path to specific cviceni JSON
   * @param {String} filename name of the specific cviceni JSON file
   * @param {JSON} data specific data of the cviceni
   */
  async function manageData(dirname, filename, data) {
    // Must be init again becouse ESLint and renaming cviceni
    const cviceniData = JSON.parse(JSON.stringify(templateCviceni.cviceni));

    // Find cviceni in ImportedVelkaTabula JSON
    const foundData = await exportVelkaTabulka.find(
      (velkaTabulka) =>
        parseInt(velkaTabulka.id, 10) === parseInt(data.cviceni.id, 10),
    );

    // If local data was found in Velka Tabulka, start renaming
    if (foundData) {
      if (!foundData.zverejneni) {
        console.log(
          `Cviceni ${filename} has not been published (in test) yet!`,
        );
        return;
      }

      // ================Copy-version====================
      cviceniData.version = data.cviceni.version;

      // ================Copy-ID====================
      cviceniData.id = data.cviceni.id;

      // ===============Katalog============
      if (foundData.zverejneni) {
        const updatedSet = new Set(data.cviceni.katalog);

        if (foundData.zverejneni === 'test') {
          updatedSet.add('test');
        }
        if (foundData.zverejneni === 'public') {
          updatedSet.add('test');
          updatedSet.add('public');
        }

        cviceniData.katalog = Array.from(updatedSet);
      }

      // ===============Jazyk============
      cviceniData.language = data.cviceni.language;

      // ===============Slug============
      cviceniData.slug = data.cviceni.slug;

      // ===============Nazev============
      cviceniData.nazev = data.cviceni.nazev;

      // ===============Partners============
      cviceniData.partners = data.cviceni.partners;

      // ===============Catalog related data============
      cviceniData.isHidden = data.cviceni.isHidden;
      cviceniData.isEvaluating = data.cviceni.isEvaluating;

      // ====================Autor====================
      if (foundData.autor) {
        cviceniData.autor = foundData.autor.split(', ');
      }

      // ====================Klicova-slova====================
      // -- RVP --
      const rvp = [];
      if (foundData.KSrvp1) {
        const rvp1 = foundData.KSrvp1.trim();
        if (rvp1 !== '') {
          rvp.push(rvp1);
        }
      }
      if (foundData.KSrvp2) {
        const rvp2 = foundData.KSrvp2.trim();
        if (rvp2 !== '') {
          rvp.push(rvp2);
        }
      }
      cviceniData.klicovaSlova.rvp = rvp;
      // -- Konceptove --
      const konceptove = [];
      if (foundData.KSkonceptove1) {
        const konceptove1 = foundData.KSkonceptove1.trim();
        if (konceptove1 !== '') {
          konceptove.push(konceptove1);
        }
      }
      if (foundData.KSkonceptove2) {
        const konceptove2 = foundData.KSkonceptove2.trim();
        if (konceptove2 !== '') {
          konceptove.push(konceptove2);
        }
      }
      cviceniData.klicovaSlova.koncept = konceptove;
      // -- B4 --
      if (foundData.KSB4) {
        const B4 = foundData.KSB4.trim();
        if (B4 !== '') {
          cviceniData.klicovaSlova.b4 = [B4];
        }
      }
      // -- Historylab
      if (foundData.KShistorylab) {
        const historylabove = foundData.KShistorylab.trim();
        if (historylabove !== '') {
          cviceniData.klicovaSlova.historylab = [historylabove];
        }
      }

      // -- Obdobi --
      const obdobi = [];
      if (foundData.KSobdobi1) {
        const obdobi1 = foundData.KSobdobi1.trim();
        if (obdobi1 !== '') {
          obdobi.push(obdobi1);
        }
      }
      if (foundData.KSobdobi2) {
        const obdobi2 = foundData.KSobdobi2.trim();
        if (obdobi2 !== '') {
          obdobi.push(obdobi2);
        }
      }
      if (obdobi.length > 0) {
        cviceniData.klicovaSlova.obdobi = obdobi;
      }

      // ====================Anotace====================
      if (foundData.anotaceProUcitele) {
        cviceniData.anotace.ucitel = foundData.anotaceProUcitele.trim();
      }
      if (foundData.anotaceProZaky) {
        cviceniData.anotace.verejna = foundData.anotaceProZaky.trim();
      }

      // ====================Doba-trvani====================
      if (foundData.dobaTrvani) {
        const trvaniInt = foundData.dobaTrvani.split(' ');
        // eslint-disable-next-line prefer-destructuring
        try {
          cviceniData.trvani = parseInt(trvaniInt[0], 10);
        } catch (e) {
          cviceniData.trvani = 0;
        }
      }

      // ====================Funkce====================
      cviceniData.funkce = data.cviceni.funkce;

      // ====================PDF====================
      cviceniData.pdf = data.cviceni.pdf;

      // ====================UvodniObrazek====================
      cviceniData.uvodniObrazek = data.cviceni.uvodniObrazek;

      // ====================Obtiznost====================
      if (foundData.obtiznost) {
        cviceniData.obtiznost = foundData.obtiznost.trim();
      }

      // ====================CASOVA OSA====================
      // -----------------Show------------------
      // 1659665612 = moderni dejiny
      cviceniData.casovaOsa.show = inputIdOfSheet === idSheetMainCatalogue;

      // ------------------Cas------------------

      if (foundData.roky) {
        cviceniData.casovaOsa.roky = foundData.roky
          .toString()
          .trim()
          .split(', ')
          .map((element) => parseInt(element, 10));
      }

      // ------------------Epochy------------------

      // rozdělení pro více epoch
      if (foundData.epochaCeska) {
        cviceniData.casovaOsa.epochy[0].obdobi = foundData.epochaCeska
          .trim()
          .split(', ');
      }

      if (foundData.epochaSvetova) {
        cviceniData.casovaOsa.epochy[1].obdobi = foundData.epochaSvetova
          .trim()
          .split(', ');
      }

      // ====================ADD-color===========================
      const getImageColorProperties =
        await require('../visual-cloud/get-image-color-properties');
      cviceniData.color = await getImageColorProperties(
        `src/img/${data.cviceni.uvodniObrazek}`,
      );

      // ==================ADD-mobile-function=======================

      cviceniData.mobileData = getMobileData(data);

      // ========================= Don't-save-if-it's-same ================================
      if (JSON.stringify(data.cviceni) === JSON.stringify(cviceniData)) {
        return;
      }

      // =====================ADD-template-to-cviceni====================
      data.cviceni = cviceniData;

      // =====================Rewrite-the-file=====================
      await fsp.writeFile(
        dirname + filename,
        JSON.stringify(data, null, 2),
        (err) => {
          if (err) return console.error(err);
          return true;
        },
      );

      // =====================Rename-cviceni=====================
      if (
        foundData.nazev.trim() &&
        foundData.nazev.trim() !== data.cviceni.nazev
      ) {
        // Wait for user's response.
        const confirm = readlineSync.question(
          `Do you want to rename cviceni ${
            data.cviceni.nazev
          } to ${foundData.nazev.trim()}? [yes]/no:`,
        );
        if (confirm !== 'no') {
          await renameCviceni(data.cviceni.nazev, foundData.nazev.trim())
            .then((result) => {
              if (result) {
                console.log(
                  `====CVICENI-WAS-SUCCESSFULLY-RENAMED from: "${
                    data.cviceni.nazev
                  }" to:"${foundData.nazev.trim()}"`,
                );
              } else {
                console.log(
                  `====CVICENI-WAS-NOT-RENAMED: "${data.cviceni.nazev}"`,
                );
              }
            })
            .catch();
        } else {
          console.log(
            `Cviceni ${data.cviceni.nazev} (${data.cviceni.id}) has different name use script change-name or re-run this script and confirm dialog for renaming cviceni`,
          );
        }
      }
    }
  }

  readFiles(
    'src/data//',
    (dirname, filename, content) => {
      // Check if the given data can be parsed into JSON
      try {
        const type = filename.split('.');
        if (type[1] !== 'json') {
          console.log(`File: ${filename} is not a json -> SKIPPING`);
          return false;
        }
        const contentParsed = JSON.parse(content);

        if (inputIdOfCviceni) {
          if (contentParsed.cviceni.id !== parseInt(inputIdOfCviceni, 10)) {
            return false;
          }
          manageData(dirname, filename, contentParsed).then(() => {
            console.log(`Cviceni ${filename} was updated`);
          });
          return true;
        }
        manageData(dirname, filename, contentParsed).then(() => {
          console.log(`Cviceni ${filename} was updated`);
        });

        // manageData(dirname, filename, contentParsed);
      } catch (e) {
        console.log(`Error pri ukladani v cviceni: ${filename}\n${e}\n`);
        return false;
      }
    },
    (err) => {
      throw err;
    },
  );
}

/**
 * Setup google Auth
 * spreadsheet key is the long id in the sheets URL
 */
async function accessSpreadsheet() {
  console.log(
    '=======================STARTING-CONFIGURATION=================================',
  );
  const velkaTabulkaExportJSON = [];
  const doc = new GoogleSpreadsheet(
    '1z6VrEK8lB-BBei93XRV4boO-2vxfRJlCiylAIwh64h4',
  );

  await doc.useServiceAccountAuth({
    client_email: creds.client_email,
    private_key: creds.private_key,
  });

  // Must call loadInfo() -> works like lazy load
  await doc.loadInfo();
  // Access spredsheet DevelopersList (which has number 1659665612 - viz url)
  const sheet = doc.sheetsById[inputIdOfSheet];

  console.log(
    '=======================LOADING-DATA-FROM-VELKA-TABULKA=================================',
  );
  // Lazy load for load rows
  const rows = await sheet.getRows();
  // Get header values
  const headerOfTable = sheet.headerValues;

  // Prepare given data from Velka Tabulka into JSON format
  rows.forEach((row) => {
    const cviceni = {};
    headerOfTable.forEach((header, index) => {
      cviceni[header] = row._rawData[index];
    });
    velkaTabulkaExportJSON.push(cviceni);
  });

  console.log(
    '=======================REWRITING-CVICENI=================================',
  );
  // Call main function with exported data in JSON format
  rewriteCviceniWithExportOfVelkaTabulka(velkaTabulkaExportJSON);
}

// Method to start auth, import and remake files from Velka Tabulka
accessSpreadsheet().catch((err) =>
  console.error(`Something went wrong with import. ${err}`),
);
