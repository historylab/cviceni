/**
 * Start by npm run json-maker in root directory.
 * 
 * Parameters: --gid {number} <- takes data from specific sheet
 * Example: npm run json-maker    ||   npm run json-maker -- --gid 1672286782
 * 
 *  */

const fs = require('fs');
const { GoogleSpreadsheet } = require('google-spreadsheet');

const creds = require('./AccountAuth.json');
const slugify = require('slugify');
const path = require('path')
const templateJsonMaker = require('./template/json-maker-template.json');

//Setup sheet
const indexIdOfSheet = process.argv.indexOf('--gid');

let inputIdOfSheet = process.argv[indexIdOfSheet + 1];
if (indexIdOfSheet === -1) {
    inputIdOfSheet = 1672286782;
}


/**
 * Setup google Auth
 * spreadsheet key is the long id in the sheets URL
 */
async function accessSpreadsheet() {
    console.log(
        '=======================JSON-MAKER-IS-STARTING================================='
    );
    const velkaTabulkaExportJSON = [];
    const doc = new GoogleSpreadsheet(
        '1z6VrEK8lB-BBei93XRV4boO-2vxfRJlCiylAIwh64h4'
    );

    await doc.useServiceAccountAuth({
        client_email: creds.client_email,
        private_key: creds.private_key,
    });

    // Must call loadInfo() -> works like lazy load
    await doc.loadInfo();
    // Access spredsheet DevelopersList (which has number 1659665612 - viz url)
    const sheet = doc.sheetsById[inputIdOfSheet];

    console.log(
        '=======================LOADING-DATA-FROM-VELKA-TABULKA================================='
    );
    // Lazy load for load rows
    const rows = await sheet.getRows();
    // Get header values
    const headerOfTable = sheet.headerValues;

    // Prepare given data from Velka Tabulka into JSON format
    rows.forEach((row) => {
        const cviceni = {};
        headerOfTable.forEach((header, index) => {
            cviceni[header] = row._rawData[index];
        });
        velkaTabulkaExportJSON.push(cviceni);
    });

    console.log(
        '=======================MAKING-JSON================================='
    );

    const nameOfFile = sheet.title;

    // Call main function with exported data in JSON format
    jsonMakerFunction(velkaTabulkaExportJSON, nameOfFile);
}

// Method to start auth, import and remake files from Velka Tabulka
accessSpreadsheet()
    .catch((err) =>
        console.error(`Something went wrong with import. ${err}`)
    );

/**
 * 
 * @param {String} str 
 * @returns all characters have first char upperCase
 */
function upperCaseFirst(str) {
    return str.charAt(0).toUpperCase() + str.substring(1);
}
/**
 * 
 * @param {String} str 
 * @returns string converted to camelCase
 */
function changeNameToCamelCase(str) {
    const stringArray = str.split('-');

    stringArray.forEach((part, index) => {
        stringArray[index] = upperCaseFirst(part);
    });

    return `${stringArray.join('')}`;
}

/**
 * Function which try to find id of given name of exercise.
 * 
 * @param {String} nameOfCviceni the name of the exercise we are looking for
 * @returns id of exercise OR null
 */
function finIdOfCviceni(nameOfCviceni) {
    let idOfCviceni = null
    const jsonsInDir = fs.readdirSync('./src/data').filter(file => path.extname(file) === '.json');
    jsonsInDir.forEach(file => {
        const fileData = fs.readFileSync(path.join('./src/data', file));
        const json = JSON.parse(fileData.toString());
        if (json.cviceni.nazev === nameOfCviceni) {
            idOfCviceni = json.cviceni.id
            return
        }
    });
    return idOfCviceni;
}

/**
 * Function which take loaded data from velka tabulka and make json (cviceni.json template). Then it make file named as sheet name
 * and saved it to scripts/table-import/{nameOfFile}.json
 * 
 * @param {Object} exportVelkaTabulka - data from velka tabulka from specific gid
 * @param {String} nameOfFile - name of the sheet (used for file name)
 */
function jsonMakerFunction(exportVelkaTabulka, nameOfFile) {
    let content = {};

    exportVelkaTabulka.forEach(exercise => {
        let exerciseObj =JSON.parse(JSON.stringify(templateJsonMaker))
        let camelCaseOfExercise = "";
        Object.keys(exercise).forEach(dataIndex => {
            switch (dataIndex) {
                case "nazev":
                    exerciseObj.nazev = exercise[dataIndex];
                    exerciseObj.slug = slugify(exercise[dataIndex], {remove: /[!?#=*+~.()'":@]/g,}).toLowerCase()
                    camelCaseOfExercise = "modul" + changeNameToCamelCase(exerciseObj.slug);
                    break;
                case "anotaceProUcitele":
                    exerciseObj.anotace.ucitel = exercise[dataIndex];
                    break;
                case "anotaceProZaky":
                    exerciseObj.anotace.verejna = exercise[dataIndex];
                    break;
                case "HLCviceni1":
                    const idCviceni1 = finIdOfCviceni(exercise[dataIndex])
                    if (idCviceni1 !== null) exerciseObj.cviceni.push(idCviceni1)
                    break;
                case "HLCviceni2":
                    const idCviceni2 = finIdOfCviceni(exercise[dataIndex])
                    if (idCviceni2 !== null) exerciseObj.cviceni.push(idCviceni2)
                    break;
                case "KShistorylab":
                    //KW
                    exerciseObj.klicovaSlova.historylab.push(exercise[dataIndex]);
                    //Id cviceni
                    const idCviceni3 = finIdOfCviceni(exercise[dataIndex])
                    if (idCviceni3 !== null) exerciseObj.cviceni.push(idCviceni3)
                    break;
                case "KSB4":
                    //KW
                    exerciseObj.klicovaSlova.b4.push(exercise[dataIndex]);
                    //Id cviceni
                    const idCviceni4 = finIdOfCviceni(exercise[dataIndex])
                    if (idCviceni4 !== null) exerciseObj.cviceni.push(idCviceni4)
                    break;
                case "epochaCeska":
                    exerciseObj.casovaOsa.epochy.forEach(epocha =>{
                        if(epocha.refId === "czech"){
                            epocha.obdobi.push(exercise[dataIndex])
                        }
                    })
                    break;
                case "obtiznost":
                    exerciseObj.obtiznost = slugify(exercise[dataIndex], {remove: /[!?#=*+~.()'":@]/g,}).toLowerCase();
                    break;
                case "doporucenyPostup":
                    exerciseObj.doporucenyPostup = exercise[dataIndex];
                    break;
                default:
                    break;
            }
        });
        content[camelCaseOfExercise] = exerciseObj
    })

    fs.writeFile(`./scripts/table-import/${nameOfFile}.json`, JSON.stringify(content, null, 2), function (err) {
        if (err) throw err;
        console.log(`JSON MAKER MADE FILE! "${nameOfFile}"`);
    });

}