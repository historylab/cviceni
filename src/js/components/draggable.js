import Component from './component';
import {
  __dispatchEvent,
  __localPosition,
  __offSetForTouchEvent,
} from '../lib/utils';

export default class Draggable extends Component {
  constructor($element) {
    super($element);

    this.idPrefix = 'drag-item';
    // duplicate source of different editor elements
    this.dataFrom = this.target.getAttribute('data-from');
    this.dataSources = this.dataFrom ? this.dataFrom.split(' ') : [];

    this.position = {
      x: 0,
      y: 0,
      dx: 0,
      dy: 0,
    };
    this.cache = {
      // to know if draggable item was dropped or not and how many times
      items: {},
    };

    this.$targetPrevious = null;
    this.$targetCurrent = null;
    this.$draggedItem = null;

    this.objWidth = null;
    this.objHeight = null;

    this.lastEvent = null;

    this.init();
  }

  init() {
    const $dragItems = this.target.querySelectorAll('.draggable__item');
    $dragItems.forEach(($dragItem) => {
      // get id
      const id = $dragItem.getAttribute('data-id');
      // inicial drop numbers
      this.cache.items[id] = 0;
      // use d3 container for $dragItem
      this.dragAndDropEvents(d3.select($dragItem), id);
    });

    // initial handler for dropped item to svg
    this.handleDeleteItemFromSvgDrop();

    // initial handler for adding tags from text
    this.handleAddTagsFromText();
  }

  // dropToken is used for handle usecase when user drag from drop-place to drop-place
  dragAndDropEvents($dragItem, itemId, dropToken = true) {
    // drag & drop
    $dragItem.call(
      d3
        .drag()
        .container(this.$slide)
        .on('start', (event) => {
          this.onStart(event, $dragItem);
        })
        .on('drag', (event) => {
          this.onDrag(event, $dragItem);
        })
        .on('end', (event) => {
          this.onDrop(event, $dragItem, itemId, dropToken);
        }),
    );
  }

  onStart(event, $dragItem) {
    this.$slide.classList.add('is-dragging');

    this.$targetPrevious = event.sourceEvent.target;

    // in this method is better to have classic html node
    const $dragItemHTML = $dragItem.node();

    // initial position
    this.objWidth = $dragItemHTML.offsetWidth;
    this.objHeight = $dragItemHTML.offsetHeight;
    this.position.dx = 0;
    this.position.dy = 0;

    // touch event dont have clientX directly in event but in array of touches
    if (event.sourceEvent.clientX) {
      this.position.x = event.sourceEvent.clientX - event.sourceEvent.offsetX;
      this.position.y = event.sourceEvent.clientY - event.sourceEvent.offsetY;
    } // for touches optimize
    else {
      const position = __localPosition(event, $dragItem);
      const offset = __offSetForTouchEvent(event.sourceEvent);
      this.position.x = position.x - offset.x;
      this.position.y = position.y - offset.y;
    }

    // get dragged node
    // clone element if from DRAG place
    if ($dragItemHTML.parentNode.hasAttribute('data-drag-place')) {
      this.$draggedItem = $dragItemHTML.cloneNode(true);
    }
    // do not clone if from DROP place
    else {
      this.$draggedItem = $dragItemHTML;

      // get table data
      const $table = $dragItemHTML.closest('.table-new__body');
      const $cell = $dragItemHTML.closest('.table-new__cell');
      const columnId = $cell.getAttribute('data-column-id');

      // dispatch event that item has been removed from drop place
      __dispatchEvent(
        document,
        'draggable.drop',
        {},
        {
          id: $table.id,
          task: 'remove',
          columnId,
          // type: itemType,
          $node: $dragItemHTML,
        },
      );
    }

    // add to body
    document.body.appendChild(this.$draggedItem);
    // convert back to d3 container
    this.$draggedItem = d3.select(this.$draggedItem);

    this.$draggedItem
      .classed('is-dragged', true)
      .classed('was-dropped', false)
      .attr('style', `top:${this.position.y}px; left:${this.position.x}px;`);

    // touchend doesnt have coordinates
    this.lastEvent = event.sourceEvent;
  }

  onDrag(event, $dragItem) {
    // touchend doesnt have coordinates
    this.lastEvent = event.sourceEvent;
    // console.log($dragItem.node());
    // update position
    this.position.dx += event.dx;
    this.position.dy += event.dy;

    // move with dragging element
    this.$draggedItem.style(
      'transform',
      `translate3d(${this.position.dx}px, ${this.position.dy}px, 0) rotate(5deg)`,
    );

    this.$targetCurrent = event.sourceEvent.target;
    // check for make placeholder in drop place
    if (this.$targetPrevious !== this.$targetCurrent) {
      // remove placeholder in old target
      const $placeholderOld = this.$targetPrevious.querySelector(
        '.draggable__item--placeholder',
      );
      if ($placeholderOld) $placeholderOld.remove();

      // create placeholder in new target
      if (this.$targetCurrent.hasAttribute('data-drop-place')) {
        const numberOfPossibleDrops =
          this.$targetCurrent.getAttribute('data-drop-place');
        const numberOfDropsDone = this.$targetCurrent.childElementCount;

        // console.log('ondrag', numberOfDropsDone);

        if (
          numberOfPossibleDrops === '-1' ||
          +numberOfPossibleDrops > numberOfDropsDone
        ) {
          // remove this class is important for creating placeholder from dragging item from drop place
          const $placeholderNew = $dragItem
            .clone(true)
            .classed('is-dragged', false)
            .classed('draggable__item--placeholder', true)
            .attr('style', 'outline: 0px;')
            .node();

          this.$targetCurrent.appendChild($placeholderNew);
        }
      }

      // update target
      this.$targetPrevious = this.$targetCurrent;
    }
  }

  onDrop(event, $dragItem, itemId, dropToken) {
    if (this.lastEvent.touches) {
      const coords = this.lastEvent.touches[0];
      this.$targetCurrent = document.elementFromPoint(
        coords.clientX,
        coords.clientY,
      );
    } else {
      this.$targetCurrent = event.sourceEvent.target;
    }

    // when dropping within droppable normal DOM node
    if (this.$targetCurrent.hasAttribute('data-drop-place')) {
      // get placeholder
      let $dropItem = this.$targetCurrent.querySelector(
        '.draggable__item--placeholder',
      );

      if ($dropItem) {
        const itemType = $dropItem.getAttribute('data-draggable-item');

        // remove unneeded classes for dropItem
        $dropItem.classList.remove(
          'draggable__item--placeholder',
          'was-dropped',
        );
        $dropItem.classList.add('dropped__item');

        // update drops num
        if (dropToken) {
          $dragItem.classed('was-dropped', true);
          this.cache.items[itemId] += 1;
        }

        if (itemType === 'svg') {
          const $fullScreenIcon = $dropItem.querySelector(
            '[data-fullscreen="toggle"]',
          );
          // replaced svg don't have full screen icon
          if ($fullScreenIcon) {
            $fullScreenIcon.remove();
          }
        } else {
          // add cross for remove item
          const $rightElmChild = $dropItem.firstElementChild;
          const $cross = document.createElement('span');
          $cross.classList.add('node-close');
          $cross.innerText = '×';

          // remove svg from frop item
          $rightElmChild.removeChild($rightElmChild.lastElementChild);
          $rightElmChild.appendChild($cross);

          // remove item after click on cross
          $cross.addEventListener('click', () => {
            this.removeDraggableItem($dropItem, itemId, dropToken);
          });
        }

        // add events to clone for posible dragging back
        // dropItem has to by in d3 container
        this.dragAndDropEvents(d3.select($dropItem), itemId, false);
      } // only click on drop item
      else {
        $dropItem = $dragItem
          .clone(true)
          .classed('is-dragged', false)
          .attr('style', 'outline: 0px;')
          .node();

        this.$targetCurrent.appendChild($dropItem);

        // add events to clone for posible dragging back
        // dropItem has to by in d3 container
        this.dragAndDropEvents(d3.select($dropItem), itemId, false);
      }

      // get table data
      const $table = this.$targetCurrent.closest('.table-new__body');
      const $cell = this.$targetCurrent.closest('.table-new__cell');
      const columnId = $cell.getAttribute('data-column-id');

      // dispatch event that item has been dropped
      __dispatchEvent(
        document,
        'draggable.drop',
        {},
        {
          id: $table.id,
          task: 'add',
          columnId,
          // type: itemType,
          $node: $dropItem,
        },
      );

      // remove draggedItem from DOM
      this.$draggedItem.remove();
    }
    // when dropping within SVG
    else {
      // get svg
      const $svg = this.$targetCurrent.closest('svg');
      // check drop attribute
      const svgCanDrop = $svg ? $svg.hasAttribute('data-svg-drop') : false;

      // when dropping within droppable svg node
      if (svgCanDrop) {
        const position = __localPosition(this.lastEvent, $svg);
        const $node = this.$draggedItem
          .classed('is-dragged', false)
          .attr('style', 'outline: 0px;')
          .node();

        // update drops num
        $dragItem.classed('was-dropped', true);
        this.cache.items[itemId] += 1;

        // add to svg
        // dispatch event that svg has changed
        __dispatchEvent(
          document,
          'svg.drop',
          {},
          {
            svg: {
              id: $svg.id,
              $node,
              type: $node.getAttribute('data-draggable-item'),
              position,
              size: {
                width: this.objWidth,
                height: this.objHeight,
              },
            },
          },
        );

        this.$draggedItem.remove();
      }
      // when dropping outside of droppable area
      else {
        // if it's last drop then remove drop class
        this.removeDraggableItem(this.$draggedItem, itemId, dropToken);
      }
    }

    this.$slide.classList.remove('is-dragging');
  }

  handleDeleteItemFromSvgDrop() {
    document.addEventListener('svg.drop.delete', (event) => {
      const { slideId, id, $object } = event.detail.item;

      // console.log('smazano', id, $object);
      const $sameItemFromDragPlace = this.target.querySelector(
        `div[data-id=${id}]`,
      );

      if ($sameItemFromDragPlace && this.$slide.id === slideId) {
        // if it's last drop then remove drop class
        if (this.cache.items[id] === 1) {
          $sameItemFromDragPlace.classList.remove('was-dropped');
        }

        // update drop number of concrete item
        this.cache.items[id] -= 1;
      }
    });
  }

  handleAddTagsFromText() {
    // need if we drop more than one same item
    let idSalt = 1;

    document.addEventListener('editor.change', (e) => {
      const data = e.detail.text;
      const concreteTagName = this.target.getAttribute('data-tagName');

      // if concreteTagName
      if (
        this.dataSources.includes(data.id) &&
        (!concreteTagName || concreteTagName === data.tagData.tagName)
      ) {
        let $newDaggabletItem;
        let $tagTextSpan;
        let textToAdd;
        let newId;
        let idParts;

        switch (data.task) {
          case 'add':
            // clone hidden tag
            $newDaggabletItem = this.target.firstElementChild.cloneNode(true);
            $newDaggabletItem.classList.remove('hidden');

            // find place where to add tags text
            $tagTextSpan = $newDaggabletItem.querySelector('.keywords-item');

            // get right text to add
            textToAdd = data.value.textContent;
            if (textToAdd[textToAdd.length - 1] === '×') {
              textToAdd = textToAdd.slice(0, -1);
            }

            // add text
            $tagTextSpan.firstElementChild.innerHTML = textToAdd;

            // add color to draggable item
            $newDaggabletItem.firstElementChild.classList.add(
              `node-text-${data.tagColor}`,
            );
            $tagTextSpan.classList.add(`node-text-${data.tagColor}`);

            // create new id
            newId = $newDaggabletItem.getAttribute('data-id');
            idParts = newId.split('-');
            idParts.splice(-1, 1, `dup-${idSalt}`);
            newId = idParts.join('-');

            // set new id
            $newDaggabletItem.setAttribute('data-id', newId);
            // add marker id, if the tag is from text editor
            if (data.value.getAttribute('data-tag-id')) {
              $newDaggabletItem.setAttribute(
                'data-marker-id',
                data.value.getAttribute('data-tag-id'),
              );
            }
            // add to cache
            this.cache.items[newId] = 0;

            idSalt += 1;
            this.target.appendChild($newDaggabletItem);

            // add listeners for drag'n'drop
            this.dragAndDropEvents(d3.select($newDaggabletItem), newId);
            break;
          case 'remove':
            this.target.childNodes.forEach(($cell) => {
              if (
                Draggable.isRightToDeleteHelper(
                  $cell,
                  data.value,
                  $cell.getAttribute('data-draggable-item'),
                )
              ) {
                $cell.remove();
              }
            });
            break;
          default:
            console.log(
              'NewTableERROR: Undefinated task for new table! (cellManager)',
            );
        }
      }
    });
  }

  removeDraggableItem($draggedItem, itemId, dropToken) {
    // if it's last drop then remove drop class
    if (this.cache.items[itemId] === 1) {
      const $sameItemFromDragPlace = this.target.querySelector(
        `div[data-id=${itemId}]`,
      );
      $sameItemFromDragPlace.classList.remove('was-dropped');
    }

    // update drop number of concrete item
    if (!dropToken) {
      this.cache.items[itemId] -= 1;
    }

    // remove draggedItem from DOM
    $draggedItem.remove();
  }

  // prepare for more types of duplicate types
  static isRightToDeleteHelper(elm, deliveredValue, itemType) {
    // fix for tags
    // table div textContent include '×' as last char
    // because it is in self span
    let elemTextContent;
    if (
      itemType === 'tag' &&
      elm.textContent[elm.textContent.length - 1] === '×'
    ) {
      elemTextContent = elm.textContent.slice(0, -1);
    } else {
      elemTextContent = elm.textContent;
    }
    return elemTextContent === deliveredValue.textContent;
  }
}
