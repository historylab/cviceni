import { Decoration, DecorationSet } from 'prosemirror-view';
import Component from '../component';
import SelectionMenu from './selectionMenu';
import WrittingMenu from './writtingMenu';
import schemaConfig from './schema';
import { __dispatchEvent } from '../../lib/utils';

export default class Editor extends Component {
  constructor($element) {
    super($element);
    this.editor = undefined;
    this.id = this.target.id;
    this.editorType = this.target.getAttribute('data-editor');
    this.wholeWords = !!this.target.getAttribute('data-editor-cela-slova');

    // find editor place in cviceni
    this.$editor = this.target.querySelector(`#prosemirror-editor-${this.id}`);
    // find editor content
    this.$content = this.target.querySelector(
      `#prosemirror-content-${this.id}`,
    );
    this.schema = new pm.Schema(schemaConfig);

    // only if cviceni isn't done load html
    if (!HISTORYLAB.import.done) {
      this.initHTML();
    } else {
      this.initJSON();
    }
  }

  // calling from save-data.js
  getEditorJSON() {
    if (this.editor) {
      // get JSON format of last editor state
      return this.editor.state.toJSON();
    }
    return {};
  }

  // type of editor is set here
  initHTML() {
    // Helper funkce pro nalezení rozsahů markerů
    function getMarkerRanges(doc, markerMark) {
      const markersMap = new Map();

      doc.descendants((node, pos) => {
        node.marks.forEach((mark) => {
          if (mark.type === markerMark) {
            const id = mark.attrs.dataTagId;
            const { dataCommandName } = mark.attrs;
            if (!markersMap.has(id)) {
              // Initialize with the current position
              markersMap.set(id, {
                id,
                from: pos,
                to: pos + node.nodeSize,
                dataCommandName,
              });
            } else {
              // Update the 'to' position to extend the range
              const marker = markersMap.get(id);
              marker.to = Math.max(marker.to, pos + node.nodeSize);
              markersMap.set(id, marker);
            }
          }
        });
      });

      // Convert the map values to an array of marker ranges
      return Array.from(markersMap.values());
    }

    // Globální proměnná pro uložení reference na view
    let currentView = null;
    // Plugin pro přidání tlačítka zavření u markerů
    function createMarkerPlugin(editorId) {
      return new pm.Plugin({
        view(view) {
          // Uložení reference na view při inicializaci pluginu
          currentView = view;
          return {
            destroy() {
              // Vyčištění reference na view při zničení pluginu
              currentView = null;
            },
          };
        },
        props: {
          decorations(state) {
            const markers = getMarkerRanges(
              state.doc,
              state.schema.marks.marker,
            );
            const decorationsArray = [];

            markers.forEach(({ id, to, dataCommandName }) => {
              const widget = document.createElement('button');
              widget.className = 'remove-marker-button';
              widget.dataset.markerId = id;
              widget.setAttribute('aria-label', 'Remove selection');
              widget.setAttribute('title', 'Remove selection');

              widget.addEventListener('click', (e) => {
                e.preventDefault();
                e.stopPropagation();
                if (currentView) {
                  const { dispatch } = currentView;
                  const markerMark = state.schema.marks.marker;
                  const $dispatchNode = SelectionMenu.connectAllSelectedParts(
                    currentView,
                    id,
                  );

                  // Ensure markerMark is correctly referenced
                  if (!markerMark) {
                    console.error('Marker mark not found in schema.');
                    return;
                  }

                  // Find all ranges with the same dataTagId
                  const allMarkers = getMarkerRanges(
                    state.doc,
                    markerMark,
                  ).filter((marker) => marker.id === id);

                  // Create a transaction to remove all matching marks
                  let { tr } = state;
                  allMarkers.forEach((markerRange) => {
                    tr = tr.removeMark(
                      markerRange.from,
                      markerRange.to,
                      markerMark,
                    );
                  });

                  // Dispatch the transaction
                  dispatch(tr);

                  // dispatch event that editor has changed
                  __dispatchEvent(
                    document,
                    'editor.change',
                    {},
                    {
                      text: {
                        id: editorId,
                        task: 'remove',
                        tagData: { tagName: dataCommandName },
                        value: $dispatchNode,
                      },
                    },
                  );
                }
              });

              // Position the widget at the end of the marker
              decorationsArray.push(
                Decoration.widget(to, widget, { side: -1, block: false }),
              );
            });

            return DecorationSet.create(state.doc, decorationsArray);
          },
        },
        // plugin key has to be unique in editor
        key: new pm.PluginKey('removeSelection'),
      });
    }

    let plugins;
    // add plugin by editor type
    if (this.editorType === 'zvyraznovani') {
      const bubbleMenu = this.selectionMenu(this.id);
      const markerPlugin = createMarkerPlugin(this.id);
      plugins = [bubbleMenu, markerPlugin];
    }

    if (this.editorType === 'predznaceny') {
      const premarkedParts = Editor.premarkedParts(this.id, this.schema);
      const markerPlugin = createMarkerPlugin(this.id);
      plugins = [premarkedParts, markerPlugin];
    }

    if (this.editorType === 'psani') {
      const writting = this.writting(pm.keydownHandler);
      const histKeymap = pm.keymap({ 'Mod-z': pm.undo, 'Mod-y': pm.redo });
      plugins = [pm.history(), histKeymap, writting];
    }

    // create editor instance by defining editor view
    this.editor = new pm.EditorView(this.$editor, {
      // state is created from html structure parsed from our text and array of plugins
      state: pm.EditorState.create({
        doc: pm.DOMParser.fromSchema(this.schema).parse(this.$content),
        plugins,
      }),
    });
  }

  // only visualization of saved data
  // load saved data from JSON format
  initJSON() {
    const data = JSON.parse(this.$content.textContent);
    const readOnly = Editor.readOnlyMod();
    const { schema } = this;

    // create editor instance by defining editor view
    this.editor = new pm.EditorView(this.$editor, {
      // editor state is created from JSON object and schema
      state: pm.EditorState.fromJSON({ schema, plugins: [readOnly] }, data),
    });
  }

  // only diable editing editor
  // https://prosemirror.net/docs/ref/#view.EditorProps.editable
  static readOnlyMod() {
    return new pm.Plugin({
      props: {
        editable() {
          return false;
        },
      },
      // plugin key has to be unique in editor
      key: new pm.PluginKey('ReadOnlyMod'),
    });
  }

  // manager of marker
  selectionMenu(editorId) {
    const { schema } = this;
    // get menu from gulp
    const $menuTemplate = this.target.querySelector(
      `#marker-menu-${this.target.id}`,
    );

    // Capture the current Editor instance
    const editorInstance = this;

    return new pm.Plugin({
      // set plugin visualization means set definated marker menu as part of plugin
      view(editorView) {
        const selectionMenu = new SelectionMenu(
          editorId,
          schema,
          editorView,
          $menuTemplate,
          editorInstance.wholeWords
        );
        return selectionMenu;
      },
      // when editable is false history doesn't work
      props: {
        editable() {
          return false;
        },
      },
      // plugin key has to be unique in editor
      key: new pm.PluginKey('SelectionMenu'),
    });
  }

  // manager of premarker
  static premarkedParts(editorId, schema) {
    return new pm.Plugin({
      props: {
        // when editable is false history doesn't work
        editable() {
          return false;
        },
        // https://prosemirror.net/docs/ref/#view.EditorProps.handleClickOn
        handleClickOn(view, pos, node, nodePos) {
          if (schema.nodes.premarked.name === node.type.name) {
            // definition of class adding to selected node
            const classSelectDefinition = 'selected';
            const { state } = view;
            const { tr } = state;

            let newClassName;
            let task;
            // variable for extra data from cviceni JSON for dispatch event distribution
            const tagData = JSON.parse(node.attrs.textData);
            tagData.tagName = 'vyznacena-pasaz';

            if (node.attrs.className.includes(classSelectDefinition)) {
              newClassName = node.attrs.className.replace(
                ` ${classSelectDefinition}`,
                '',
              );
              task = 'remove';
            } else {
              newClassName = `${node.attrs.className} ${classSelectDefinition}`;
              task = 'add';
            }

            // for create new state it's important to not change html node directly
            // it's important to use prosemirror functions
            const nodeAttrs = {
              id: node.attrs.id,
              className: newClassName,
              textData: node.attrs.textData,
            };

            // replace old node on set position by new node with new attributes
            tr.setNodeMarkup(nodePos, null, nodeAttrs);

            // changing editor's state
            const newState = state.apply(tr);
            view.updateState(newState);

            // dispatch event that editor has changed
            const $node = view.dom.querySelector(`#${node.attrs.id}`);
            __dispatchEvent(
              document,
              'editor.change',
              {},
              {
                text: {
                  id: editorId,
                  task,
                  tagData,
                  value: $node.cloneNode(true),
                },
              },
            );
          }
          return false;
        },
      },
      // plugin key has to be unique in editor
      key: new pm.PluginKey('PremarkedParts'),
    });
  }

  // manager of writting
  writting(keydownHandler) {
    // get menu items from gulp
    const $toolbarTemplate = this.target.querySelector(
      `#toolbar-${this.target.id}`,
    );
    // console.log($toolbarTemplate);
    let sugar = 0;

    // definition of deleted parts logic
    const deletedParts = (state, dispatch) => {
      // create unique id
      const id = `remove-part-${sugar}`;
      // define command
      const command = pm.toggleMark(this.schema.marks.writtingRemovePart, {
        id,
      });
      // marked selection part
      command(state, dispatch);
      // increase sugar
      sugar += 1;
      return true;
    };

    return new pm.Plugin({
      // set plugin visualization means set definated writting menu as part of plugin
      view(editorView) {
        return new WrittingMenu(editorView, $toolbarTemplate);
      },
      props: {
        handleKeyDown: keydownHandler({
          Delete: deletedParts,
          Backspace: deletedParts,
        }),
      },
      // plugin key has to be unique in editor
      key: new pm.PluginKey('Writting'),
    });
  }
}
