import { __dispatchEvent } from '../../lib/utils';

// plugin specification: https://prosemirror.net/docs/ref/#state.Plugin_System
export default class SelectionMenu {
  constructor(editorId, schema, editorView, $menuTemplate, wholeWords = true) {
    this.$menuBox = $menuTemplate;

    this.init(editorId, schema, editorView);
    this.update(editorView, null);
    this.wholeWords = wholeWords;
  }

  init(editorId, schema, editorView) {
    const { nodes, marks } = schema;

    // menu close buttom logic
    // In Firefox still is selected text
    const $closeButton = this.$menuBox.querySelector('a');
    $closeButton.addEventListener('click', () => {
      this.update(editorView);
    });

    // hide menu after unfocus editor
    editorView.dom.parentNode.addEventListener('blur', (event) => {
      if (
        event.relatedTarget &&
        !event.relatedTarget.classList.contains('menu-item') && // no menu button
        !event.relatedTarget.classList.contains('contextual-menu-component')
      ) {
        // no menu close button
        this.update(editorView);
      }
    });

    // menu items logic
    const $menuItems = this.$menuBox.querySelectorAll('.menu-item');
    // sugar for creating different data-tag-id
    let sugar = 0;

    if ($menuItems) {
      $menuItems.forEach(($menuItem) => {
        // get attributes for marked parts
        const color = $menuItem.getAttribute('data-color');
        const dataCommandName = $menuItem.getAttribute('data-command-name');

        $menuItem.addEventListener('click', (event) => {
          event.preventDefault();

          if (!SelectionMenu.isAdded(marks.marker, editorView.state)) {
            if (this.wholeWords) {
              // Retrieve the current selection positions
              const { from, to } = editorView.state.selection;

              // Expand the selection to word boundaries
              const { newFrom, newTo } =
                SelectionMenu.expandSelectionToWordBoundaries(
                  editorView.state,
                  from,
                  to,
                  [' ', '-', ',', '.', ';', ':', '!', '?'], // Add any other delimiters as needed
                );

              // Create a transaction to set the new expanded selection
              const trM = editorView.state.tr.setSelection(
                pm.TextSelection.create(editorView.state.doc, newFrom, newTo),
              );

              // Dispatch the transaction to update the selection
              editorView.dispatch(trM);
            }
            // create unique dataTagId
            const dataTagId = `${this.$menuBox.id}-${sugar}`;
            // add remove button from schema
            // SelectionMenu.addRemoveButton(editorView, nodes.removeButton);
            // define command
            // https://github.com/ProseMirror/prosemirror-commands/blob/91f86b300ba850d496cc6e142c0aed9ae81893f1/src/commands.js#L488
            // https://github.com/ProseMirror/prosemirror-transform/blob/03a011ae1e098b958f9479c1cee1e997cef2ba4d/src/mark.js#L9
            // https://github.com/ProseMirror/prosemirror-transform/blob/03a011ae1e098b958f9479c1cee1e997cef2ba4d/src/mark_step.js#L16
            // https://github.com/ProseMirror/prosemirror-transform/blob/master/CHANGELOG.md#129-2021-01-19
            const command = pm.toggleMark(marks.marker, {
              color,
              dataCommandName,
              dataTagId,
            });
            // marked selection part
            command(editorView.state, editorView.dispatch, editorView);
            // remember selection
            // const markedSelection = editorView.state.selection;
            // console.log('=>(selectionMenu.js:61) markedSelection', markedSelection);
            // set selection empty
            SelectionMenu.closeSelection(editorView);
            // increase sugar
            sugar += 1;
            // create one dispatchable dom element to dispatch from this tool
            const $dispatchNode = SelectionMenu.connectAllSelectedParts(
              editorView,
              dataTagId,
            );
            // add removing marker logic
            // TODO: if whant to remove after click on the selection
            // SelectionMenu.removingMarkerHandler({
            //   editorId,
            //   editorView,
            //   markedSelection,
            //   $dispatchNode,
            //   dataTagId,
            //   dataCommandName,
            //   marker: marks.marker,
            // });
            // dispatch event that editor has changed
            __dispatchEvent(
              document,
              'editor.change',
              {},
              {
                text: {
                  id: editorId,
                  task: 'add',
                  tagData: { tagName: dataCommandName },
                  value: $dispatchNode,
                  tagColor: color,
                },
              },
            );
          } else {
            this.update(editorView, null);
          }
        });
      });
    }

    // add to editor
    editorView.dom.parentNode.appendChild(this.$menuBox);
  }

  // calling from editor every time when editor is updated
  // manage showing and hiding menu and menu position
  update(view, lastState) {
    const { state, readOnly } = view;
    const activeElementClasses = document.activeElement.classList;

    // have to be canvas dom node
    if (
      !state ||
      readOnly ||
      state.selection.empty ||
      !activeElementClasses.contains('canvas')
    ) {
      if (!this.$menuBox.classList.contains('hidden')) {
        this.$menuBox.classList.add('hidden');
      }
      return;
    }

    this.$menuBox.classList.remove('hidden');

    if (!this.$menuBox.offsetParent) {
      if (!this.$menuBox.classList.contains('hidden')) {
        this.$menuBox.classList.add('hidden');
      }
      return;
    }
    // Update the Content state before calculating the position
    // this.contentUpdate(editorView.state);

    const { from, to } = state.selection;

    try {
      const start = view.coordsAtPos(from);
      const end = view.coordsAtPos(to);

      const box = this.$menuBox.getBoundingClientRect();

      const offsetParentBox =
        this.$menuBox.offsetParent.getBoundingClientRect();
      let left =
        (start.left + end.left) / 2 - box.width / 2 - offsetParentBox.left;

      if (left < 5) {
        left = 5;
      }

      this.$menuBox.style.left = `${left}px`;
      this.$menuBox.style.top = `${
        start.top - offsetParentBox.top - box.height
      }px`;
    } catch (err) {
      console.error('Menu selection error', err);
    }
  }

  destroy() {
    if (this.$menuBox) {
      this.$menuBox.remove();
    }
  }

  // check if part of selection was marked
  static isAdded(marker, state) {
    const { ranges } = state.selection;
    let has = false;

    for (let i = 0; !has && i < ranges.length; i += 1) {
      const { $from, $to } = ranges[i];
      // https://prosemirror.net/docs/ref/#model.Node.rangeHasMark
      has = state.doc.rangeHasMark($from.pos, $to.pos, marker);
    }

    return has;
  }

  // TODO: do in same transaction with toggleMark
  // static addRemoveButton(editorView, removeButton) {
  //   const { state } = editorView;
  //   const { ranges } = state.selection;

  //   const lastPosition = ranges[ranges.length - 1].$to.pos;
  //   // https://prosemirror.net/docs/ref/#state.EditorState.apply
  //   // https://prosemirror.net/docs/ref/#transform.Transform.insert
  //   // https://prosemirror.net/docs/ref/#model.NodeType.create
  //   const newState = state.apply(state.tr.insert(lastPosition, removeButton.create()));
  //   // update editor state
  //   editorView.updateState(newState);
  // }

  static closeSelection(editorView) {
    const { state } = editorView;

    // https://prosemirror.net/docs/ref/#state.EditorState.apply
    // https://prosemirror.net/docs/ref/#state.Transaction.setSelection
    // https://prosemirror.net/docs/ref/#state.TextSelection^create
    const newState = state.apply(
      state.tr.setSelection(pm.TextSelection.create(state.doc, 0, 0)),
    );
    // update editor state
    editorView.updateState(newState);
  }

  static removingMarkerHandler(inputs) {
    const {
      editorId,
      editorView,
      markedSelection,
      $dispatchNode,
      dataTagId,
      dataCommandName,
      marker,
    } = inputs;
    // find remove button at the end of marked part
    const selection = editorView.dom.querySelector(
      `[data-tag-id=${dataTagId}]`,
    );
    selection.addEventListener('click', (event) => {
      const { state, dispatch } = editorView;
      const { tr } = state;

      // Calculate positions
      const start = markedSelection.from;
      const end = markedSelection.to;

      // Remove the mark from the text within the document
      tr.removeMark(start, end, marker);

      // Apply the transaction to update the editor state
      dispatch(tr);

      // dispatch event that editor has changed
      __dispatchEvent(
        document,
        'editor.change',
        {},
        {
          text: {
            id: editorId,
            task: 'remove',
            tagData: { tagName: dataCommandName },
            value: $dispatchNode,
          },
        },
      );
    });
  }

  // helper function for dispatch event
  // connect all marked parts
  // clone first part and add here text content of other parts
  static connectAllSelectedParts(view, dataTagId) {
    const $nodes = view.dom.querySelectorAll(
      `span[data-tag-id="${dataTagId}"]`,
    );

    if ($nodes.length === 0) {
      console.error("Error: Didn't find parts of marked text.");
      return null;
    }

    // Clone the first node to preserve its attributes and classes
    const $finalNode = $nodes[0].cloneNode(false); // Shallow clone (no children)

    // Iterate through each node to clone and clean content
    $nodes.forEach(($node) => {
      // Deep clone the node to include all child elements
      const $clone = $node.cloneNode(true);

      // Remove all remove-marker-button elements within the clone
      const buttons = $clone.querySelectorAll('button.remove-marker-button');
      buttons.forEach((button) => button.remove());

      // Append the cleaned HTML content to the final node
      $finalNode.innerHTML += $clone.innerHTML;
    });

    return $finalNode;
  }

  // Add this function within the SelectionMenu class or as a separate utility
  static expandSelectionToWordBoundaries(
    state,
    from,
    to,
    delimiters = [' ', '-'],
  ) {
    let newFrom = from;
    let newTo = to;
    const { doc } = state;

    // Check if the selection starts with a delimiter
    const charAtStart = doc.textBetween(newFrom, newFrom + 1, null, '\0');
    const shouldExpandLeft = !delimiters.includes(charAtStart);

    //  Check if the selection ends with a delimiter
    const charAtEnd = doc.textBetween(newTo - 1, newTo, null, '\0');
    const shouldExpandRight = !delimiters.includes(charAtEnd);

    // Expand to the left until a delimiter is found
    if (shouldExpandLeft) {
      while (newFrom > 0) {
        const charBefore = doc.textBetween(newFrom - 1, newFrom, null, '\0');
        if (delimiters.includes(charBefore)) {
          break;
        }
        newFrom -= 1;
      }
    }

    if (shouldExpandRight) {
      // Expand to the right until a delimiter is found
      while (newTo < doc.content.size) {
        const charAfter = doc.textBetween(newTo, newTo + 1, null, '\0');
        if (delimiters.includes(charAfter)) {
          break;
        }
        newTo += 1;
      }
    }

    return { newFrom, newTo };
  }
}
