import Component from './component';

export default class FakeMap extends Component {
  constructor($element) {
    super($element);

    this.$buttons = this.target.querySelectorAll('[data-fake-map="toggle"]');
    this.$maps = this.target.querySelectorAll('[data-fake-map="map"]');
    this.lastActiveIndex = 0;

    this.addEventListenerToButtons();
  }

  switchMap($button, index) {
    this.$maps[this.lastActiveIndex].classList.remove('active');
    this.$buttons[this.lastActiveIndex].classList.remove('active');

    this.$maps[index].classList.add('active');
    $button.classList.add('active');

    this.lastActiveIndex = index;
  }

  addEventListenerToButtons() {
    this.$buttons.forEach(($button, index) => {

      $button.addEventListener('click', (event) => {
        this.switchMap($button, index);
        this.showFeedback();

        event.preventDefault();
      });

    });
  }
}
