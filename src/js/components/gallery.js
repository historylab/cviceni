import Component from './component';
import AudioPlayer from './media/audio-player';

export default class Gallery extends Component {
  constructor($element) {
    super($element);

    // select all right gallery items
    const $selectableItemss = this.target.querySelectorAll('.selected-item');
    const $dupItems = this.target.querySelectorAll('[data-duplicate]');

    // fix for selectables - data load
    if (!HISTORYLAB.import.done) {
      // set events for all found items
      $selectableItemss.forEach(($item) => Gallery.setSelectEvents($item));
      $dupItems.forEach(($item) => Gallery.setTextEvents($item));
    } else {
      $selectableItemss.forEach(($item) => {
        // hide placeholder
        const $placeholder = $item.querySelector('.selected-item__placeholder');
        $placeholder.classList.add('is-hidden');
        // right item
        const $destination = document.querySelector(`#${$item.getAttribute('data-selectedId')} .is-checked`);
        if ($destination) Gallery.addItemFromSelect($item, '-loaded', {$node: $destination})
      });
    }
  }

  static setSelectEvents($item) {
    const selectedId = $item.getAttribute('data-selectedId');
    const $placeholder = $item.querySelector('.selected-item__placeholder');

    document.addEventListener('selection.change', (e) => {
      if (selectedId === e.detail.id) {
        const data = e.detail;
        const idTail = '-duplicated';

        switch (data.task) {
          // singlechoice
          case 'change':
            Gallery.removeItemFromSelect($item);
            Gallery.addItemFromSelect($item, idTail, data);
            break;

          // multichoice
          case 'select':
            // hide placeholder
            $placeholder.classList.add('is-hidden');

            Gallery.addItemFromSelect($item, idTail, data);
            break;

          case 'unselect':
            Gallery.removeItemFromSelect($item, data.itemId + idTail);

            // show placeholder if its have to
            if ($item.childElementCount === 1) {
              $placeholder.classList.remove('is-hidden');
            }
            break;

          default:
            console.error('ERROR: Undefinated task type.');
        }
      }
    });
  }

  static setTextEvents($item) {
    const acceptedId = $item.getAttribute('data-duplicate');
    const textDuplicateSources = acceptedId ? acceptedId.split(' ') : [];

    document.addEventListener('text.change', (e) => {
      const data = e.detail.text;

      if (textDuplicateSources.includes(data.id)) {
        switch (data.task) {
          case 'create':
          case 'change':
            // TODO: More inputs to one output
            $item.innerHTML = data.text;
            break;
          default:
            console.log('Nondefinated text events task. :(');
        }
      }
    });
  }

  static addItemFromSelect($item, idTail, data) {
    const $dupNode = data.$node.cloneNode(true);
    $dupNode.setAttribute('data-id', data.itemId + idTail);

    // add listeners to audio
    if ($dupNode.hasAttribute('data-player-audio')) {
      AudioPlayer.addEventListeners($dupNode, () => {});
    }

    $item.appendChild($dupNode);
  }

  static removeItemFromSelect($item, dataId = undefined) {
    if (dataId) {
      const $dupNode = $item.querySelector(`[data-id=${dataId}]`);
      $dupNode.remove();
    }
    else {
      $item.firstChild.remove();
    }
  }
}
