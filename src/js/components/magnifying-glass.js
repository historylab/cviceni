import Component from './component';

export default class MagnifyingGlass extends Component {
  // https://www.youtube.com/watch?v=e8IwM3iqnL0
  constructor($element) {
    super($element);
    // SVG lupa a ještě dynamicky měnícího se SVG,
    // který není na počátku známé?
    // if (el.nodeName === 'svg') {
    //   console.log('SVG');
    //   el.children[0].getAttribute('xlink:href')
    // }
    this.$element = document.querySelector('[data-lupa]');
    this.$canvas = document.querySelector('[data-lupa-canvas]');
    this.ctx = this.$canvas.getContext('2d');

    this._onStart = this._onStart.bind(this);
    this._onMove = this._onMove.bind(this);
    this._onEnd = this._onEnd.bind(this);
    this._update = this._update.bind(this);
    this._onResize = this._onResize.bind(this);

    this.zoomGlassSize = 230;
    this.targetBCR = null;
    this.zoomed = 0;
    this.targetZoomed = 0;

    this.x = 0;
    this.y = 0;
    this.trackingTouch = false;
    this.displayStyle = '';

    this._addEventListeners();
    this._initCanvas();
    this._onResize();

    window.requestAnimationFrame(this._update);
  }

  _initCanvas() {
    const width = this.zoomGlassSize;
    const height = this.zoomGlassSize;
    // if retina makes canvas more dense
    const dPR = window.devicePixelRation || 1;
    this.$canvas.width = width * dPR;
    this.$canvas.height = height * dPR;

    this.$canvas.style.width = `${width}px`;
    this.$canvas.style.height = `${height}px`;

    this.ctx.scale(dPR, dPR);
  }

  _onResize() {
    this.targetBCR = this.target.getBoundingClientRect();
  }

  _onStart(event) {
    this.displayStyle = getComputedStyle(this.$element).display;
    if (event.target !== this.target || this.displayStyle === 'none') {
      return;
    }

    event.preventDefault();
    this.x = event.pageX || event.touches[0].pageX;
    this.y = event.pageY || event.touches[0].pageY;

    this.trackingTouch = true;
    this.targetZoomed = 1;
    this.zoomed = 0;

    window.requestAnimationFrame(this._update);
  }

  _onMove(event) {
    if (!this.trackingTouch || this.displayStyle === 'none') {
      return;
    }

    this.x = event.pageX || event.touches[0].pageX;
    this.y = event.pageY || event.touches[0].pageY;
  }

  _onEnd() {
    if (this.targetZoomed) {
      // show OK button or feedback
      this.showFeedback();
    }

    this.targetZoomed = 0;
    this.trackingTouch = false;
  }

  _update() {
    const TAU = Math.PI * 2;
    const MAX_RADIUS = 100;
    const radius = this.zoomed * MAX_RADIUS;

    this.targetBCR = this.target.getBoundingClientRect();

    // local coordinates (ratio <0,1>)
    const ratioX =
      // min. 0
      Math.max(0,
        // max. 1
        Math.min(1, ((this.x - this.targetBCR.left) / this.targetBCR.width))
      );
    // console.log(ratioX);
    const ratioY =
      // min. 0
      Math.max(0,
        // max. 1
        Math.min(1, ((this.y - this.targetBCR.top) / this.targetBCR.height))
      );
    // console.log(ratioY);

    // actual zooming (scaling)
    const imageScale = 2.5;
    const scaledTargetWidth = this.targetBCR.width * imageScale;
    const scaledTargetHeight = this.targetBCR.height * imageScale;

    // Firefox on Windows has a bug because of the shadow
    if (!(platform.os.family.indexOf('Windows') > -1 && platform.name.indexOf('Firefox') > -1)) {
      // Shadow.
      this.ctx.shadowColor = 'rgba(0,0,0,0.4)';
      this.ctx.shadowBlur = 12;
      this.ctx.shadowOffsetY = 8;
    }

    // Background.
    this.ctx.clearRect(0, 0, this.zoomGlassSize, this.zoomGlassSize);
    this.ctx.fillStyle = '#fff';
    this.ctx.beginPath();
    this.ctx.arc(
      // x
      this.zoomGlassSize / 2,
      // y
      (this.zoomGlassSize - 10) - radius,
      // radius, startAngle, endAngle
      radius, 0, TAU);
    this.ctx.closePath();
    this.ctx.fill();

    // save the default state (without image)
    this.ctx.save();

    // Zoomed image.
    this.ctx.beginPath();
    this.ctx.arc(
      // x
      this.zoomGlassSize / 2,
      // y
      (this.zoomGlassSize - 10) - (radius + 1),
      // radius, startAngle, endAngle
      radius * 1.03, 0, TAU);
    this.ctx.clip();
    this.ctx.closePath();
    this.ctx.drawImage(this.target,
      // dx and dy (left and top)
      (-ratioX * scaledTargetWidth) + (this.zoomGlassSize / 2),
      (-ratioY * scaledTargetHeight) + (this.zoomGlassSize / 2),
      // scaled width and height
      scaledTargetWidth, scaledTargetHeight);

    // go back to default state (without image)
    // so next update starts fresh
    this.ctx.restore();

    // Position the parent $element.
    this._setTransformStyle(this.x, this.y);

    // Update the zoom value.
    this.zoomed += (this.targetZoomed - this.zoomed) / 4;

    // Schedule another update if the zoom is fairly non-zero.
    if (this.zoomed > 0.001) {
      // if (this.zoomed > 0.001 && this.zoomed < 0.998) {
      window.requestAnimationFrame(this._update);
      // } else if (this.zoomed >= 0.998) {
      //   this.zoomed = 1;
    } else {
      this.zoomed = 0;
    }
  }

  _addEventListeners() {
    // touch events
    document.addEventListener('touchstart', this._onStart);
    document.addEventListener('touchmove', this._onMove);
    document.addEventListener('touchend', this._onEnd);
    // mouse events
    document.addEventListener('mousedown', this._onStart);
    document.addEventListener('mousemove', this._onMove);
    document.addEventListener('mouseup', this._onEnd);
    // window events
    window.addEventListener('resize', this._onResize);
  }

  _setTransformStyle(x, y) {
    const halfYoomGlassSize = this.zoomGlassSize / 2;
    let betterX;
    let betterY;

    if (x - halfYoomGlassSize < 0) {
      betterX = Math.abs(x) + halfYoomGlassSize + 18;
    }
    else if (x + halfYoomGlassSize > window.innerWidth) {
      betterX = window.innerWidth - (x - window.innerWidth) - this.zoomGlassSize;
    }
    else {
      betterX = x;
    }

    if (y - this.zoomGlassSize < 0) {
      betterY = Math.abs(y) + this.zoomGlassSize + 18;
    }
    else {
      betterY = y;
    }

    this.$element.style.transform = `translate(${betterX}px, ${betterY}px)`;
  }
}
