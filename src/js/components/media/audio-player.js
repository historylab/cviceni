import Component from '../component';
import { getSecondsMinutesFormat, stopPlayingVideoPlayers } from './media';

export default class AudioPlayer extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor($element) {
    super($element);
  }

  // TODO: move to audio-simple.js
  static addEventListeners($item, showFeedback) {
    const $players = document.querySelectorAll('[data-player-audio]');
    const $audio = $item.querySelector('audio');
    const $playButton = $item.querySelector('[data-player-control]');
    const $timeLeft = $item.querySelector('[data-audio-time-left]');

    $playButton.addEventListener('click', (event) => {
      if ($playButton.getAttribute('data-player-control') === 'paused') {
        // TODO: abstract
        stopPlayingVideoPlayers($players);
        $audio.play();
        $playButton.setAttribute('data-player-control', 'playing');
      } else {
        // TODO: abstract
        $audio.pause();
        $playButton.setAttribute('data-player-control', 'paused');
      }

      showFeedback();

      event.stopPropagation();
    }, false);

    // do not propagate audio control events higher into the dom
    $playButton.addEventListener('mousedown', (event) => {
      event.stopPropagation();
    }, false);

    $audio.addEventListener('timeupdate', () => {
      $timeLeft.innerText = getSecondsMinutesFormat($audio.currentTime);
    });

    $audio.addEventListener('ended', () => {
      $playButton.setAttribute('data-player-control', 'paused');
    });
  }
}
