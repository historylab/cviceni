import Component from './component';
import { __dispatchEvent } from '../lib/utils';

export default class UserText extends Component {
  constructor($element) {
    super($element);

    this.$item = this.target.closest('.user-text');
    this.$counter = this.$item.querySelector('[data-usertext-counter]');
    this.$counterWrapper = this.$item.querySelector('.user-text__counter');
    this.$instruction = this.$item.querySelector('.user-text__question');

    this.$userTexts = this.$slide.querySelectorAll('[data-textarea-target]');

    this.feedback = JSON.parse(this.target.getAttribute('data-feedback'));

    this.lengthExpected = {
      min: parseInt(this.target.getAttribute('minlength') === '0' ? 1 : this.target.getAttribute('minlength'), 10),
      avg: null,
      max: parseInt(this.target.getAttribute('maxlength') / 2, 10),
    };
    this.lengthExpected.avg = Math.floor((this.lengthExpected.max - this.lengthExpected.min) / 4 + this.lengthExpected.min);

    this.cache = {
      answersCorrect: 0,
      colors: [
        'color-red',
        'color-orange',
        'color-green',
      ],
      questions: [],
    };

    this.lastValue = '';
    this.hadData = this.target.value && this.target.value.length > 0;
    this.wasUsed = false;

    if (this.hadData) {
      this.updateCharacterCounter();
    }

    if (!HISTORYLAB.import.done) {
      // duplicate source of different text element
      this.toDuplicate = this.target.getAttribute('data-duplicate-text');
      this.textDuplicateSources = this.toDuplicate ? this.toDuplicate.split(' ') : [];

      // listen events
      this.getEvents();
      this.addEventListeners();
    } // when cviceni is done it needs to reload questions - fix for selectable
    else {
      if (this.$instruction && this.$instruction.hasAttribute('data-from')) {
        const keyAttribute = 'data-selectable-data';
        const fromId = this.$instruction.getAttribute('data-from');
        const $destination = document.querySelector(`#${fromId} .is-checked [${keyAttribute}]`);

        // set question
        if ($destination && $destination.getAttribute(keyAttribute)) {
          this.$instruction.innerText = $destination.getAttribute(keyAttribute);
        }
      }
    }
  }

  updateCharacterCounter(length = this.target.value.length) {
    this.$counterWrapper.classList.remove(...this.cache.colors);
    if (length < this.lengthExpected.min) this.$counterWrapper.classList.add(this.cache.colors[0]);
    else if (length <= this.lengthExpected.avg) this.$counterWrapper.classList.add(this.cache.colors[1]);
    else if (length > this.lengthExpected.avg) this.$counterWrapper.classList.add(this.cache.colors[2]);

    this.$counter.innerText = length;
    this.target.classList.add('is-used', !this.wasUsed);
    this.wasUsed = true;
  }

  updateFeedback(length = this.target.value.length) {
    if (!this.$buttonFeedback) return;

    this.$buttonFeedback.classList.remove(...this.cache.colors);
    if (length < this.lengthExpected.min) {
      this.$buttonFeedback.classList.add(this.cache.colors[0]);
      this.$buttonFeedbackText.innerText = this.feedback.none;
    } else {

      if (length <= this.lengthExpected.avg) {
        this.$buttonFeedback.classList.add(this.cache.colors[1]);
        this.$buttonFeedbackText.innerText = this.feedback.short;
      }
      else if (length > this.lengthExpected.avg) {
        this.$buttonFeedback.classList.add(this.cache.colors[2]);
        this.$buttonFeedbackText.innerText = this.feedback.ok;
      }

      this.target.setAttribute('data-textarea-target', 'success');
      // check if all conditions of the same component on the same slide are met
      const conditionAll = Array.from(this.$userTexts).find(($userText) => ($userText.getAttribute('data-textarea-target') !== 'success'));

      if (conditionAll === undefined) {
        // show OK button or feedback
        this.showFeedback();
      }
    }
  }

  addEventListeners() {
    // BLUR
    this.target.addEventListener('blur', (event) => {
      this.updateCharacterCounter();

      if (event.target.value !== this.lastValue) {
        const taskName = (this.lastValue === '') ? 'create' : 'change';
        this.lastValue = event.target.value;

        // dispatch event that text has changed
        __dispatchEvent(document, 'text.change', {}, {
          text: {
            id: event.target.id,
            task: taskName,
            text: event.target.value,
          },
        });

        // remove connection of any textfields
        this.textDuplicateSources = [];
      }
    });

    // KEYUP
    this.target.addEventListener('keyup', (event) => {
      event.stopPropagation();

      const valueLength = this.target.value.length;
      this.updateCharacterCounter(valueLength);
      this.updateFeedback(valueLength);
    });
  }

  getEvents() {
    // when Selectable module modify question in user text
    if (this.$instruction && this.$instruction.hasAttribute('data-from')) {
      const fromId = this.$instruction.getAttribute('data-from');

      document.addEventListener('selection.change', (e) => {
        if (fromId === e.detail.id && e.detail.itemData && typeof e.detail.itemData === 'string') {
          this.$instruction.innerText = e.detail.itemData;
        }
      });
    }

    // when textarea value is taken from other textarea
    if (this.textDuplicateSources[0]) {

      document.addEventListener('text.change', (e) => {
        if (this.textDuplicateSources.includes(e.detail.text.id)) {

          const taskName = (this.target.value === '') ? 'create' : 'change';
          switch (e.detail.text.task) {
            case 'create':
            case 'change':
              // if is something already there
              if (this.target.value !== '') this.target.value += '\n';

              // if array length is one it means that can by connection line
              if (this.textDuplicateSources.length === 1) {
                this.target.value = e.detail.text.text;
              } else {
                this.target.value += `${e.detail.text.text}\n`;
              }

              // dispatch event that text has changed
              __dispatchEvent(document, 'text.change', {}, {
                text: {
                  id: this.target.id,
                  task: taskName,
                  text: this.target.value,
                },
              });

              // make different first value if is need
              this.lastValue = this.target.value;

              this.updateCharacterCounter();
              break;
            default:
              console.log('Nondefinated text events task. :(');
          }
        }
      });
    }
  }
}
