/* eslint-disable linebreak-style */
import Component from './component';
import { __dispatchEvent } from '../lib/utils';

export default class VideoStamp extends Component {

  constructor($element) {
    super($element);

    this.$video = this.target.querySelector('video');
    this.$stampItems = this.target.querySelectorAll(`[data-stamp]`);

    this.stampsWrapper = VideoStamp.createStampsWrapper(this.target);

    this.$stampItems.forEach(($stamp) => {
      this._addEventListeners($stamp);
    });

    // get colors for all feedback variants
    if (this.feedback) {
      this.cache.zpetnaVazba = true;
      this.cache.colors = this.feedback.map((feedback) => feedback.barva);
    }

  }

  static createStampsWrapper = (target) => {
    const videoControls = target.querySelector('[data-video-time-progress]');
    const stampsWrapper = document.createElement('div');
    stampsWrapper.classList.add('stamp-wrapper');
    stampsWrapper.setAttribute('data-stamp-wrapper', '');
    videoControls.appendChild(stampsWrapper);
    return stampsWrapper;
  };

  static addStamp = (stampMetadata) => {
    //Mark
    const newMark = document.createElement('div');
    newMark.className = 'mark';
    newMark.setAttribute('data-stamp-time', stampMetadata.currentTime);
    newMark.setAttribute('data-stamp-mark', '');
    const currentTimePercentage = stampMetadata.currentTime / stampMetadata.videoDuration;
    newMark.style.left = `${currentTimePercentage * 100}%`;
    //Stamp
    const stamp = document.createElement('div');
    stamp.className = `stamp ${stampMetadata.removable && 'removable-stamp'}`;
    stamp.innerHTML = stampMetadata.stamp;
    newMark.appendChild(stamp);

    if (stampMetadata.removable) {
      stamp.addEventListener('click', (e) => {
        let mark = e.target.closest('div[data-stamp-mark]');
        mark.remove();
      });
    }
    stampMetadata.stampsWrapper.appendChild(newMark);
  };

  _addEventListeners($stamp) {
    $stamp.addEventListener('click', () => {
      if (this.$video.currentTime === 0) return;

      const stampMetadata = {
        stamp: $stamp.innerHTML,
        currentTime: this.$video.currentTime,
        videoDuration: this.$video.duration,
        removable: true,
        stampsWrapper: this.stampsWrapper,
      };

      VideoStamp.addStamp(stampMetadata);
    }, false);
  }

  //TODO
  _dispatchEvent($input) {
    // const items = Array.from(this.$inputs).map(($input) => ({
    //   checked: $input.checked,
    //   id: $input.id,
    // // }));
    // const $label = $input.parentNode.querySelector('label');
    // let task;
    //
    // if ($input.type === 'radio') {
    //   task = 'change';
    // } else if ($input.type === 'checkbox') {
    //   task = ($input.checked) ? 'select' : 'unselect';
    // } else {
    //   console.error('ERROR: Undefinated input type.');
    // }
    //
    // __dispatchEvent(this.target, 'selection.change', {}, {
    //   id: this.target.id,
    //   task,
    //   itemId: $input.id,
    //   itemData: $input.getAttribute('data-selectable-data'),
    //   $node: $label.firstChild,
    // });
  }
}
