import Cviceni from './cviceni';

// javascript pro první a poslední slajd
export default class Forms extends Cviceni {
  constructor() {
    super();

    this.$button = this.$initialSlide.querySelector('[data-nav-button="next"]');
    this.$showUserEmail = document.querySelectorAll(
      '[data-form="about-user-email"]'
    );
    this.$formExport = this.$exportSlide.querySelector('#form-to-send');

    this.isFromLMS =
      typeof this.import.lmsHash === 'string' && this.import.lmsHash.length > 0;
    this.groupMembers = 1;
  }

  init() {
    this.$button.classList.remove('button-hidden');

    // cvičení je v prohlížecím módu
    if (this.isDone) {
      this.whenDone();
    }
  }

  whenDone() {
    // vypsat email uživatele nebo jeho jméno pokud existuje
    this.$showUserEmail.forEach(($userEmail) => {
      const userName = this.import.exerciseData.user.form.name;
      const userEmail = this.import.user;
      const anon = this.urlParams.get('anon');
      let userEnd = '';

      if (anon && anon === '0') {
        if (userName) userEnd = userName;
        if (userEmail) userEnd += userName ? ` (${userEmail})` : userEmail;
      }
      else {
        userEnd = '**********';
      }

      // // TODO: translation
      $userEmail.innerText += ` ${userEnd || 'neznámý uživatel'}`;
    });

    // vypsat alternativní copy tlačítka
    this.$button.innerText = this.$button.getAttribute('data-done');
  }
}
