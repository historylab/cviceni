/* eslint-disable brace-style */
import escape from 'lodash/escape';
import unescape from 'lodash/unescape';
import isEmpty from 'lodash/isEmpty';
import Cviceni from './cviceni';
import VideoStampSave from '../saveModules/VideoStampSave';

export default class SaveData extends Cviceni {
  constructor() {
    super();

    this.$formExport = this.$exportSlide.querySelector('#form-to-send');

    // bind the context of an event listener
    this._clickListener = this._clickListener.bind(this);
    this.beforeSendForm = this.beforeSendForm.bind(this);
    this.beforeSubmitForm = this.beforeSubmitForm.bind(this);
    this.getSaveDataSize = this.getSaveDataSize.bind(this);
  }

  init() {
    switch (this.isDone) {
      // cvičení je vyplněné
      case true:
        // dát celému dokumentu classu .is-done
        document.documentElement.classList.add('is-done');
        document.documentElement.setAttribute('data-state', 'done');

        // zobrazit obsah, pro cvičení v prohlížecím módu
        document
          .querySelectorAll('.show-when-done')
          .forEach(($showWhenDone) => {
            $showWhenDone.classList.remove('hidden');
          });
        // skrýt obsah, pro cvičení v prohlížecím módu
        document
          .querySelectorAll('.hide-when-done')
          .forEach(($hideWhenDone) => {
            $hideWhenDone.classList.add('hidden');
          });
        break;

      // cvičení není vyplněné
      case false:
        this.addEventListeners();
        break;

      default:
        break;
    }
  }

  addEventListeners() {
    this.loopSaveButtons();
    this.$exportSlide
      .querySelector('button[type="submit"]')
      .addEventListener('click', this.beforeSendForm);
    this.$formToSend
      .addEventListener('submit', this.beforeSubmitForm);
  }

  getSaveDataSize() {
    const saveData = JSON.stringify(this.data);
    const saveDataSize = new TextEncoder().encode(saveData).length;
    return saveDataSize > 100000000 ? 999999999 : saveDataSize;
  }

  beforeSubmitForm(event) {
    event.preventDefault();
    try {
      this.$exportSaveButton.disabled = true;
      this.$exportSaveButton.classList.add('button-sent');
      this.$exportSaveButton.textContent = this.$exportSaveButton.dataset.saving || '…';
      this.beforeSendForm();
      if (document.getElementById('odpoved_id').value === 'abcdefghijklmnop') {
        console.error('ERROR: data writing to form');
        alert('Něco se pokazilo. Zkuste cvičení vyplnit znovu.');
      }
      else {
        const formAction = this.$formToSend.action;
        this.$formToSend.action = `${formAction}&size=${this.getSaveDataSize()}`;
        this.$formToSend.submit();
      }
    } catch (e) {
      console.error('Error, saving data beforeSubmit', e);
    }
  }

  beforeSendForm() {
    this.analytics = HISTORYLAB.analytics.data;
    this.analytics.time.end = {
      time: Math.round(performance.now()),
      date: Date.now(),
    };
    this.analytics.time.timeSpent = HISTORYLAB.analytics.timeSpent;
    this.analytics.activity.length += 1;
    this.analytics.activity.steps[this.analytics.activity.length] = {
      type: 'K',
      time: this.analytics.time.end.time,
      date: Date.now(),
    };
    HISTORYLAB.analytics.data = this.analytics;

    document.getElementById('odpoved_id').value = this.data;
    // document.getElementById('analytika_id').value = this.data;
    // console.log("DATA", this.data);
    // event.preventDefault();
  }

  // check for user data
  checkForData(importedData) {
    if (isEmpty(importedData)) {
      console.log('nemáme uživatelská data');
    } else {
      console.log('máme uživatelská data');
      // load & render user data
      // console.log('user.data', importedData.user.data);
      this.data = importedData.user.data;
    }

    return this;
  }

  // buttons which saves the current state of user data
  loopSaveButtons() {
    for (let i = 0; i < this.$saveButtons.length; i += 1) {
      this.$saveButtons[i].addEventListener(
        'click',
        this._clickListener,
        false,
      );
    }
  }

  // print data to console for debug
  _clickListener() {
    // console.log(this.data);
  }

  // eslint-disable-next-line class-methods-use-this
  getExerciseMode() {
    // pokud máme informaci z backendu
    if (
      Object.prototype.hasOwnProperty.call(HISTORYLAB.import, 'exerciseMode')
    ) {
      return HISTORYLAB.import.exerciseMode;
    }
    // pokud nemáme nic tak nezvoleno nic
    return -1;
  }

  // save data
  get data() {
    const version = {
      global: document
        .querySelector('[data-version-global]')
        .getAttribute('data-version-global'),
    };
    [version.major, version.minor, version.patch] = version.global.split('.');

    function replaceURL(html) {
      // const html = SaveData.b64DecodeUnicode(dataField.value);

      // replace `cviceni-aktualni` in saved html
      if (html.indexOf('cviceni-aktualni')) {
        const regex = /cviceni-aktualni/g;

        // from v6.0.0 we store every minor version (eg. `v6-0`, `v7-2`)
        if (version.major >= 6) {
          return html.replace(
            regex,
            `cviceni-verze/v${version.major}-${version.minor}`,
          );
        }
        return html.replace(regex, `cviceni-verze/v${version.major}`);
      }
      return html;
    }

    // data object
    const data = {};
    // informace o cviceni a záznamu
    data.entryId = HISTORYLAB.import.entryid || -1;
    data.lmsHash = HISTORYLAB.import.lmsHash || '';
    data.exercise = {
      id: parseInt(this.$cviceni.getAttribute('data-exercise-id'), 10),
      slug: this.$cviceni.getAttribute('id'),
      name: document.querySelector('#header .title').innerText || '',
      version: {
        global: version.global,
        content: parseInt(
          document.documentElement.getAttribute('data-version-content'),
          10,
        ),
      },
      mode: this.getExerciseMode(),
      language: HISTORYLAB.import.language || 'cs',
    };

    // data o a od uživatele
    data.user = {
      // data z LMS nebo z formuláře
      form: {
        name: HISTORYLAB.import.name,
        email: HISTORYLAB.import.user,
        institution: HISTORYLAB.import.institution,
        teacher: HISTORYLAB.import.teacher,
        group: {
          members: HISTORYLAB.import.groupMembers,
        },
        exhibition: HISTORYLAB.import.exhibitionId,
        redirectUrl: HISTORYLAB.import.redirectUrl || null,
      },
      // analytická data
      analytics: HISTORYLAB.analytics.data || {},
      // uživatelská data
      data: [],
      // tracking
      tracking: HISTORYLAB.import.tracking,
    };

    // loop through all user data fields
    const $dataFields = document.querySelectorAll('[data-save]');
    data.user.data = Array.from($dataFields).map(($dataField) => {
      const dataField = {
        // default properties of dataField object
        id: '',
        node: '',
        value: '',
        slide: {
          id: '',
          index: 0,
        },
      };

      const $slide = $dataField.closest('.slide');
      dataField.slide.id = $slide.getAttribute('id');
      dataField.slide.index = parseInt(
        $slide.getAttribute('data-slide-index'),
        10,
      );

      dataField.id = $dataField.getAttribute('id');
      dataField.node = $dataField.tagName;

      // check for element type and get its appropriate value
      // TEXTAREA
      if (dataField.node === 'TEXTAREA') {
        if (
          $dataField.hasAttribute('data-textarea-target')
          || $dataField.hasAttribute('data-svg-textarea-target')
        ) {
          // if there is data-attribute
          if ($dataField.value) {
            dataField.value = escape($dataField.value);
          } else {
            dataField.value = null;
          }
          // if TEXTAREA in SVG, save also ID of that SVG
          if ($dataField.getAttribute('data-svg-textarea-target')) {
            dataField.svg = $dataField.getAttribute('data-svg-textarea-target');
          }
        } else {
          // if no data-attribute
          console.log(dataField.id);
          console.log(
            `Not registered ${dataField.node}. Please, add data-attribute.`,
          );
        }
      }
      // INPUT
      else if (dataField.node === 'INPUT') {
        if ($dataField.getAttribute('data-save') === 'value') {
          // if there is data-attribute
          if ($dataField.value) {
            dataField.value = escape($dataField.value);
          } else {
            dataField.value = null;
          }
        } else {
          // if no data-attribute
          console.log(dataField.id);
          console.log(
            `Not registered ${dataField.node}. Please, add data-attribute.`,
          );
        }
      }
      // SELECT
      else if (dataField.node === 'SELECT') {
        if ($dataField.getAttribute('data-save') === 'value') {
          // if there is data-attribute
          if ($dataField.value) {
            dataField.value = $dataField.value;
            for (let i = 0; i < $dataField.options.length; i += 1) {
              if ($dataField.options[i].innerHTML === $dataField.value) {
                dataField.selectedIndex = i;
                break;
              }
            }
          } else {
            dataField.value = null;
          }
        } else {
          // if no data-attribute
          console.log(dataField.id);
          console.log(
            `Not registered ${dataField.node}. Please, add data-attribute.`,
          );
        }
      }
      // SVG
      else if (dataField.node === 'svg') {
        // TODO: remove this condition data-save="html" should be sufficient universal rule atm
        if ($dataField.hasAttribute('data-svg-target')) {
          // if data-attribute
          dataField.value = SaveData.b64EncodeUnicode(
            replaceURL($dataField.innerHTML),
          );

          const $paths = $dataField.querySelectorAll('[data-svg-path]').length;
          const $comments = $dataField.querySelectorAll(
            '[data-svg-textarea-target]',
          ).length;
          const $points = $dataField.querySelectorAll('[data-svg-circle]').length;

          dataField.analytics = {
            comments: $comments > 0 ? $comments : false,
            paths: $paths > 0 ? $paths : false,
            points: $points > 0 ? $points : false,
          };
        } else {
          // if no data-attribute
          console.log(dataField.id);
          console.log(
            `Not registered ${dataField.node}. Please, add data-attribute.`,
          );
        }
      }
      // DIV
      else if (dataField.node === 'TABLE' || dataField.node === 'TBODY') {
        if ($dataField.getAttribute('data-save') === 'html') {
          // if data-attribute
          // console.log(dataField.id);
          // console.log($dataField.innerHTML);
          dataField.value = SaveData.b64EncodeUnicode(
            replaceURL($dataField.innerHTML),
          );
        }
      }
      // DIV
      else if (dataField.node === 'DIV') {
        // Řazení
        // TODO: remove this condition data-save="html" should be sufficient universal rule atm
        if ($dataField.hasAttribute('data-sortable-target')) {
          // if data-attribute
          dataField.value = SaveData.b64EncodeUnicode(
            replaceURL($dataField.innerHTML),
          );
        }
        // TODO: remove this condition data-save="html" should be sufficient universal rule atm
        // Analytická tabulka
        else if (
          $dataField.getAttribute('data-target') === 'analyticka-tabulka'
        ) {
          // if data-attribute
          // console.log(dataField.id);
          // console.log($dataField.innerHTML);
          dataField.value = SaveData.b64EncodeUnicode(
            replaceURL($dataField.innerHTML),
          );
        }
        // Video STAMP
        else if (
          $dataField.getAttribute('data-save') === 'video-stamp'
        ) {
          dataField.value = VideoStampSave.getData($dataField);
        }
          // TODO: remove this condition data-save="html" should be sufficient universal rule atm
        // Analytická tabulka
        else if ($dataField.getAttribute('data-target') === 'editor') {
          // if data-attribute
          // console.log(dataField.id);
          // console.log($dataField.innerHTML);
          dataField.value = SaveData.b64EncodeUnicode(
            replaceURL($dataField.innerHTML),
          );
        }
        // Anything else with HTML content to save
        else if ($dataField.getAttribute('data-save') === 'html') {
          // if data-attribute
          // console.log(dataField.id);
          // console.log($dataField.innerHTML);
          const editor = HISTORYLAB.tools[$dataField.id];
          if (editor) {
            dataField.value = editor.getEditorJSON();
          } else {
            dataField.value = SaveData.b64EncodeUnicode(
              replaceURL($dataField.innerHTML),
            );
          }
        } else {
          // if no data-attribute
          console.log(dataField.id);
          console.log(
            `Not registered ${dataField.node}. Please, add data-attribute.`,
          );
        }
      }

      return dataField;
    });

    // save what browser and os user used
    data.user.analytics.client = platform.description;

    // differentiate saved JSON
    data.done = true;

    return JSON.stringify(data);
  }

  // load data
  set data(importedData) {
    // Necessary order for rendering user data
    // 1.loop — Render DOM (svg, div)
    // 2.loop — Load VALUES (textarea)
    const loops = 2;

    for (let loopIndex = 0; loopIndex < loops; loopIndex += 1) {
      SaveData.renderUserData(importedData, loopIndex);
    }

    // 1. loop all src, xlink:href
    // 2. check for `cviceni-aktualni`
    // 3. replace it with `cviceni-verze/${versionMajor}`
  }

  static renderUserData(data, loopIndex) {
    const version = {
      global: document
        .querySelector('[data-version-global]')
        .getAttribute('data-version-global')
        .split('.'),
    };
    [version.major, version.minor, version.patch] = version.global;

    data.forEach((dataField) => {
      const $element = document.getElementById(dataField.id);

      if (!$element) {
        console.error(dataField.id, 'No element with the id exists!');
        return;
      }

      switch (loopIndex) {
        // 1.loop — Render DOM (svg, div)
        // needs to be decoded from Base64
        // https://developer.mozilla.org/en-US/docs/Web/API/WindowOrWorkerGlobalScope/atob
        case 0:
          switch (dataField.node) {
            case 'svg':
            case 'DIV':
            case 'TABLE':
            case 'TBODY':
              // eslint-disable-next-line no-case-declarations
              if (typeof dataField.value === 'string') {
                const html = SaveData.b64DecodeUnicode(dataField.value);

                // replace `cviceni-aktualni` in saved html
                if (html.indexOf('cviceni-aktualni')) {
                  const regex = /cviceni-aktualni/g;

                  // from v6.0.0 we store every minor version (eg. `v6-0`, `v7-2`)
                  if (version.major >= 6) {
                    $element.innerHTML = html.replace(
                      regex,
                      `cviceni-verze/v${version.major}-${version.minor}`,
                    );
                  } else {
                    $element.innerHTML = html.replace(
                      regex,
                      `cviceni-verze/v${version.major}`,
                    );
                  }
                } else {
                  $element.innerHTML = html;
                }
              } else {
                const $editor = document.querySelector(
                  `#prosemirror-content-${dataField.id}`,
                );
                if ($editor) {
                  $editor.innerHTML = JSON.stringify(dataField.value);
                } else {
                  console.error(
                    'ERROR: editor dosnt have definated content node',
                  );
                }
              }
              break;

            default:
              break;
          }

          break;

        // 2.loop — Load VALUES (textarea, input, select)
        case 1:
          switch (dataField.node) {
            case 'INPUT':
              $element.value = unescape(dataField.value || ' ');
              $element.readOnly = true;
              break;
            case 'SELECT':
              $element.selectedIndex = dataField.selectedIndex || 0;
              $element.value = dataField.value || ' ';
              $element.disabled = true;
              break;
            case 'TEXTAREA':
              $element.value = unescape(dataField.value || ' ');
              $element.readOnly = true;
              break;
            case 'DIV':
              const type = $element.getAttribute('data-save');
              switch (type) {
                case 'video-stamp':
                  VideoStampSave.loadData($element, dataField);
                  break;
              }
              // VideoStampSave.loadData($element);
              // $element.value = unescape(dataField.value || ' ');
              // $element.readOnly = true;
              break;

            default:
              break;
          }

          break;

        default:
          break;
      }
    });
  }

  // https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
  static b64EncodeUnicode(str) {
    // first we use encodeURIComponent to get percent-encoded UTF-8,
    // then we convert the percent encodings into raw bytes which
    // can be fed into btoa.
    return btoa(
      encodeURIComponent(str).replace(
        /%([0-9A-F]{2})/g,
        // eslint-disable-next-line arrow-body-style
        (match, p1) => {
          return String.fromCharCode(`0x${p1}`);
        },
      ),
    );
  }

  static b64DecodeUnicode(str) {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    // eslint-disable-next-line arrow-body-style
    return decodeURIComponent(
      atob(str)
        .split('')
        .map((c) => `%${`00${c.charCodeAt(0).toString(16)}`.slice(-2)}`)
        .join(''),
    );
  }
}
