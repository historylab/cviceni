import VideoStamp from '../components/video-stamp';

export default class VideoStampSave {
  static getData(target) {
    return [...target.querySelectorAll('[data-stamp-mark]')].map((stamp, index) => {
      return {
        id: index + 1,
        time: stamp.getAttribute('data-stamp-time'),
        content: stamp.innerText,
      };
    });
  }

  static loadData(target, data) {

    //Create stamps wrapper
    let stampsWrapper = VideoStamp.createStampsWrapper(target);

    //Get Video
    const $video = target.querySelector('video');

    //Wait for video to load metadata, then render stamps
    $video.addEventListener('loadedmetadata', () => {
      data.value.forEach(stampLoaded => {
        const stampMetadata = {
          stamp: stampLoaded.content,
          currentTime: stampLoaded.time,
          videoDuration: $video.duration,
          removable: false,
          stampsWrapper: stampsWrapper,
        };
        VideoStamp.addStamp(stampMetadata);
      });
    });


  }
}
