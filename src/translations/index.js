const translations = {
  _common: {
    close: {
      cs: 'Zavřít',
      en: 'Close',
      pl: 'Zamknij',
      sk: 'Zavrieť',
    },
  },
  editor: {
    reset: {
      cs: 'Obnovit původní text',
      en: 'Reset to original text',
      pl: 'Przywróć oryginalny tekst',
      sk: 'Obnoviť pôvodný text',
    },
  },
  fakeMap: {
    switchLayers: {
      cs: 'Přepínání vrstev',
      en: 'Switch layers',
      pl: 'Przełączanie warstw',
      sk: 'Prepínanie vrstiev',
    },
  },
  feedback: {
    default: {
      cs: 'Mám hotovo!',
      en: 'Done!',
      pl: 'Gotowe!',
      sk: 'Mám hotovo!',
    },
    hover: {
      cs: 'Další slajd →',
      en: 'Next slide →',
      pl: 'Kolejny slajd →',
      sk: 'Ďalší slajd →',
    },
    text: {
      none: {
        cs: 'Napiš něco!',
        en: 'Write something!',
        pl: 'Napisz coś',
        sk: 'Napíš niečo!',
      },
      short: {
        cs: 'Není toho moc',
        en: 'There isn\'t much text',
        pl: 'Nie ma tego zbyt dużo',
        sk: 'Nie je toho veľa',
      },
      ok: {
        cs: 'Mám hotovo!',
        en: 'I am done!',
        pl: 'Gotowe!',
        sk: 'Mám hotovo!',
      },
    },
  },
  fullscreen: {
    enlarge: {
      cs: 'Zvětšit',
      en: 'Enlarge',
      pl: 'Powiększ',
      sk: 'Zväčšiť',
    },
  },
  help: {
    help: {
      cs: 'Nápověda',
      en: 'Help',
      pl: 'Podpowiedź',
      sk: 'Pomocník',
    },
  },
  media: {
    transcript: {
      cs: 'Přepis nahrávky',
      en: 'Transcript',
      pl: 'Transkrypcja nagrania',
      sk: 'Prepis nahrávky',
    },
  },
  navigation: {
    next: {
      cs: 'Další',
      en: 'Next',
      pl: 'Dalej',
      sk: 'Ďalší',
    },
    previous: {
      cs: 'Předchozí',
      en: 'Previous',
      pl: 'Wróć',
      sk: 'Predchádzajúci',
    },
    hotkeys: {
      cs: 'Klávesové zkratky',
      en: 'Shortcuts',
      pl: 'Skróty klawiszowe',
      sk: 'Klávesové skratky',
    },
  },
  other: {
    loading: {
      cs: 'Načítám cvičení',
      en: 'Loading',
      pl: 'Wgrywam ćwiczenie',
      sk: 'Načítavam cvičenie',
    },
    loaded: {
      cs: 'Hotovo',
      en: 'Ready',
      pl: 'Gotowe',
      sk: 'Hotovo',
    },
    unsupported: {
      sorry: {
        cs: 'Omlouváme se, ale cvičení nelze spustit.',
        en: 'Sorry, the activity cannot be started.',
        pl: 'Przepraszamy, ale nie można rozpocząć ćwiczenia.',
        sk: 'Ospravedlňujeme sa, ale cvičenie nemožno spustiť.',
      },
      update: {
        cs: 'Prosím, aktualizujte si internetový prohlížeč (<a href="https://www.mozilla.org/firefox/" target="_blank">Firefox</a>, <a href="https://www.google.com/chrome/" target="_blank">Chrome</a>, <a href="https://www.microsoft.com/windows/microsoft-edge" target="_blank">Edge</a>).',
        en: 'Please update your internet browser (<a href="https://www.mozilla.org/firefox/" target="_blank">Firefox</a>, <a href="https://www.google.com/chrome/" target="_blank">Chrome</a>, <a href="https://www.microsoft.com/windows/microsoft-edge" target="_blank">Edge</a>).',
        pl: 'Zaktualizuj przeglądarkę internetową (<a href="https://www.mozilla.org/firefox/" target="_blank">Firefox</a>, <a href="https://www.google.com/chrome/" target="_blank">Chrome</a>, <a href="https://www.microsoft.com/windows/microsoft-edge" target="_blank">Edge</a>).',
        sk: 'Prosím, aktualizujte si internetový prehliadač (<a href="https://www.mozilla.org/firefox/" target="_blank">Firefox</a>, <a href="https://www.google.com/chrome/" target="_blank">Chrome</a>, <a href="https://www.microsoft.com/windows/microsoft-edge" target="_blank">Edge</a>).',
      },
      version: {
        cs: 'Verze vašeho prohlížeče:',
        en: 'Your browser\'s version:',
        pl: 'Twoja wersja przeglądarki:',
        sk: 'Verzia vášho prehliadača:',
      },
    },
  },
  slideExport: {
    default: {
      h1: {
        cs: 'Výborně! Úspěšně jste prošli celým cvičením.',
        en: 'Great! You have completed the activity.',
        pl: 'Świetnie! Udało Ci się wykonać całe ćwiczenie.',
        sk: 'Výborne! Úspešne ste prešli celým cvičením.',
      },
      h2: {
        cs: 'Teď už ho stačí jen uložit',
        en: 'Save it now!',
        pl: 'Teraz wystarczy ćwiczenie zapisać',
        sk: 'Teď už ho stačí jen uložit',
      },
      save: {
        cs: 'Uložit cvičení',
        en: 'Save activity',
        pl: 'Zapisz ćwiczenie',
        sk: 'Uložiť cvičenie',
      },
      saving: {
        cs: 'Ukládání…',
        en: 'Saving…',
        pl: 'Oszczędność…',
        sk: 'Ukladanie…',
      },
      return: {
        cs: '… nebo se vrátit zpět a vylepšit ho',
        en: '… or go back and improve it',
        pl: '… lub wróć i popraw je.',
        sk: '… alebo sa vrátiť späť a vylepšiť ho',
      },
      privacyPolicy: {
        cs: 'Klinutím na tlačítko „Uložit cvičení“ souhlasíte se zpracováním osobních údajů a poskytnutím výsledků cvičení k dalšímu výzkumu a vývoji aplikace Historylab. Informace o zpracování osobních údajů naleznete <a href="https://historylab.cz/informace-o-zpracovani-osobnich-udaju.html" target="_blank">zde</a>.',
        en: 'By clicking on the "Save activity" button, you agree to allow us to process your personal data and to provide the results of the activity for further research and development of the HistoryLab application. Find more information about the processing of your personal data <a href="https://historylab.cz/informace-o-zpracovani-osobnich-udaju.html" target="_blank">here</a>.',
        pl: 'Klikając przycisk „Zapisz ćwiczenie” wyrażasz zgodę na przetwarzanie danych osobowych oraz udostępnienie wyników ćwiczeń w celu dalszych badań i rozwoju aplikacji Historylab. Informacje o przetwarzaniu danych osobowych znajdziesz <a href="https://historylab.cz/informace-o-zpracovani-osobnich-udaju.html" target="_blank">tutaj</a>.',
        sk: 'Klinutím na tlačidlo „Uložiť cvičenie“ súhlasíte so spracovaním osobných údajov a poskytnutím výsledkov cvičenia k ďalšiemu výskumu a vývoju aplikácie Historylab. Informácie o spracovaní osobných údajov nájdete <a href="https://historylab.cz/informace-o-zpracovani-osobnich-udaju.html" target="_blank">tu</a>.',
      },
      logolink: {
        cs: 'Projekt vznikl díky grantu Technologické agentury ČR',
        en: 'The project was funded by the Technology Agency of the Czech Republic',
        pl: 'Projekt powstał dzięki dotacji Agencji Technologicznej Republiki Czeskiej',
        sk: 'Projekt vznikol vďaka grantu Technologické agentury ČR',
      },
    },
    done: {
      h1: {
        cs: 'Konec vyplněného cvičení od',
        en: 'The end of the completed activity from',
        pl: 'Koniec uzupełnionego ćwiczenia od',
        sk: 'Koniec vyplneného cvičenia od',
      },
      return: {
        cs: 'vrátit se a ještě prohlédnout',
        en: 'go back and review the activity again',
        pl: 'wróć i przejrzyj jeszcze raz',
        sk: 'vrátiť sa a ešte prezrieť',
      },
    },
    userText: {
      cs: 'Napadlo vás k tématu cvičení něco dalšího?',
      en: 'Do you have any other thoughts on the topic?',
      pl: 'Masz w związku z tematem ćwiczenia inny pomysł?',
      sk: 'Napadlo vás k téme cvičenia niečo ďalšie?',
    },
  },
  slideForm: {
    default: {
      welcome: {
        cs: 'Vítejte ve cvičení',
        en: 'Welcome to the activity',
        pl: 'Witamy w ćwiczeniu',
        sk: 'Vitajte v cvičení',
      },
      missingAnotation: {
        cs: 'Omlouváme se, pro toto cvičení ještě nemáme anotaci.',
        en: 'Sorry, we haven\'t prepared an annotation for this activity yet.',
        pl: 'Przepraszamy, nie mamy jeszcze adnotacji do tego ćwiczenia.',
        sk: 'Ospravedlňujeme sa, pre toto cvičenie ešte nemáme anotáciu.',
      },
      instruction: {
        cs: 'Pokud budete chtít cvičení uložit, vyplňte následující formulář',
        en: 'If you want to save this exercise, fill in the following form',
        pl: 'Jeśli chcesz zapisać ćwiczenie, wypełnij formularz',
        sk: 'Pokiaľ budete chcieť cvičenie uložiť, vyplňte nasledujúci formulár',
      },
      aboutMe: {
        cs: 'Údaje o mně',
        en: 'About me',
        pl: 'Dane o mnie',
        sk: 'Údaje o mne',
      },
      required: {
        cs: 'povinné',
        en: 'required',
        pl: 'obowiązkowe',
        sk: 'povinné',
      },
      name: {
        cs: 'Mé jméno a příjmení',
        en: 'My first name and surname',
        pl: 'Moje imię i nazwisko',
        sk: 'Moje meno a priezvisko',
      },
      email: {
        cs: 'Můj e-mail',
        en: 'My e-mail address',
        pl: 'Mój e-mail',
        sk: 'Môj e-mail',
      },
      eg: {
        cs: 'např.',
        en: 'eg.',
        pl: 'np.',
        sk: 'napr.',
      },
      aboutInstitution: {
        cs: 'Údaje o škole a učiteli',
        en: 'About your institution & teacher',
        pl: 'Dane dotyczące szkoły i nauczyciela',
        sk: 'Údaje o škole a učiteľovi',
      },
      institution: {
        cs: 'Název a místo mé školy',
        en: 'Name and location of your school',
        pl: 'Nazwa i lokalizacja mojej szkoły',
        sk: 'Názov a miesto mojej školy',
      },
      emailTeacher: {
        cs: 'E-mail mého učitele',
        en: 'Your teacher\'s e-mail',
        pl: 'E-mail mojego nauczyciela',
        sk: 'E-mail môjho učiteľa',
      },
      mode: {
        cs: 'Způsob práce',
        en: 'Working method',
        pl: 'Sposób pracy',
        sk: 'Spôsob práce',
      },
      individually: {
        cs: 'samostatně',
        en: 'individually',
        pl: 'indywidualnie',
        sk: 'samostatne',
      },
      group: {
        cs: 've skupině',
        en: 'in a group',
        pl: 'w grupach',
        sk: 'v skupine',
      },
      class: {
        cs: 's celou třídou',
        en: 'in class',
        pl: 'z całą klasą',
        sk: 's celou triedou',
      },
      groupEmails: {
        cs: 'E-maily dalších členů skupiny',
        en: 'Other members of your group\'s email addresses',
        pl: 'E-maile innych członków grupy',
        sk: 'E-maily ďalších členov skupiny',
      },
      groupEmailsPlaceholder: {
        cs: 'Zde napište e-mail člena skupiny',
        en: 'Write the email address of a member of your group here',
        pl: 'Tutaj wpisz e-mail członka grupy',
        sk: 'Tu napíšte e-mail člena skupiny',
      },
      groupEmailsAdd: {
        cs: '+ Přidat dalšího člena skupiny',
        en: '+ Add another group member',
        pl: 'Dodaj kolejnego członka grupy',
        sk: '+ Pridať ďalšieho člena skupiny',
      },
      button: {
        default: {
          cs: 'Mám přečteno',
          en: 'I\'m done with the text',
          pl: 'Przeczytane.',
          sk: 'Mám prečítané',
        },
        hover: {
          cs: 'Pustit se do práce →',
          en: 'Start the activity →',
          pl: 'Zabierz się do pracy →',
          sk: 'Pustiť sa do práce →',
        },
      },
      partners: {
        cs: 'Ve spolupráci s',
        en: 'In cooperation with',
        pl: 'We współpracy z',
        sk: 'V spolupráci s',
      },
    },
    done: {
      welcome: {
        cs: 'Vítejte ve vyplněném cvičení',
        en: 'Welcome to the completed activity',
        pl: 'Witamy w uzupełnionym ćwiczeniu',
        sk: 'Vitajte vo vyplnenom cvičení',
      },
      completedBy: {
        cs: 'Cvičení vyplnil(a)',
        en: 'Completed by',
        pl: 'Ćwiczenie wykonanał/a',
        sk: 'Cvičenie vyplnil(a)',
      },
      button: {
        cs: 'Prohlédnout vyplněné cvičení',
        en: 'View the completed activity',
        pl: 'Zobacz uzupełnione ćwiczenie',
        sk: 'Pozrieť vyplnené cvičenia',
      },
      unknownUser: {
        cs: 'neznámý uživatel',
        en: 'unknown user',
        pl: 'nieznany użytkownik',
        sk: 'neznámy užívateľ',
      },
    },
  },
  svg: {
    addText: {
      cs: 'Přidat text',
      en: 'Add text',
      pl: 'Dodać tekst',
      sk: 'Pridať text',
    },
    changeColor: {
      cs: 'Změnit barvu',
      en: 'Change color',
      pl: 'Zmienić kolor',
      sk: 'Zmeniť farbu',
    },
    comics: {
      narrator: {
        cs: 'Vypravěč',
        en: 'Narrator',
        pl: 'Narrator',
        sk: 'Rozprávač',
      },
      speech: {
        cs: 'Promluva…',
        en: 'Speech…',
        pl: 'Przemówienie…',
        sk: 'Príhovor…',
      },
      thought: {
        cs: 'Myšlenka…',
        en: 'Thought…',
        pl: 'Myśl…',
        sk: 'Myšlienka…',
      },
    },
    remove: {
      cs: 'Vymazat prvek',
      en: 'Remove item',
      pl: 'Usunąć element',
      sk: 'Vymazať prvok',
    },
  },
  userText: {
    placeholder: {
      cs: 'Napadlo mě, že…',
      en: 'I think that…',
      pl: 'Myślę, że…',
      sk: 'Napadlo ma, že…',
    },
  },
  video: {
    muted: {
      cs: 'Bez zvuku',
      en: 'Muted',
      pl: 'Bez dźwięku',
      sk: 'Bez zvuku',
    },
  },
  selectable: {
    select: {
      cs: 'Vybrat',
      en: 'Select',
      pl: 'Wybierz',
      sk: 'Vybrať',
    },
    deselect: {
      cs: 'Odebrat',
      en: 'Deselect',
      pl: 'odznaczyć',
      sk: 'Odobrať',
    },
  },
  table: {
    results: {
      cs: 'Výsledky předchozí práce',
      en: 'Your previous work',
      pl: 'Twoja poprzednia praca',
      sk: 'Výsledky predchádzajúcej práce',
    },
  },
};

module.exports = translations;
